#ifndef ZprimeDM_CutflowHists_H
#define ZprimeDM_CutflowHists_H

#include <TFile.h>

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>

class CutflowHists : public HistogramManager
{
public:

  CutflowHists(const std::string& name, const std::string& detailStr);
  virtual ~CutflowHists() ;

  virtual StatusCode initialize();
  virtual StatusCode initialize(TH1F *cutflow, TH1F *cutflow_weighted);

  int addCut(const std::string& cut);

  StatusCode executeInitial(float totalNevents, float totalWeight);
  StatusCode executeInitial(TH1F *cutflow, TH1F *cutflow_weighted);
  StatusCode execute( int cutflow, float eventWeight);
  StatusCode execute( const std::string& cutflow, float eventWeight);

  virtual StatusCode finalize(TH1F *cutflow, TH1F *cutflow_weighted);

  StatusCode write(TFile *fh);

  using HistogramManager::book;     // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute;  // make other overloaded version of execute() to show up in subclass
  using HistogramManager::finalize; // make other overloaded version of finalize() to show up in subclass

private:
  //histograms
  TH1F* m_cutflow;             //!
  TH1F* m_cutflowW;            //!
};

#endif
