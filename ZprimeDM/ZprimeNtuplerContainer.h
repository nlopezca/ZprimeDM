#ifndef ZprimeDM_ZprimeNtuplerContainer_H
#define ZprimeDM_ZprimeNtuplerContainer_H

#include <string>

class ZprimeNtuplerContainer
{
public:
  enum Type {JET,FATJET,PHOTON,MUON,ELECTRON,TRUTH,HLT_PHOTON};

  ZprimeNtuplerContainer();
  ZprimeNtuplerContainer(ZprimeNtuplerContainer::Type type, const std::string& containerName, const std::string& branchName, const std::string& detailStr, const std::string& detailStrSyst="");
  ZprimeNtuplerContainer(ZprimeNtuplerContainer::Type type, const std::string& branchName, const std::string& detailStr);

  Type m_type;
  std::string m_containerName;
  std::string m_branchName;
  std::string m_detailStr;
  std::string m_detailStrSyst;

  std::string m_subjetDetailStr; // Only for FatJetContainer
  std::string m_subjetDetailStrSyst; // Only for FatJetContainer
private:
};

#endif // ZprimeDM_ZprimeNtuplerContainer_H
