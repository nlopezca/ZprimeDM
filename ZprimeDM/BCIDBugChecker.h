#ifndef ZprimeDM_BCIDBugChecker_H
#define ZprimeDM_BCIDBugChecker_H

#include <string>
#include <set>

class BCIDBugChecker
{
public:
  BCIDBugChecker();
  ~BCIDBugChecker();

  void addList(const std::string& path);
  bool check(uint runNumber, uint eventNumber) const;

private:
  std::set<std::pair<uint,uint> > m_badEvents;
};

#endif // ZprimeDM_BCIDBugChecker_H
