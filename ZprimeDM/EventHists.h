#ifndef ZprimeDM_EventHists_H
#define ZprimeDM_EventHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/DijetISREvent.h>

class EventHists : public HistogramManager
{
public:

  EventHists(const std::string& name, const std::string& detailStr);
  virtual ~EventHists() ;

  bool m_debug;
  virtual StatusCode initialize();

  StatusCode execute(const DijetISREvent& event, float eventWeight);
  using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

private:
  //histograms
  TH1F*     h_NPV;
  TH1F*     h_mu_ave;
  TH1F*     h_mu_act;

  TH1F*     h_weight;
};

#endif // ZprimeDM_EventHists_H
