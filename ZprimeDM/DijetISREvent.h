#ifndef ZprimeDM_DijetISREvent_H
#define ZprimeDM_DijetISREvent_H

#include <TTree.h>
#include <TLorentzVector.h>
#include <TClonesArray.h>

#include <vector>
#include <string>

#include <xAODAnaHelpers/JetContainer.h>
#include <xAODAnaHelpers/PhotonContainer.h>
#include <xAODAnaHelpers/MuonContainer.h>
#include <xAODAnaHelpers/ElectronContainer.h>
#include <xAODAnaHelpers/FatJetContainer.h>
#include <xAODAnaHelpers/TruthContainer.h>

class DijetISREvent
{
public:
  static DijetISREvent *global();

  DijetISREvent();
  virtual ~DijetISREvent();

public:
  // Settings
  bool m_doTruthOnly;
  bool m_mc;

  // Event variables
  int      m_runNumber;
  Long64_t m_eventNumber;
  int      m_lumiBlock;
  uint32_t m_coreFlags;
  int      m_bcid;

  int      m_mcEventNumber;
  int      m_mcChannelNumber;
  float    m_mcEventWeight;
  std::vector<float> m_mcEventWeights;

  int m_NPV;
  float  m_actualInteractionsPerCrossing;
  float  m_averageInteractionsPerCrossing;

  // trigger
  std::vector<std::string>  *m_passedTriggers  =nullptr;
  std::vector<std::string>  *m_disabledTriggers=nullptr;
  std::vector<float>        *m_triggerPrescales=nullptr;
  std::vector<std::string>  *m_isPassBitsNames =nullptr;
  std::vector<unsigned int> *m_isPassBits      =nullptr;

  unsigned int isPassBits(const std::string& trigger) const;

  // weights
  float m_weight;
  float m_weight_xs;
  float m_weight_pileup;

  // custom
  float m_Zprime_pt;
  float m_Zprime_eta;
  float m_Zprime_phi;
  float m_Zprime_m;

  TLorentzVector m_Zprime;

  // Particles
  void setJets(std::vector<xAH::Jet> *jets);
  std::vector<xAH::Jet>* getJets() const;
  bool haveJets() const;
  uint jets() const;
  const xAH::Jet* jet(uint idx) const;

  void setPhotons(std::vector<xAH::Photon> *photons);
  std::vector<xAH::Photon>* getPhotons() const;
  bool havePhotons() const;
  uint photons() const;
  const xAH::Photon* photon(uint idx) const;

  void setMuons(std::vector<xAH::Muon> *muons);
  std::vector<xAH::Muon>* getMuons() const;
  bool haveMuons() const;
  uint muons() const;
  const xAH::Muon* muon(uint idx) const;

  void setElectrons(std::vector<xAH::Electron> *electrons);
  std::vector<xAH::Electron>* getElectrons() const;
  bool haveElectrons() const;
  uint electrons() const;
  const xAH::Electron* electron(uint idx) const;

  void setFatJets(std::vector<xAH::FatJet> *fatjets);
  std::vector<xAH::FatJet>* getFatJets() const;
  bool haveFatJets() const;
  uint fatjets() const;
  const xAH::FatJet* fatjet(uint idx) const;

  void setTrigJets(std::vector<xAH::Jet> *trigjets);
  std::vector<xAH::Jet>* getTrigJets() const;
  bool haveTrigJets() const;
  uint trigjets() const;
  const xAH::Jet* trigjet(uint idx) const;

  void setTruths(std::vector<xAH::TruthPart> *truths);
  std::vector<xAH::TruthPart>* getTruths() const;
  bool haveTruths() const;
  uint truths() const;
  const xAH::TruthPart* truth(uint idx) const;

protected:
  // containers
  std::vector<xAH::Jet>      *m_jets     =nullptr;
  std::vector<xAH::Photon>   *m_photons  =nullptr;
  std::vector<xAH::Muon>     *m_muons    =nullptr;
  std::vector<xAH::Electron> *m_electrons=nullptr;
  std::vector<xAH::FatJet>   *m_fatjets  =nullptr;
  std::vector<xAH::TruthPart>*m_truths   =nullptr;
  std::vector<xAH::Jet>      *m_trigJets =nullptr;

private:
  static DijetISREvent *m_event;
};

#endif // ZprimeDM_DijetISREvent_H
