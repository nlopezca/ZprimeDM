#ifndef ZprimeDM_ZprimeMiniTree_H
#define ZprimeDM_ZprimeMiniTree_H

#include "xAODAnaHelpers/HelpTreeBase.h"
#include "TTree.h"

class ZprimeMiniTree : public HelpTreeBase
{
private:
  std::vector<int> m_photons_HLT_g60_loose;
  std::vector<int> m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI;
  std::vector<int> m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1;
  std::vector<int> m_photons_HLT_g140_loose;
  std::vector<int> m_photons_HLT_g160_loose;

  std::vector<float> m_fatjets_ANN_score;

  float m_weight;
  float m_weight_corr;
  float m_weight_xs;

  float m_Zprime_pt;
  float m_Zprime_eta;
  float m_Zprime_phi;
  float m_Zprime_m;
  int   m_Zprime_pdg;

  float m_reso0_pt;
  float m_reso0_eta;
  float m_reso0_phi;
  float m_reso0_E;

  float m_reso1_pt;
  float m_reso1_eta;
  float m_reso1_phi;
  float m_reso1_E;

  float m_ISR_pt;
  float m_ISR_eta;
  float m_ISR_phi;
  float m_ISR_E;
  int m_ISR_pdgId;

  bool m_doTruth;
  bool m_saveZprimeDecay;
  bool m_saveExtraVariables;

  std::vector<float> m_HLT_ph_pt;
  std::vector<float> m_HLT_ph_eta;
  std::vector<float> m_HLT_ph_phi;

public:

  ZprimeMiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, bool doTruthInfo = false, bool saveZprimeDecay = false, bool saveExtraVariables = false);
  ~ZprimeMiniTree();

  void AddEventUser     (const std::string& detailStr = "" );
  void FillEventUser    (const xAOD::EventInfo* eventInfo );
  void ClearEventUser   ();

  void AddPhotonsUser   (const std::string& detailStr = "", const std::string& photonName = "ph");
  void FillPhotonsUser  (const xAOD::Photon* photon, const std::string& /*photonName*/);
  void ClearPhotonsUser (const std::string& /*photonName*/);

  void AddFatJetsUser   (const std::string& detailStr = "", const std::string& fatjetName = "", const std::string& suffix = "");
  void FillFatJetsUser  (const xAOD::Jet* /*jet*/, int /*pvLocation = 0*/, const std::string& /*fatjetName = "fatjet"*/, const std::string& /*suffix = ""*/);
  void ClearFatJetsUser (const std::string& /*fatjetName = "fatjet"*/, const std::string& /*suffix = ""*/);

  void AddHLTPhotons();
  void ClearHLTPhotons();
  void FillHLTPhotons(const xAOD::PhotonContainer* photons);

};
#endif
