#ifndef ZprimeDM_FatJetHists_H
#define ZprimeDM_FatJetHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/DijetISREvent.h>

namespace ZprimeDM
{
  class FatJetHists : public HistogramManager
  {
  public:

    FatJetHists(const std::string& name, const std::string& detailStr, const std::string& prefix="");
    virtual ~FatJetHists() ;

    bool m_debug;
    virtual StatusCode initialize();

    StatusCode execute(const xAH::FatJet*      fatjet, float eventWeight);
    using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
    using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

  private:
    HelperClasses::JetInfoSwitch m_infoSwitch;

    std::string m_prefix;

    //histograms

    // kinematic
    TH1F* h_pt;
    TH1F* h_pt_m;
    TH1F* h_pt_l;
    TH1F* h_eta;
    TH1F* h_phi;
    TH1F* h_m_s;
    TH1F* h_m;
    TH1F* h_m_m;
    TH1F* h_m_l;
    TH1F* h_rho;
    TH1F* h_mpt;

    // area
    TH1F* h_GhostArea         ;
    TH1F* h_ActiveArea        ;
    TH1F* h_VoronoiArea       ;
    TH1F* h_ActiveArea4vec_pt ;
    TH1F* h_ActiveArea4vec_eta;
    TH1F* h_ActiveArea4vec_phi;
    TH1F* h_ActiveArea4vec_m  ;

    // substructure
    TH1F* h_Split12        ;
    TH1F* h_Split23        ;
    TH1F* h_Split34        ;
    TH1F* h_Tau1_wta       ;
    TH1F* h_Tau2_wta       ;
    TH1F* h_Tau3_wta       ;
    TH1F* h_Tau21_wta      ;
    TH1F* h_Tau32_wta      ;
    TH1F* h_ECF1           ;
    TH1F* h_ECF2           ;
    TH1F* h_ECF3           ;
    TH1F* h_C2             ;
    TH1F* h_D2             ;
    TH1F* h_NTrimSubjets   ;
    TH1F* h_MyNClusters    ;
    TH1F* h_GhostTrackCount;

    // constituent
    TH1F* h_numConstituents;
    
    // bosonCount
    TH1F* h_GhostTQuarksFinalCount;
    TH1F* h_GhostWBosonsCount     ;
    TH1F* h_GhostZBosonsCount     ;
    TH1F* h_GhostHBosonsCount     ;

    // VTags
    TH1F* h_Wtag_medium;
    TH1F* h_Ztag_medium;

    TH1F* h_Wtag_tight ;
    TH1F* h_Ztag_tight ;

    // trackJets
    TH1F* h_nFixTrkJets ;
    TH1F* h_nFixTrkBJets;

    TH1F* h_nVRTrkJets ;
    TH1F* h_nVRTrkBJets;
  };
}

#endif // ZprimeDM_FatJetHists_H
