#ifndef ZprimeDM_ZprimeMGTrijetPickAlgo_H
#define ZprimeDM_ZprimeMGTrijetPickAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>

#include <ZprimeDM/CutflowHists.h>
#include <ZprimeDM/ZprimeMGTrijetHists.h>

#include <sstream>
#include <vector>
using namespace std;


class ZprimeMGTrijetPickAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
//  bool m_debug;
  std::string m_truthDetailStr;
  std::string m_pickMode;

  // Kinematic selection
  float m_jetPtCut;
  float m_leadJetPtCut;
  float m_ystarCut;

private:
  //
  // Cutflow
  int m_cf_jets;
  int m_cf_jet0;
  int m_cf_ystar;

  //
  // Histograms
  ZprimeMGTrijetHists *h_mjj;  //!
  ZprimeMGTrijetHists *h_mjj_correct;   //!
  ZprimeMGTrijetHists *h_mjj_incorrect; //!

  ZprimeMGTrijetHists *h_mjj_binscor[10]; //!
  ZprimeMGTrijetHists *h_mjj_binsinc[10]; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  ZprimeMGTrijetPickAlgo ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeMGTrijetPickAlgo, 1);
};

#endif
