#ifndef ZprimeDM_JMRUncertaintyAlgo_H
#define ZprimeDM_JMRUncertaintyAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

// rootcore includes
#include <GoodRunsLists/GoodRunsListSelectionTool.h>
#include <PileupReweighting/PileupReweightingTool.h>
#include <AsgTools/AnaToolHandle.h>

//algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>

#include <ZprimeDM/CutflowHists.h>

class JMRUncertaintyAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  enum VARIATIONS {NOMINAL, SIGMA1, SIGMA2, SIGMA3};
  
  bool m_do1Sigma=false; // Do 1-sigma variation
  bool m_do2Sigma=false; // Do 2-sigma variation
  bool m_do3Sigma=false; // Do 3-sigma variation

private:

  TRandom3 m_random;
  DijetISREvent* m_eventData =nullptr; //!

  std::vector<xAH::FatJet> m_jmrFatJets1; //!
  std::vector<xAH::FatJet> m_jmrFatJets2; //!
  std::vector<xAH::FatJet> m_jmrFatJets3; //!

  static DijetISREvent* m_eventData1; //!
  static DijetISREvent* m_eventData2; //!
  static DijetISREvent* m_eventData3; //!

  void copyNominal(DijetISREvent* eventDataV, std::vector<xAH::FatJet> *fatjetV);
  void varyNominal(DijetISREvent* eventDataV, JMRUncertaintyAlgo::VARIATIONS var);

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  static DijetISREvent *global(JMRUncertaintyAlgo::VARIATIONS var);

  // this is a standard constructor
  JMRUncertaintyAlgo (const std::string& className = "JMRUncertaintyAlgo");
  virtual ~JMRUncertaintyAlgo();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode finalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(JMRUncertaintyAlgo, 1);
};

#endif // ZprimeDM_JMRUncertaintyAlgo_H
