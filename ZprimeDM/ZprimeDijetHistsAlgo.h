#ifndef ZprimeDM_ZprimeDijetHistsAlgo_H
#define ZprimeDM_ZprimeDijetHistsAlgo_H

// algorithm wrapper
#include <ZprimeDM/ZprimeHistsBaseAlgo.h>

class ZprimeDijetHistsAlgo : public ZprimeHistsBaseAlgo
{
public:
  uint m_resoJet0Idx; // Index of leading resonance jet
  uint m_resoJet1Idx; // Index of subleading resonance jet

  uint m_nJets;       // Number of jets to require
  float m_minJetPt;   // Minimum jet pT for njets cut

protected:
  void initISRCutflow();
  bool doISRCutflow();

public:
  // this is a standard constructor
  ZprimeDijetHistsAlgo ();

private:
  // cutflow
  int m_cf_njets;

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeDijetHistsAlgo, 1);
};

#endif
