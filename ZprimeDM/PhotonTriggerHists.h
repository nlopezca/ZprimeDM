#ifndef ZprimeDM_PhotonTriggerHists_H
#define ZprimeDM_PhotonTriggerHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <ZprimeDM/ZprimeHelperClasses.h>
#include <ZprimeDM/DijetISREvent.h>
#include <ZprimeDM/JetHists.h>

class PhotonTriggerHists : public HistogramManager
{
public:
  PhotonTriggerHists(const std::string& name, const std::string& detailStr="", const std::string& jetDetailStr="");
  virtual ~PhotonTriggerHists();

  virtual void record(EL::IWorker *wk);

  StatusCode initialize();
  StatusCode execute(const xAH::Photon* photon, const xAH::Jet* trigJet0, const xAH::Jet* trigJet1, const xAH::Jet* trigJet2, float eventWeight);
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  // histograms
  std::string m_jetDetailStr;
  ZprimeDM::JetHists *m_trigJet0;
  ZprimeDM::JetHists *m_trigJet1;
  ZprimeDM::JetHists *m_trigJet2;

  TH1F* h_dRPhotonTrigJet0;
  TH1F* h_dRPhotonTrigJet1;
  TH1F* h_dRPhotonTrigJet2;

  TH1F* h_dRPhotonTrigJet0_50;
  TH1F* h_dRPhotonTrigJet1_50;
  TH1F* h_dRPhotonTrigJet2_50;
};

#endif
