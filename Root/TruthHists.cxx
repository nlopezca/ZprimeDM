#include <ZprimeDM/TruthHists.h>

using namespace ZprimeDM;

TruthHists :: TruthHists (const std::string& name, const std::string& detailStr, const std::string& prefix)
  : HistogramManager(name, detailStr), m_debug(false), m_infoSwitch(detailStr), m_prefix(prefix)
{ }

TruthHists :: ~TruthHists () 
{ }

StatusCode TruthHists::initialize()
{

  // kinematic
  if(m_infoSwitch.m_kinematic)
    {
      h_pt   = book(m_name, "pt"  , m_prefix+" p_{T} [GeV]", 100, 0, 500);
      h_pt_m = book(m_name, "pt_m", m_prefix+" p_{T} [GeV]", 100, 0, 1000);
      h_pt_l = book(m_name, "pt_l", m_prefix+" p_{T} [GeV]", 100, 0, 5000);

      h_eta  = book(m_name, "eta" , m_prefix+" #eta", 60, -3          , 3);
      h_phi  = book(m_name, "phi" , m_prefix+" #phi", 50, -TMath::Pi(), TMath::Pi());
    }

  if(true) // always
    {
      h_pdgId = book(m_name, "pdgId" , m_prefix+" PDG ID", 100, -49.5 , 50.5);
    }

  return StatusCode::SUCCESS;
}

StatusCode TruthHists::execute(const xAH::TruthPart* truth, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  if(m_infoSwitch.m_kinematic)
    {
      h_pt  ->Fill(truth->p4.Pt(),eventWeight);
      h_pt_m->Fill(truth->p4.Pt(),eventWeight);
      h_pt_l->Fill(truth->p4.Pt(),eventWeight);

      h_eta->Fill(truth->p4.Eta(),eventWeight);
      h_phi->Fill(truth->p4.Phi(),eventWeight);
    }

  if(true) //always
    {
      h_pdgId->Fill(truth->pdgId);
    }

  return StatusCode::SUCCESS;
}
