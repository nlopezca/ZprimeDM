#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "ZprimeDM/ZprimeMiniTree.h"

ZprimeMiniTree :: ZprimeMiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, bool doTruthInfo, bool saveZprimeDecay, bool saveExtraVariables) :
  HelpTreeBase(event, tree, file, 1e3)
{
  Info("ZprimeMiniTree", "Creating output TTree");
  m_doTruth = doTruthInfo;
  m_saveZprimeDecay = saveZprimeDecay;
  m_saveExtraVariables = saveExtraVariables;
}

ZprimeMiniTree :: ~ZprimeMiniTree()
{
}
//////////////////// Connect Defined variables to branches here /////////////////////////////
void ZprimeMiniTree::AddEventUser(const std::string& /*detailStr*/)
{
  m_tree->Branch("weight",    &m_weight   , "weight/F");
  m_tree->Branch("weight_xs", &m_weight_xs, "weight_xs/F");

  if (m_doTruth) {
    m_tree->Branch("Zprime_pt" , &m_Zprime_pt ,"Zprime_pt/F");
    m_tree->Branch("Zprime_eta", &m_Zprime_eta,"Zprime_eta/F");
    m_tree->Branch("Zprime_phi", &m_Zprime_phi,"Zprime_phi/F");
    m_tree->Branch("Zprime_m"  , &m_Zprime_m  ,"Zprime_m/F");
    m_tree->Branch("Zprime_pdg", &m_Zprime_pdg,"Zprime_pdg/i");

    if(m_saveZprimeDecay){
      m_tree->Branch("reso0_pt" , &m_reso0_pt ,"reso0_pt/F");
      m_tree->Branch("reso0_eta", &m_reso0_eta,"reso0_eta/F");
      m_tree->Branch("reso0_phi", &m_reso0_phi,"reso0_phi/F");
      m_tree->Branch("reso0_E"  , &m_reso0_E  ,"reso0_E/F");

      m_tree->Branch("reso1_pt" , &m_reso1_pt ,"reso1_pt/F");
      m_tree->Branch("reso1_eta", &m_reso1_eta,"reso1_eta/F");
      m_tree->Branch("reso1_phi", &m_reso1_phi,"reso1_phi/F");
      m_tree->Branch("reso1_E"  , &m_reso1_E  ,"reso1_E/F");
      
      m_tree->Branch("ISR_pt",    &m_ISR_pt ,  "ISR_pt/F");
      m_tree->Branch("ISR_eta",   &m_ISR_eta,  "ISR_eta/F");
      m_tree->Branch("ISR_phi",   &m_ISR_phi,  "ISR_phi/F");
      m_tree->Branch("ISR_E",     &m_ISR_E  ,  "ISR_E/F");
      m_tree->Branch("ISR_pdgId", &m_ISR_pdgId,"ISR_pdgId/I");
    }
  }

}

//////////////////// Connect Defined variables to branches here ///////////////////////////// 
void ZprimeMiniTree::AddPhotonsUser(const std::string& /*detailStr*/, const std::string& /*photonName*/)
{

  m_tree->Branch("ph_trig_match_HLT_g75_tight_3j25noL1_L1EM22VHI", &m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI);
  m_tree->Branch("ph_trig_match_HLT_g85_tight_L1EM22VHI_3j50noL1", &m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1);
  m_tree->Branch("ph_trig_match_HLT_g60_loose", &m_photons_HLT_g60_loose);
  m_tree->Branch("ph_trig_match_HLT_g140_loose", &m_photons_HLT_g140_loose);
  m_tree->Branch("ph_trig_match_HLT_g160_loose", &m_photons_HLT_g160_loose);

}

void ZprimeMiniTree::AddFatJetsUser(const std::string& /*detailStr*/, const std::string& /*fatjetName*/, const std::string& /*suffix*/)
{
  if(m_saveExtraVariables)
    {
      m_tree->Branch("fatjet_ANN_score", &m_fatjets_ANN_score);
    }
}

void ZprimeMiniTree::AddHLTPhotons()
{

  m_tree->Branch("HLT_ph_pt",  &m_HLT_ph_pt);
  m_tree->Branch("HLT_ph_eta", &m_HLT_ph_eta);
  m_tree->Branch("HLT_ph_phi", &m_HLT_ph_phi);

}

//////////////////// Clear any defined vectors here ////////////////////////////
void ZprimeMiniTree::ClearEventUser() {
  m_weight_corr = -999;
  m_weight    = -999;
  m_weight_xs = -999;

  if (m_doTruth) {
    m_Zprime_pt  = -999;
    m_Zprime_eta = -999;
    m_Zprime_phi = -999;
    m_Zprime_m   = -999;
    m_Zprime_pdg = -999;

    if(m_saveZprimeDecay){
      m_reso0_pt  = -999;
      m_reso0_eta = -999;
      m_reso0_phi = -999;
      m_reso0_E   = -999;
      
      m_reso1_pt  = -999;
      m_reso1_eta = -999;
      m_reso1_phi = -999;
      m_reso1_E   = -999;
      
      m_ISR_pt  = -999;
      m_ISR_eta = -999;
      m_ISR_phi = -999;
      m_ISR_E   = -999;
      m_ISR_pdgId   = -999;
    }
  }

}

//////////////////// Clear any defined vectors here //////////////////////////// 
void ZprimeMiniTree::ClearPhotonsUser(const std::string& /*photonName*/)
{
  m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI.clear();
  m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1.clear();
  m_photons_HLT_g60_loose.clear();
  m_photons_HLT_g140_loose.clear();
  m_photons_HLT_g160_loose.clear();
}

void ZprimeMiniTree::ClearFatJetsUser(const std::string& /*fatjetName*/, const std::string& /*suffix = ""*/)
{
  m_fatjets_ANN_score.clear();
}

void ZprimeMiniTree::ClearHLTPhotons(){
  
  m_HLT_ph_pt.clear();
  m_HLT_ph_eta.clear();
  m_HLT_ph_phi.clear();
  
}

/////////////////// Assign values to defined event variables here ////////////////////////
void ZprimeMiniTree::FillEventUser( const xAOD::EventInfo* eventInfo ) {
  if( eventInfo->isAvailable< float >( "weight" ) )
    m_weight = eventInfo->auxdecor< float >( "weight" );
  if( eventInfo->isAvailable< float >( "weight_xs" ) )
    m_weight_xs = eventInfo->auxdecor< float >( "weight_xs" );

  // truth Z' kinematic info
  if (m_doTruth) {
    if( eventInfo->isAvailable< float >( "Zprime_pt" ) )
      m_Zprime_pt = eventInfo->auxdecor< float >( "Zprime_pt" );
    if( eventInfo->isAvailable< float >( "Zprime_eta" ) )
      m_Zprime_eta = eventInfo->auxdecor< float >( "Zprime_eta" );
    if( eventInfo->isAvailable< float >( "Zprime_phi" ) )
      m_Zprime_phi = eventInfo->auxdecor< float >( "Zprime_phi" );
    if( eventInfo->isAvailable< float >( "Zprime_m" ) )
      m_Zprime_m = eventInfo->auxdecor< float >( "Zprime_m" );
    if( eventInfo->isAvailable< int   >( "Zprime_pdg" ) )
      m_Zprime_pdg = eventInfo->auxdecor< int   >( "Zprime_pdg" );
    
    if(m_saveZprimeDecay){
      if( eventInfo->isAvailable< float >( "reso0_pt" ) )
	m_reso0_pt = eventInfo->auxdecor< float >( "reso0_pt" );
      if( eventInfo->isAvailable< float >( "reso0_eta" ) )
	m_reso0_eta = eventInfo->auxdecor< float >( "reso0_eta" );
      if( eventInfo->isAvailable< float >( "reso0_phi" ) )
	m_reso0_phi = eventInfo->auxdecor< float >( "reso0_phi" );
      if( eventInfo->isAvailable< float >( "reso0_E" ) )
	m_reso0_E = eventInfo->auxdecor< float >( "reso0_E" );
      
      if( eventInfo->isAvailable< float >( "reso1_pt" ) )
	m_reso1_pt = eventInfo->auxdecor< float >( "reso1_pt" );
      if( eventInfo->isAvailable< float >( "reso1_eta" ) )
	m_reso1_eta = eventInfo->auxdecor< float >( "reso1_eta" );
      if( eventInfo->isAvailable< float >( "reso1_phi" ) )
	m_reso1_phi = eventInfo->auxdecor< float >( "reso1_phi" );
      if( eventInfo->isAvailable< float >( "reso1_E" ) )
	m_reso1_E = eventInfo->auxdecor< float >( "reso1_E" );
      
      if( eventInfo->isAvailable< float >( "ISR_pt" ) )
	m_ISR_pt = eventInfo->auxdecor< float >( "ISR_pt" );
      if( eventInfo->isAvailable< float >( "ISR_eta" ) )
	m_ISR_eta = eventInfo->auxdecor< float >( "ISR_eta" );
      if( eventInfo->isAvailable< float >( "ISR_phi" ) )
	m_ISR_phi = eventInfo->auxdecor< float >( "ISR_phi" );
      if( eventInfo->isAvailable< float >( "ISR_E" ) )
	m_ISR_E = eventInfo->auxdecor< float >( "ISR_E" );
      if( eventInfo->isAvailable< int >( "ISR_pdgId" ) )
	m_ISR_pdgId = eventInfo->auxdecor< int >( "ISR_pdgId" );
    } 
  }
}
void ZprimeMiniTree::FillPhotonsUser( const xAOD::Photon* ph_itr, const std::string& /*photonName*/) {

  static SG::AuxElement::ConstAccessor< char > HLT_g60 ("DFCommonTrigMatch_HLT_g60_loose" );
  static SG::AuxElement::ConstAccessor< char > HLT_g140("DFCommonTrigMatch_HLT_g140_loose");
  static SG::AuxElement::ConstAccessor< char > HLT_g160("DFCommonTrigMatch_HLT_g160_loose");

  static SG::AuxElement::ConstAccessor< char > HLT_g75 ("DFCommonTrigMatch_HLT_g75_tight_3j25noL1_L1EM22VHI");
  static SG::AuxElement::ConstAccessor< char > HLT_g85 ("DFCommonTrigMatch_HLT_g85_tight_L1EM22VHI_3j50noL1");

  if (HLT_g60.isAvailable( *ph_itr ))
    m_photons_HLT_g60_loose.push_back(HLT_g60( *ph_itr ));
  else
    m_photons_HLT_g60_loose.push_back(-1);

  if (HLT_g140.isAvailable( *ph_itr ))
    m_photons_HLT_g140_loose.push_back(HLT_g140( *ph_itr ));
  else
    m_photons_HLT_g140_loose.push_back(-1);

  if (HLT_g160.isAvailable( *ph_itr ))
    m_photons_HLT_g160_loose.push_back(HLT_g160( *ph_itr ));
  else
    m_photons_HLT_g160_loose.push_back(-1);

  if (HLT_g75.isAvailable( *ph_itr ))
    m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI.push_back(HLT_g75( *ph_itr ));
  else
    m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI.push_back(-1);

  if (HLT_g85.isAvailable( *ph_itr ))
    m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1.push_back(HLT_g85( *ph_itr ));
  else
    m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1.push_back(-1);

}

void ZprimeMiniTree::FillFatJetsUser(const xAOD::Jet* fatjet, int /*pvLocation = 0*/, const std::string& /*fatjetName = "fatjet"*/, const std::string& /*suffix = ""*/)
{
  static SG::AuxElement::ConstAccessor< float > ANNTagger_Score("ANNTagger_Score");

  if(m_saveExtraVariables)
    {
      if(ANNTagger_Score.isAvailable(*fatjet))
	m_fatjets_ANN_score.push_back(ANNTagger_Score(*fatjet));
      else
	m_fatjets_ANN_score.push_back(-999.);      
    }
}


void ZprimeMiniTree::FillHLTPhotons(const xAOD::PhotonContainer* photons)
{
  for(const xAOD::Photon* ph : *photons)
    {
      if(ph->pt() > 10000.)
	{
	  m_HLT_ph_pt.push_back(ph->pt()/1000.);
	  m_HLT_ph_eta.push_back(ph->eta());
	  m_HLT_ph_phi.push_back(ph->phi());
	}
    }
}
