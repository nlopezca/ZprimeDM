#include <ZprimeDM/CutflowHists.h>
#include <sstream>

CutflowHists :: CutflowHists (const std::string& name, const std::string& detailStr)
  : HistogramManager(name, detailStr)
{ }

CutflowHists :: ~CutflowHists () 
{ }

int CutflowHists::addCut(const std::string& cut)
{
  int binN=m_cutflow ->GetXaxis()->FindBin(cut.c_str());
  int binW=m_cutflowW->GetXaxis()->FindBin(cut.c_str());

  if(binN!=binW)
    std::cout << "Warning! nEvents and sumW bins don't match for " << cut << std::endl;

  return binN;
}

StatusCode CutflowHists::initialize() 
{
  // These plots are always made
  m_cutflow   = book(m_name, "cutflow", "cutflow", 1, 1, 2);
  m_cutflow ->SetCanExtend(TH1::kAllAxes);
  m_cutflowW  = book(m_name, "cutflow_weighted", "cutflow_weighted", 1, 1, 2);
  m_cutflowW->SetCanExtend(TH1::kAllAxes);

  m_cutflow ->GetXaxis()->FindBin("all");
  m_cutflowW->GetXaxis()->FindBin("all");

  return StatusCode::SUCCESS;
}

StatusCode CutflowHists::initialize(TH1F *cutflow, TH1F */*cutflowW*/) 
{
  // These plots are always made
  m_cutflow   = book(m_name, "cutflow", "cutflow", 1, 1, 2);
  m_cutflow ->SetCanExtend(TH1::kAllAxes);
  m_cutflowW  = book(m_name, "cutflow_weighted", "cutflow_weighted", 1, 1, 2);
  m_cutflowW->SetCanExtend(TH1::kAllAxes);

  for(int i=1; i<cutflow->GetNbinsX()+1; ++i)
    {
      const char* label=cutflow->GetXaxis()->GetBinLabel(i);
      if(label[0]=='\0') continue;
      m_cutflow ->GetXaxis()->FindBin(label);
      m_cutflowW->GetXaxis()->FindBin(label);
    }

  return StatusCode::SUCCESS;
}

StatusCode CutflowHists::executeInitial(float totalNevents, float totalWeight)
{
  m_cutflow ->Fill("all", totalNevents);
  m_cutflowW->Fill("all", totalWeight);

  return StatusCode::SUCCESS;
}

StatusCode CutflowHists::executeInitial(TH1F *cutflow, TH1F *cutflowW)
{
  int bin_all=cutflow->GetXaxis()->FindBin("all");
  float totalNevents=cutflow ->GetBinContent(bin_all);
  float totalWeight =cutflowW->GetBinContent(bin_all);

  m_cutflow ->Fill("all", totalNevents);
  m_cutflowW->Fill("all", totalWeight);

  return StatusCode::SUCCESS;
}

StatusCode CutflowHists::execute(const std::string& cut, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  m_cutflow ->Fill(cut.c_str(), 1);
  m_cutflowW->Fill(cut.c_str(), eventWeight);

  return StatusCode::SUCCESS;
}

StatusCode CutflowHists::execute(int cut, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  m_cutflow ->Fill(cut, 1);
  m_cutflowW->Fill(cut, eventWeight);

  return StatusCode::SUCCESS;
}

StatusCode CutflowHists::finalize(TH1F *cutflow, TH1F *cutflowW) 
{
  ANA_CHECK(HistogramManager::finalize());

  // These plots are always made
  for(int i=1; i<cutflow->GetNbinsX()+1; ++i)
    {
      const char* label=cutflow->GetXaxis()->GetBinLabel(i);
      if(label[0]=='\0') continue;
      m_cutflow ->SetBinContent(i, cutflow ->GetBinContent(i));
      m_cutflowW->SetBinContent(i, cutflowW->GetBinContent(i));
    }

  return StatusCode::SUCCESS;
}

StatusCode CutflowHists::write(TFile *fh)
{
  TH1F* cutflow =static_cast<TH1F*>(m_cutflow ->Clone());
  cutflow ->SetName("cutflow" );
  cutflow ->SetDirectory(fh);

  TH1F* cutflowW=static_cast<TH1F*>(m_cutflowW->Clone());
  cutflowW->SetName("cutflow_weighted");
  cutflowW->SetDirectory(fh);

  return StatusCode::SUCCESS;
}
