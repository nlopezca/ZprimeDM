#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include "PathResolver/PathResolver.h"

#include <AsgTools/MessageCheck.h>

#include <ZprimeDM/GammaJetHistsAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(GammaJetHistsAlgo)

GammaJetHistsAlgo :: GammaJetHistsAlgo () :
  m_mc(false),
  m_histDetailStr(""),
  m_jetDetailStr(""),
  m_photonDetailStr(""),
  m_doDetails(false),
  m_doBCIDCheck(false),
  m_doPUReweight(false),
  m_doCleaning(false),
  m_jetPtCleaningCut(25.),
  m_doTrigger(false),
  m_dumpTrig(false),
  m_trigger(""),
  m_photon0PtCut(25.),
  m_jet0PtCut(25.),
  m_ystarCut(-1),
  m_phTrigMatch(false),
  m_BCIDchecker(0),
  hIncl(nullptr)
{
  ANA_MSG_INFO("GammaJetHistsAlgo::GammaJetHistsAlgo()");
}

EL::StatusCode GammaJetHistsAlgo :: histInitialize ()
{
  ANA_CHECK(xAH::Algorithm::algInitialize());
  ANA_MSG_INFO("GammaJetHistsAlgo::histInitialize()");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_trigger =m_cutflow->addCut("trigger");
  m_cf_cleaning=m_cutflow->addCut("cleaning");
  m_cf_photon0 =m_cutflow->addCut("photon0");
  m_cf_jet0    =m_cutflow->addCut("jet0");
  if(m_ystarCut>0)  m_cf_ystar      =m_cutflow->addCut("ystar");
  if(m_phTrigMatch) m_cf_phTrigMatch=m_cutflow->addCut("PhTrigMatch");

  m_cutflow->record(wk());

  //
  // Histograms
  hIncl      =new GammaJetHists(m_name, m_histDetailStr, m_jetDetailStr, m_photonDetailStr);
  ANA_CHECK(hIncl->initialize());
  hIncl->record(wk());

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode GammaJetHistsAlgo :: initialize ()
{
  ANA_MSG_DEBUG("GammaJetHistsAlgo::initialize()");

  // Trigger
  std::string token;
  std::istringstream ss(m_trigger);

  while(std::getline(ss, token, ','))
    m_triggers.push_back(token);

  // BCID bug BS
  if(m_doBCIDCheck)
  {
    m_BCIDchecker=new BCIDBugChecker();
    m_BCIDchecker->addList(PathResolverFindDataFile("ZprimeDM/EventList_EGamma.txt"));
    m_BCIDchecker->addList(PathResolverFindDataFile("ZprimeDM/EventList_Jets.txt"));
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode GammaJetHistsAlgo :: execute ()
{
  ANA_MSG_DEBUG("GammaJetHistsAlgo::execute()");

  //
  // Cuts
  float eventWeight    =m_event->m_weight;
  if(m_mc && m_doPUReweight)
    eventWeight *= m_event->m_weight_pileup;

  //
  // do trigger
  if(m_doTrigger)
    {
      ANA_MSG_DEBUG("Doing Trigger");

      if(m_dumpTrig)
	{
	  for(std::string& thisTrig:*m_event->m_passedTriggers)
	    ANA_MSG_DEBUG("\t" << thisTrig);
	}

      //
      // trigger
      bool passTrigger=true;
      for(const std::string& trigger : m_triggers)
	passTrigger &= (std::find(m_event->m_passedTriggers->begin(), m_event->m_passedTriggers->end(), trigger ) != m_event->m_passedTriggers->end());

      if(!passTrigger)
	{
	  ANA_MSG_DEBUG(" Fail Trigger");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_trigger, eventWeight);
    }

  //
  // doing cleaning
  //
  bool  passCleaning   = true;
  if(!m_event->m_doTruthOnly)
    {
      for(unsigned int i = 0;  i<m_event->jets(); ++i)
	{
	  const xAH::Jet* jet=m_event->jet(i);
	  if(jet->p4.Pt() > m_jetPtCleaningCut)
	    {
	      if(!m_doCleaning && !jet->clean_passLooseBad)
		{
		  ANA_MSG_DEBUG(" Skipping jet clean");
		  continue;
		}
	      if(!jet->clean_passLooseBad) passCleaning = false;
	    }
	  else
	    break;
	}
    }

  //
  //  Jet Cleaning 
  //
  if(m_doCleaning && !passCleaning)
    {
      ANA_MSG_DEBUG(" Fail cleaning");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_cleaning, eventWeight);

  //
  // Photon part of the selection
  //
  const xAH::Photon* photon0=m_event->photon(0);
  const xAH::Jet*    jet0   =m_event->jet   (0);

  if(photon0->p4.Pt() < m_photon0PtCut)
    {
      ANA_MSG_DEBUG(" Fail PhotonPt0");
      return EL::StatusCode::SUCCESS;
    }
  if(m_mc) eventWeight*=photon0->TightEffSF;
  m_cutflow->execute(m_cf_photon0,eventWeight);

  if(jet0->p4.Pt() < m_jet0PtCut)
    {
      ANA_MSG_DEBUG(" Fail JetPt0");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_jet0,eventWeight);

  //
  // ystar cut
  //
  if(m_ystarCut>0)
    {
      float yStar = ( photon0->p4.Rapidity() - jet0->p4.Rapidity() ) / 2.0;
      if(fabs(yStar) > m_ystarCut){
	ANA_MSG_DEBUG(" Fail Ystar ");
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_ystar,eventWeight);
    }

  //
  // trigger matching
  if(m_phTrigMatch)
    {
      bool passTrigMatch=true;
      for(const std::string& trigger : m_triggers)
	passTrigMatch &= (std::find(photon0->trigMatched.begin(), photon0->trigMatched.end(), trigger ) != photon0->trigMatched.end());

      if(!passTrigMatch)
	{
	  ANA_MSG_DEBUG(" Fail Trigger Matching");
	  return EL::StatusCode::SUCCESS;
	}

      m_cutflow->execute(m_cf_phTrigMatch,eventWeight);
    }


  ANA_MSG_DEBUG(" Pass All Cut");

  if(m_doBCIDCheck)
    {
      if(m_BCIDchecker->check(m_event->m_runNumber,m_event->m_eventNumber))
	std::cout << "BCID BUG!!! runNumber = " << m_event->m_runNumber << ", eventNumber = " << m_event->m_eventNumber << std::endl;
    }

  //
  //Filling
  hIncl->execute(*m_event, photon0, jet0, eventWeight);

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode GammaJetHistsAlgo :: histFinalize ()
{
  if(m_doBCIDCheck) 
    {
      delete m_BCIDchecker;
      m_BCIDchecker=0;
    }

  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  ANA_CHECK(hIncl->finalize());
  delete hIncl;

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  return EL::StatusCode::SUCCESS;
}

