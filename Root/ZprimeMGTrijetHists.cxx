#include <ZprimeDM/ZprimeMGTrijetHists.h>

ZprimeMGTrijetHists :: ZprimeMGTrijetHists (const std::string& name, const std::string& detailStr) 
  : HistogramManager(name, detailStr)
{ }

ZprimeMGTrijetHists :: ~ZprimeMGTrijetHists () 
{}

void ZprimeMGTrijetHists::record(EL::IWorker* wk)
{
  HistogramManager::record(wk);

  m_jet0->record(wk);
  m_jet1->record(wk);
  m_jet2->record(wk);
}

StatusCode ZprimeMGTrijetHists::initialize()
{
  std::cout << "ZprimeMGTrijetHists::initialize()" << std::endl;
  HistogramManager::initialize();

  // Jets
  m_jet0=new ZprimeDM::TruthHists(m_name+"jet0_" , "kinematic", "leading jet");
  ANA_CHECK(m_jet0->initialize());

  m_jet1=new ZprimeDM::TruthHists(m_name+"jet1_" , "kinematic", "subleading jet");
  ANA_CHECK(m_jet1->initialize());

  m_jet2=new ZprimeDM::TruthHists(m_name+"jet2_" , "kinematic", "third jet");
  ANA_CHECK(m_jet2->initialize());

  // These plots are always made
  h_ptjj     = book(m_name, "ptjj",      "p_{T,jj} [GeV]", 100,            0,     500   );
  h_ptjj_l   = book(m_name, "ptjj_l",    "p_{T,jj} [GeV]", 100,            0,    1000   );
  h_etajj    = book(m_name, "etajj",     "#eta_{jj}",      100,           -4,       4   );
  h_phijj    = book(m_name, "phijj",     "#phi_{jj}",      100, -TMath::Pi(),TMath::Pi());

  h_mjj      = book(m_name, "mjj",       "m_{jj} [GeV]",      500,           0,    5000 );

  h_dEtajj        = book(m_name, "dEtajj", "|#Delta#eta_{jj}|", 100, 0,           4);
  h_dPhijj        = book(m_name, "dPhijj", "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());
  h_dRjj          = book(m_name, "dRjj"  , "#DeltaR_{jj}"     , 100, 0,           5);

  h_yStarjj       = book(m_name, "ystarjj"       , "y^{*}_{jj}"         ,  90,  0  ,  3);
  h_yBoostjj      = book(m_name, "yboostjj"      , "y^{boost}_{jj}"     ,  90,  0  ,  3);
  h_chijj         = book(m_name, "chijj"         , "#chi_{jj}"          , 120,  0  , 60);

  h_asymjj        = book(m_name, "asymjj",     "(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)",    120,  0, 1);
  h_vecasymjj     = book(m_name, "vecasymjj",  "|#vec{p}_{T}^{1}-#vec{p}_{T}^{2}|/|#vec{p}_{T}^{1}+#vec{p}_{T}^{2}|",120, 0, 7);
  h_projasymjj    = book(m_name, "projasymjj", "(#vec{p}_{T}^{1}-#vec{p}_{T}^{2})#upoint#hat{p}_{T}^{jj}/p_{T}^{jj}", 50, 0, 5);

  //
  // ISR stuff
  h_yStarISRjj    = book(m_name, "yStarISRjj",     "y^{*}_{ISR,jj}",        90,      0,       3   );
  h_yBoostISRjj   = book(m_name, "yBoostISRjj",    "y^{boost}_{ISR,jj}",    90,      0,       3   );
  h_chiISRjj      = book(m_name, "chiISRjj",       "#chi_{ISR,jj}",        100,      0,      60   );

  h_dEtaISRclosej = book(m_name, "dEtaISRclosej", "|#Delta#eta_{ISR,close-j}|", 100, 0,           4);
  h_dPhiISRclosej = book(m_name, "dPhiISRclosej", "|#Delta#phi_{ISR,close-j}|", 100, 0, TMath::Pi());
  h_dRISRclosej   = book(m_name, "dRISRclosej",   "#DeltaR_{ISR,close-j}",      100, 0,           5);

  h_dEtaISRclosephij = book(m_name, "dEtaISRclosephij", "|#Delta#eta_{ISR,close-#phi-j}|", 100, 0,           4);
  h_dPhiISRclosephij = book(m_name, "dPhiISRclosephij", "|#Delta#phi_{ISR,close-#phi-j}|", 100, 0, TMath::Pi());
  h_dRISRclosephij   = book(m_name, "dRISRclosephij",   "#DeltaR_{ISR,close-#phi-j}",      100, 0,           5);

  //
  // Random energy
  h_ht   = book(m_name, "ht",   "h_{T} [GeV]",      100, 0, 2000);
  
  h_pt0isr=book(m_name, "pt0isr", "p_{T,r1}/p_{T,ISR}", 50, 0, 2);
  h_pt1isr=book(m_name, "pt1isr", "p_{T,r2}/p_{T,ISR}", 50, 0, 2);
  h_pt1pt0=book(m_name, "pt1pt0", "p_{T,r2}/p_{T,r1}" , 50, 0, 2);

  //
  // Unboosted stuff
  h_dEtaZBoost = book(m_name, "dEtaZBoost", "|#Delta#eta_{Z',close-rest-j}|", 100, 0,           4);
  h_dPhiZBoost = book(m_name, "dPhiZBoost", "|#Delta#phi_{Z',close-rest-j}|", 100, 0, TMath::Pi());
  h_dRZBoost   = book(m_name, "dRZBoost",   "#DeltaR_{Z',close-rest-j}",      100, 0,           5);

  h_dEtaZBoostQ= book(m_name, "dEtaZBoostQ","|#Delta#eta_{Z',rest-q}|", 100, 0,           4);
  h_dPhiZBoostQ= book(m_name, "dPhiZBoostQ","|#Delta#phi_{Z',rest-q}|", 100, 0, TMath::Pi());
  h_dRZBoostQ  = book(m_name, "dRZBoostQ",  "#DeltaR_{Z',rest-q}",      100, 0,           5);

  return StatusCode::SUCCESS;
}

StatusCode ZprimeMGTrijetHists::execute(const xAH::TruthPart* reso0, const xAH::TruthPart* reso1, const xAH::TruthPart* isr, float eventWeight) 
{
  //
  // Jets
  const xAH::TruthPart* jets[3] = {reso0, reso1, isr};
  std::sort(std::begin(jets),std::end(jets),ZprimeHelperClasses::TruthSorter::sort);

  ANA_CHECK(m_jet0->execute(jets[0], eventWeight));
  ANA_CHECK(m_jet1->execute(jets[1], eventWeight));
  ANA_CHECK(m_jet2->execute(jets[2], eventWeight));

  //
  // Resonance info
  TLorentzVector p4_Zprime=reso0->p4+reso1->p4;

  h_ptjj    ->Fill(p4_Zprime.Pt() , eventWeight);
  h_ptjj_l  ->Fill(p4_Zprime.Pt() , eventWeight);
  h_etajj   ->Fill(p4_Zprime.Eta(), eventWeight);
  h_phijj   ->Fill(p4_Zprime.Phi(), eventWeight);
  h_mjj     ->Fill(p4_Zprime.M()  , eventWeight);

  // Deltas
  float dEtajj = fabs(reso0->p4.Eta()-reso1->p4.Eta());
  h_dEtajj ->Fill(dEtajj , eventWeight);

  float dPhijj = fabs(reso0->p4.DeltaPhi(reso1->p4));
  h_dPhijj ->Fill(dPhijj , eventWeight);

  float dRjj = reso0->p4.DeltaR(reso1->p4);
  h_dRjj   ->Fill(dRjj  , eventWeight);

  // Y's
  double yStarjj =fabs(reso0->p4.Rapidity()-reso1->p4.Rapidity())/2.;
  h_yStarjj ->Fill(yStarjj                   , eventWeight);

  double yBoostjj=fabs(reso0->p4.Rapidity()+reso1->p4.Rapidity())/2.;
  h_yBoostjj->Fill(yBoostjj                  , eventWeight);

  double chijj=exp(2*fabs(yStarjj));
  h_chijj   ->Fill(chijj                     , eventWeight);

  // Asym
  double asymjj = (reso0->p4.Pt()-reso1->p4.Pt())/(reso0->p4.Pt()+reso1->p4.Pt());
  h_asymjj->Fill( asymjj, eventWeight);

  double vecasymjj = (reso0->p4-reso1->p4).Pt()/(reso0->p4+reso1->p4).Pt();
  h_vecasymjj->Fill( vecasymjj, eventWeight);

  TVector3 p3_Zprime=p4_Zprime.Vect();
  p3_Zprime.SetZ(0);
  p3_Zprime.SetMag(1.);
  double projasymjj = ((reso0->p4-reso1->p4).Vect().Dot(p3_Zprime))/p4_Zprime.Pt();
  h_projasymjj->Fill( projasymjj, eventWeight);

  //
  // Stuff to do with ISR
  const xAH::TruthPart* closej=(isr->p4.DeltaR(reso0->p4)<isr->p4.DeltaR(reso1->p4))?reso0:reso1;
  //std::cout << isr->p4.DeltaR(reso0->p4) << " < " << isr->p4.DeltaR(reso1->p4) << " = " << isr->p4.DeltaR(closej->p4) << std::endl;

  h_dEtaISRclosej->Fill(fabs(isr->p4.Eta()-closej->p4.Eta()), eventWeight);
  h_dPhiISRclosej->Fill(fabs(isr->p4.DeltaPhi(closej->p4))  , eventWeight);
  h_dRISRclosej  ->Fill(isr->p4.DeltaR(closej->p4)          , eventWeight);

  const xAH::TruthPart* closephij=(fabs(isr->p4.DeltaPhi(reso0->p4))<fabs(isr->p4.DeltaR(reso1->p4)))?reso0:reso1;
  //std::cout << isr->p4.DeltaR(reso0->p4) << " < " << isr->p4.DeltaR(reso1->p4) << " = " << isr->p4.DeltaR(closej->p4) << std::endl;

  h_dEtaISRclosephij->Fill(fabs(isr->p4.Eta()-closephij->p4.Eta()), eventWeight);
  h_dPhiISRclosephij->Fill(fabs(isr->p4.DeltaPhi(closephij->p4))  , eventWeight);
  h_dRISRclosephij  ->Fill(isr->p4.DeltaR(closephij->p4)          , eventWeight);

  //
  // Stuff to do with Z'
  float yStarISRjj=fabs(isr->p4.Rapidity()-p4_Zprime.Rapidity())/2.;
  h_yStarISRjj ->Fill(yStarISRjj, eventWeight);

  float yBoostISRjj=fabs(isr->p4.Rapidity()+p4_Zprime.Rapidity())/2.;
  h_yBoostISRjj->Fill(yBoostISRjj  , eventWeight);

  h_chiISRjj   ->Fill(exp(2*fabs(yStarISRjj))        , eventWeight);

  //
  // Random energy
  h_ht->Fill(reso0->p4.Pt()+reso1->p4.Pt()+isr->p4.Pt(), eventWeight);

  h_pt0isr->Fill(reso0->p4.Pt()/isr  ->p4.Pt(), eventWeight);
  h_pt1isr->Fill(reso1->p4.Pt()/isr  ->p4.Pt(), eventWeight);
  h_pt1pt0->Fill(reso1->p4.Pt()/reso0->p4.Pt(), eventWeight);

  //
  // Unboosted stuff
  TVector3 zboost=p4_Zprime.BoostVector();
  TLorentzVector p4_zboost=p4_Zprime;
  TLorentzVector p4_boost0=reso0->p4;
  TLorentzVector p4_boost1=reso1->p4;

  p4_zboost.Boost(-zboost);
  p4_boost0.Boost(-zboost);
  p4_boost1.Boost(-zboost);
  
  TLorentzVector p4_quark=(fabs(p4_Zprime.DeltaPhi(p4_boost0))<fabs(p4_Zprime.DeltaPhi(p4_boost1)))?p4_boost0:p4_boost1;
  //std::cout << "CHOICES " << fabs(p4_Zprime.DeltaPhi(p4_boost0)) << " " << fabs(p4_Zprime.DeltaPhi(p4_boost1)) << std::endl;
  h_dEtaZBoost->Fill(fabs(p4_quark.Eta()-p4_Zprime.Eta()), eventWeight);
  h_dPhiZBoost->Fill(fabs(p4_quark.DeltaPhi(p4_Zprime))  , eventWeight);
  h_dRZBoost  ->Fill(p4_quark.DeltaR(p4_Zprime)          , eventWeight);

  p4_quark=(reso0->pdgId>0)?p4_boost0:p4_boost1;
  //std::cout << "To Quark " << fabs(p4_quark.DeltaPhi(p4_Zprime)) << std::endl;
  h_dEtaZBoostQ->Fill(fabs(p4_quark.Eta()-p4_Zprime.Eta()), eventWeight);
  h_dPhiZBoostQ->Fill(fabs(p4_quark.DeltaPhi(p4_Zprime))  , eventWeight);
  h_dRZBoostQ  ->Fill(p4_quark.DeltaR(p4_Zprime)          , eventWeight);

  return StatusCode::SUCCESS;
}

StatusCode ZprimeMGTrijetHists::finalize()
{
  ANA_CHECK(HistogramManager::finalize());

  ANA_CHECK(m_jet0->finalize());
  ANA_CHECK(m_jet1->finalize());
  ANA_CHECK(m_jet2->finalize());

  return StatusCode::SUCCESS;
}
