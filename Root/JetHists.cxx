#include <ZprimeDM/JetHists.h>

using namespace ZprimeDM;

JetHists :: JetHists (const std::string& name, const std::string& detailStr, const std::string& prefix)
  : HistogramManager(name, detailStr), m_debug(false), m_infoSwitch(detailStr), m_prefix(prefix)
{ }

JetHists :: ~JetHists () 
{ }

StatusCode JetHists::initialize()
{

  // kinematic
  if(m_infoSwitch.m_kinematic)
    {
      h_pt   = book(m_name, "pt"    , m_prefix+" jet p_{T} [GeV]", 100, 0,  500);
      h_pt_m = book(m_name, "pt_m"  , m_prefix+" jet p_{T} [GeV]", 100, 0, 1000);
      h_pt_l = book(m_name, "pt_l"  , m_prefix+" jet p_{T} [GeV]", 100, 0, 5000);

      h_eta  = book(m_name, "eta"   , m_prefix+" jet #eta", 60, -3          , 3);
      h_phi  = book(m_name, "phi"   , m_prefix+" jet #phi", 50, -TMath::Pi(), TMath::Pi());

      h_vr   = book(m_name, "vr"    , m_prefix+" jet VR"  , 40,  0          , 0.4);
    }

  // clean
  if( m_infoSwitch.m_clean ) {
    h_Timing                     =book(m_name, "JetTiming" ,                 m_prefix+" jet Timing [ns]",                120, -50,   50);
    h_LArQuality                 =book(m_name, "LArQuality" ,                m_prefix+" jet LAr Quality",                120, -10,   10);
    h_HECQuality                 =book(m_name, "HECQuality" ,                m_prefix+" jet HEC Quality",                120, -10,   10);
    h_NegativeE                  =book(m_name, "NegativeE" ,                 m_prefix+" jet Negative Energy",            120, -10,    0);
    h_AverageLArQF               =book(m_name, "AverageLArQF" ,              m_prefix+" jet Abs <LAr Quality Factor>" ,  120,   0, 1000);
    h_LArQmean                   =book(m_name, "LArQmean" ,                  m_prefix+" jet <LAr Quality Factor>" ,      120,   0,    1);
    h_BchCorrCell                =book(m_name, "BchCorrCell" ,               m_prefix+" jet BCH Corr Cell" ,             120,   0,    0.1);
    h_N90Constituents            =book(m_name, "N90Constituents",            m_prefix+" jet N90 Constituents" ,          40,   -0.5, 39.5);
    h_LArBadHVEnergyFrac         =book(m_name, "LArBadHVEnergyFrac",         m_prefix+" jet LAr Bad HV Energy Fraction", 120,   0,    1);
    h_LArBadHVNCell              =book(m_name, "LArBadHVNCell",              m_prefix+" jet LAr Bad HV N_{cells}",       120,  -0.5,499.5);
    h_OotFracClusters5           =book(m_name, "OotFracClusters5",           m_prefix+" jet OotFracClusters5" ,          120,   0,    1);
    h_OotFracClusters10          =book(m_name, "OotFracClusters10",          m_prefix+" jet OotFracClusters10" ,         120,   0,    1);
    h_LeadingClusterPt           =book(m_name, "LeadingClusterPt",           m_prefix+" jet Leading Cluster P_{T}" ,     120,   0, 1000);
    h_LeadingClusterSecondLambda =book(m_name, "LeadingClusterSecondLambda", m_prefix+" jet LeadingClusterSecondLambda", 120,   0, 1000e3);
    h_LeadingClusterCenterLambda =book(m_name, "LeadingClusterCenterLambda", m_prefix+" jet LeadingClusterCenterLambda", 120,   0, 5000);
    h_LeadingClusterSecondR      =book(m_name, "LeadingClusterSecondR",      m_prefix+" jet LeadingClusterSecondR" ,     120,   0, 300e3);
    h_clean_passLooseBad         =book(m_name, "clean_passLooseBad",         m_prefix+" jet LooseBad Cleaning Flag" ,      2,  -0.5,  1.5);
    h_clean_passLooseBadUgly     =book(m_name, "clean_passLooseBadUgly",     m_prefix+" jet LooseBadUgly Cleaning Flag",   2,  -0.5,  1.5);
    h_clean_passTightBad         =book(m_name, "clean_passTightBad",         m_prefix+" jet TightBad Cleaning Flag" ,      2,  -0.5,  1.5);
    h_clean_passTightBadUgly     =book(m_name, "clean_passTightBadUgly",     m_prefix+" jet TightBadUgly Cleaning Flag",   2,  -0.5,  1.5);
  }

  // energy
  if(m_infoSwitch.m_energy)
    {
      h_HECFrac               = book(m_name, "HECFrac",               m_prefix+" jet HEC Fraction" ,          120,  0,      1.5);
      h_EMFrac                = book(m_name, "EMFrac",                m_prefix+" jet EM Fraction" ,           120,  0,      1.5);
      h_CentroidR             = book(m_name, "CentroidR",             m_prefix+" jet CentroidR [cm]" ,        120,  0,   5000);
      h_FracSamplingMax       = book(m_name, "FracSamplingMax",       m_prefix+" jet FracSamplingMax" ,       120,  0,      1);
      h_FracSamplingMaxIndex  = book(m_name, "FracSamplingMaxIndex",  m_prefix+" jet FracSamplingMaxIndex" ,   22, -0.5,   21.5);
      h_LowEtConstituentsFrac = book(m_name, "LowEtConstituentsFrac", m_prefix+" jet LowEtConstituentsFrac" , 120,  0,      1);
      h_GhostMuonSegmentCount = book(m_name, "GhostMuonSegmentCount", m_prefix+" jet GhostMuonSegmentCount" ,  10, -0.5,    9.5);
      h_Width                 = book(m_name, "Width",                 m_prefix+" jet Width",                  100, 0, 0.5);
    }

  // trackPV
  if(m_infoSwitch.m_trackPV)
    {
      h_NumTrkPt1000PV     = book(m_name, "NumTrkPt1000PV",   m_prefix+" jet N_{trk,P_{T}>1 GeV}"     ,  50, -0.5, 49.5);
      h_SumPtTrkPt1000PV   = book(m_name, "SumPtTrkPt1000PV", m_prefix+" jet #sum_{trk,P_{T}>1 GeV}"  , 100,    0, 1000);
      h_TrackWidthPt1000PV = book(m_name, "TrackWidthPt1000P",m_prefix+" jet w_{trk,P_{T}>1 GeV}"     , 100,    0, 0.5);
      h_NumTrkPt500PV      = book(m_name, "NumTrkPt500PV",    m_prefix+" jet N_{trk,P_{T}>500 MeV}"   ,  50, -0.5, 49.5);
      h_SumPtTrkPt500PV    = book(m_name, "SumPtTrkPt500PV",  m_prefix+" jet #sum_{trk,P_{T}>500 MeV}", 100,    0, 1000);
      h_TrackWidthPt500PV  = book(m_name, "TrackWidthPt500P", m_prefix+" jet w_{trk,P_{T}>500 MeV}"   , 100,    0, 0.5);
      h_JVFPV              = book(m_name, "JVFPV",            m_prefix+" jet JVF_{PV}"                , 100,    0, 0.5);
    }

  // trackAll or trackPV
  if(m_infoSwitch.m_trackAll || m_infoSwitch.m_trackPV)
    {
      h_Jvt       = book(m_name, "Jvt",        m_prefix+" jet JVT"       , 100, -0.2, 1);
      h_JvtJvfcorr= book(m_name, "JvtJvfcorr", m_prefix+" jet corrJVF"   , 100, -1  , 1);
      h_JvtRpt    = book(m_name, "JvtRpt",     m_prefix+" jet R_{p_{T}}" , 100,  0  , 1.5);
    }

  // flavorTag
  if(m_infoSwitch.m_flavorTag)
    {
      h_SV0                       = book(m_name, "SV0",                       m_prefix+" jet SV0"          , 100,  -20  , 20);
      h_SV1                       = book(m_name, "SV1",                       m_prefix+" jet SV1"          , 100,  -20  , 20);
      h_IP3D                      = book(m_name, "IP3D",                      m_prefix+" jet IP3D"         , 100,  -20  , 20);
      h_SV1plusIP3D_discriminant  = book(m_name, "SV1plusIP3D_discriminant",  m_prefix+" jet SV1+IP3D"     , 100,  -20  , 20);
      h_MV1                       = book(m_name, "MV1",                       m_prefix+" jet MV1"          , 100,   -1  ,  1);
      h_MV2c00                    = book(m_name, "MV2c00",                    m_prefix+" jet MV2c00"       , 100,   -1  ,  1);
      h_MV2c10                    = book(m_name, "MV2c10",                    m_prefix+" jet MV2c10"       , 100,   -1  ,  1);
      h_MV2c10mu                  = book(m_name, "MV2c10mu",                  m_prefix+" jet MV2c10mu"     , 100,   -1  ,  1);
      h_MV2c10rnn                 = book(m_name, "MV2c10rnn",                 m_prefix+" jet MV2c10rnn"    , 100,   -1  ,  1);
      h_MV2c20                    = book(m_name, "MV2c20",                    m_prefix+" jet MV2c20"       , 100,   -1  ,  1);
      h_DL1                       = book(m_name, "DL1",                       m_prefix+" jet DL1"          ,  80,   -5  , 11);
      h_DL1_pu                    = book(m_name, "DL1_pu",                    m_prefix+" jet DL1 p_{u}"    , 100,    0  ,  1);
      h_DL1_pc                    = book(m_name, "DL1_pc",                    m_prefix+" jet DL1 p_{c}"    , 100,    0  ,  1);
      h_DL1_pb                    = book(m_name, "DL1_pb",                    m_prefix+" jet DL1 p_{b}"    , 100,    0  ,  1);
      h_DL1mu                     = book(m_name, "DL1mu",                     m_prefix+" jet DL1mu"        ,  80,   -5  , 11);
      h_DL1mu_pu                  = book(m_name, "DL1mu_pu",                  m_prefix+" jet DL1mu p_{u}"  , 100,    0  ,  1);
      h_DL1mu_pc                  = book(m_name, "DL1mu_pc",                  m_prefix+" jet DL1mu p_{c}"  , 100,    0  ,  1);
      h_DL1mu_pb                  = book(m_name, "DL1mu_pb",                  m_prefix+" jet DL1mu p_{b}"  , 100,    0  ,  1);
      h_DL1rnn                    = book(m_name, "DL1rnn",                    m_prefix+" jet DL1rnn"       ,  80,   -5  , 11);
      h_DL1rnn_pu                 = book(m_name, "DL1rnn_pu",                 m_prefix+" jet DL1rnn p_{u}" , 100,    0  ,  1);
      h_DL1rnn_pc                 = book(m_name, "DL1rnn_pc",                 m_prefix+" jet DL1rnn p_{c}" , 100,    0  ,  1);
      h_DL1rnn_pb                 = book(m_name, "DL1rnn_pb",                 m_prefix+" jet DL1rnn p_{b}" , 100,    0  ,  1);
      h_HadronConeExclTruthLabelID= book(m_name, "HadronConeExclTruthLabelID",m_prefix+" jet Truth Label"  ,  30,   -0.5, 29.5);
    }

  // truth
  if(m_infoSwitch.m_truth)
    {
    std::cout << "making truth hists now" << std::endl;
//      h_ConeTruthLabelID  = book(m_name, "ConeTruthLabelID", m_prefix+" jet ConeTruthLabelID", 100, -20, 20);
      h_TruthCount        = book(m_name, "TruthCount      ", m_prefix+" jet TruthCount", 100, -20, 20);
      h_TruthLabelDeltaR_B= book(m_name, "TruthLabelDeltaR_B", m_prefix+" jet TruthLabelDeltaR_B", 100, 0, 10);
      h_TruthLabelDeltaR_C= book(m_name, "TruthLabelDeltaR_C", m_prefix+" jet TruthLabelDeltaR_C", 100, 0, 10);
      h_TruthLabelDeltaR_T= book(m_name, "TruthLabelDeltaR_T", m_prefix+" jet TruthLabelDeltaR_T", 100, 0, 10);
      h_PartonTruthLabelID= book(m_name, "PartonTruthLabelID", m_prefix+" jet PartonTruthLabelID",  22, -0.5, 21.5);
      h_GhostTruthAssociationFraction= book(m_name, "GhostTruthAssociationFraction", m_prefix+" jet GhostTruthAssociationFraction", 100, 0, 1);

      h_truth_pt   = book(m_name, "truth_pt"    , m_prefix+" jet truth p_{T} [GeV]", 100, 0, 500);
      h_truth_pt_m = book(m_name, "truth_pt_m"  , m_prefix+" jet truth p_{T} [GeV]", 100, 0, 1000);
      h_truth_pt_l = book(m_name, "truth_pt_l"  , m_prefix+" jet truth p_{T} [GeV]", 100, 0, 5000);

      h_truth_eta  = book(m_name, "truth_eta"   , m_prefix+" jet truth #eta", 100, -3          , 3);
      h_truth_phi  = book(m_name, "truth_phi"   , m_prefix+" jet truth #phi", 100, -TMath::Pi(), TMath::Pi());
    }

  // charge
  if(m_infoSwitch.m_charge)
    {
      h_charge= book(m_name, "charge", m_prefix+" jet charge", 100, -10, 10);
    }


  return StatusCode::SUCCESS;
}

StatusCode JetHists::execute(const xAH::Jet* jet, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  if(m_infoSwitch.m_kinematic)
    {
      h_pt   ->Fill(jet->p4.Pt() ,eventWeight);
      h_pt_m ->Fill(jet->p4.Pt() ,eventWeight);
      h_pt_l ->Fill(jet->p4.Pt() ,eventWeight);

      h_eta  ->Fill(jet->p4.Eta(),eventWeight);
      h_phi  ->Fill(jet->p4.Phi(),eventWeight);

      h_vr   ->Fill(std::max(0.02, std::min(0.4, 30/jet->p4.Pt())), eventWeight);
    }

  if(m_infoSwitch.m_clean)
    {
      h_Timing                    ->Fill(jet->Timing                    ,eventWeight);
      h_LArQuality                ->Fill(jet->LArQuality                ,eventWeight);
      h_HECQuality                ->Fill(jet->HECQuality                ,eventWeight);
      h_NegativeE                 ->Fill(jet->NegativeE                 ,eventWeight);
      h_AverageLArQF              ->Fill(jet->AverageLArQF              ,eventWeight);
      h_LArQmean                  ->Fill(jet->AverageLArQF/65535        ,eventWeight);
      h_BchCorrCell               ->Fill(jet->BchCorrCell               ,eventWeight);
      h_N90Constituents           ->Fill(jet->N90Constituents           ,eventWeight);
      h_LArBadHVEnergyFrac        ->Fill(jet->LArBadHVEFrac             ,eventWeight);
      h_LArBadHVNCell             ->Fill(jet->LArBadHVNCell             ,eventWeight);
      h_OotFracClusters5          ->Fill(jet->OotFracClusters5          ,eventWeight);
      h_OotFracClusters10         ->Fill(jet->OotFracClusters10         ,eventWeight);
      h_LeadingClusterPt          ->Fill(jet->LeadingClusterPt          ,eventWeight);
      h_LeadingClusterSecondLambda->Fill(jet->LeadingClusterSecondLambda,eventWeight);
      h_LeadingClusterCenterLambda->Fill(jet->LeadingClusterCenterLambda,eventWeight);
      h_LeadingClusterSecondR     ->Fill(jet->LeadingClusterSecondR     ,eventWeight);
      h_clean_passLooseBad        ->Fill(jet->clean_passLooseBad        ,eventWeight);
      h_clean_passLooseBadUgly    ->Fill(jet->clean_passLooseBadUgly    ,eventWeight);
      h_clean_passTightBad        ->Fill(jet->clean_passTightBad        ,eventWeight);
      h_clean_passTightBadUgly    ->Fill(jet->clean_passTightBadUgly    ,eventWeight);
    }

  if(m_infoSwitch.m_energy)
    {
      h_HECFrac              ->Fill(jet->HECFrac,              eventWeight);
      h_EMFrac               ->Fill(jet->EMFrac,               eventWeight);
      h_CentroidR            ->Fill(jet->CentroidR,            eventWeight);
      h_FracSamplingMax      ->Fill(jet->FracSamplingMax,      eventWeight);
      h_FracSamplingMaxIndex ->Fill(jet->FracSamplingMaxIndex, eventWeight);
      h_LowEtConstituentsFrac->Fill(jet->LowEtConstituentsFrac,eventWeight);
      h_GhostMuonSegmentCount->Fill(jet->GhostMuonSegmentCount,eventWeight);
      h_Width                ->Fill(jet->Width,                eventWeight);
    }

  if(m_infoSwitch.m_trackPV)
    {
      h_NumTrkPt1000PV    ->Fill(jet->NumTrkPt1000PV    , eventWeight);
      h_SumPtTrkPt1000PV  ->Fill(jet->SumPtTrkPt1000PV  , eventWeight);
      h_TrackWidthPt1000PV->Fill(jet->TrackWidthPt1000PV, eventWeight);
      h_NumTrkPt500PV     ->Fill(jet->NumTrkPt500PV     , eventWeight);
      h_SumPtTrkPt500PV   ->Fill(jet->SumPtTrkPt500PV   , eventWeight);
      h_TrackWidthPt500PV ->Fill(jet->TrackWidthPt500PV , eventWeight);
      h_JVFPV             ->Fill(jet->JVFPV             , eventWeight);
    }

  if(m_infoSwitch.m_trackPV || m_infoSwitch.m_trackAll)
    {
      h_Jvt       ->Fill(jet->Jvt        , eventWeight);
      h_JvtJvfcorr->Fill(jet->JvtJvfcorr , eventWeight);
      h_JvtRpt    ->Fill(jet->JvtRpt     , eventWeight);
    }

  if(m_infoSwitch.m_flavorTag)
    {
      h_SV0                       ->Fill(jet->SV0                  , eventWeight);
      h_SV1                       ->Fill(jet->SV1                  , eventWeight);
      h_IP3D                      ->Fill(jet->IP3D                 , eventWeight);
      h_SV1plusIP3D_discriminant  ->Fill(jet->SV1IP3D              , eventWeight);
      h_MV1                       ->Fill(jet->MV1                  , eventWeight);
      h_MV2c00                    ->Fill(jet->MV2c00               , eventWeight);
      h_MV2c10                    ->Fill(jet->MV2c10               , eventWeight);
      h_MV2c10mu                  ->Fill(jet->MV2c10mu             , eventWeight);
      h_MV2c10rnn                 ->Fill(jet->MV2c10rnn            , eventWeight);
      h_MV2c20                    ->Fill(jet->MV2c20               , eventWeight);
      h_DL1                       ->Fill(jet->DL1                  , eventWeight);
      h_DL1_pu                    ->Fill(jet->DL1_pu               , eventWeight);
      h_DL1_pc                    ->Fill(jet->DL1_pc               , eventWeight);
      h_DL1_pb                    ->Fill(jet->DL1_pb               , eventWeight);
      h_DL1mu                     ->Fill(jet->DL1mu                , eventWeight);
      h_DL1mu_pu                  ->Fill(jet->DL1mu_pu             , eventWeight);
      h_DL1mu_pc                  ->Fill(jet->DL1mu_pc             , eventWeight);
      h_DL1mu_pb                  ->Fill(jet->DL1mu_pb             , eventWeight);
      h_DL1rnn                    ->Fill(jet->DL1rnn               , eventWeight);
      h_DL1rnn_pu                 ->Fill(jet->DL1rnn_pu            , eventWeight);
      h_DL1rnn_pc                 ->Fill(jet->DL1rnn_pc            , eventWeight);
      h_DL1rnn_pb                 ->Fill(jet->DL1rnn_pb            , eventWeight);
      h_HadronConeExclTruthLabelID->Fill(jet->HadronConeExclTruthLabelID, eventWeight);
    }

  // truth
  if(m_infoSwitch.m_truth)
    {
//      h_ConeTruthLabelID  ->Fill(jet->ConeTruthLabelID  , eventWeight);
      h_TruthCount        ->Fill(jet->TruthCount        , eventWeight);
      h_TruthLabelDeltaR_B->Fill(jet->TruthLabelDeltaR_B, eventWeight);
      h_TruthLabelDeltaR_C->Fill(jet->TruthLabelDeltaR_C, eventWeight);
      h_TruthLabelDeltaR_T->Fill(jet->TruthLabelDeltaR_T, eventWeight);
      h_PartonTruthLabelID->Fill(jet->PartonTruthLabelID, eventWeight);
      h_GhostTruthAssociationFraction->Fill(jet->GhostTruthAssociationFraction, eventWeight);

      h_truth_pt   ->Fill(jet->truth_p4.Pt(),  eventWeight);
      h_truth_pt_m ->Fill(jet->truth_p4.Pt(),  eventWeight);
      h_truth_pt_l ->Fill(jet->truth_p4.Pt(),  eventWeight);

      h_truth_eta  ->Fill(jet->truth_p4.Eta(), eventWeight);
      h_truth_phi  ->Fill(jet->truth_p4.Phi(), eventWeight);
    }

  // charge
  if(m_infoSwitch.m_charge)
    {
      h_charge->Fill(jet->charge, eventWeight);
    }


  return StatusCode::SUCCESS;
}
