#include "ZprimeDM/DijetISREvent.h"

#include <iostream>

DijetISREvent* DijetISREvent::m_event=0;

DijetISREvent* DijetISREvent::global()
{
  if(DijetISREvent::m_event==0)
    DijetISREvent::m_event=new DijetISREvent();
  return m_event;
}

DijetISREvent::DijetISREvent()
{ }

DijetISREvent::~DijetISREvent()
{ }

unsigned int DijetISREvent::isPassBits(const std::string& trigger) const
{
  for(uint i=0;i<m_isPassBitsNames->size();i++)
    if(trigger==m_isPassBitsNames->at(i))
      return m_isPassBits->at(i);
  return 0;
}


void DijetISREvent::setJets(std::vector<xAH::Jet> *jets)
{ m_jets=jets; }

std::vector<xAH::Jet>* DijetISREvent::getJets() const
{ return m_jets; }

bool DijetISREvent::haveJets() const
{ return m_jets!=nullptr; }

uint DijetISREvent::jets() const
{ return (m_jets)?m_jets->size():0; }

const xAH::Jet* DijetISREvent::jet(uint idx) const
{ return &m_jets->at(idx); }


void DijetISREvent::setPhotons(std::vector<xAH::Photon> *photons)
{ m_photons=photons; }

std::vector<xAH::Photon>* DijetISREvent::getPhotons() const
{ return m_photons; }

bool DijetISREvent::havePhotons() const
{ return m_photons!=nullptr; }

uint DijetISREvent::photons() const
{ return (m_photons)?m_photons->size():0; }

const xAH::Photon* DijetISREvent::photon(uint idx) const
{ return &m_photons->at(idx);  }


void DijetISREvent::setMuons(std::vector<xAH::Muon> *muons)
{ m_muons=muons; }

std::vector<xAH::Muon>* DijetISREvent::getMuons() const
{ return m_muons; }

bool DijetISREvent::haveMuons() const
{ return m_muons!=nullptr; }

uint DijetISREvent::muons() const
{ return (m_muons)?m_muons->size():0; }

const xAH::Muon* DijetISREvent::muon(uint idx) const
{ return &m_muons->at(idx);  }


void DijetISREvent::setElectrons(std::vector<xAH::Electron> *electrons)
{ m_electrons=electrons; }

std::vector<xAH::Electron>* DijetISREvent::getElectrons() const
{ return m_electrons; }

bool DijetISREvent::haveElectrons() const
{ return m_electrons!=nullptr; }

uint DijetISREvent::electrons() const
{ return (m_electrons)?m_electrons->size():0; }

const xAH::Electron* DijetISREvent::electron(uint idx) const
{ return &m_electrons->at(idx);  }


void DijetISREvent::setFatJets(std::vector<xAH::FatJet> *fatjets)
{ m_fatjets=fatjets; }

std::vector<xAH::FatJet>* DijetISREvent::getFatJets() const
{ return m_fatjets; }

bool DijetISREvent::haveFatJets() const
{ return m_fatjets!=nullptr; }

uint DijetISREvent::fatjets() const
{ return (m_fatjets)?m_fatjets->size():0; }

const xAH::FatJet* DijetISREvent::fatjet(uint idx) const
{ return &m_fatjets->at(idx);  }


void DijetISREvent::setTrigJets(std::vector<xAH::Jet> *trigjets)
{ m_trigJets=trigjets; }

std::vector<xAH::Jet>* DijetISREvent::getTrigJets() const
{ return m_trigJets; }

bool DijetISREvent::haveTrigJets() const
{ return m_trigJets!=nullptr; }

uint DijetISREvent::trigjets() const
{ return (m_trigJets)?m_trigJets->size():0; }

const xAH::Jet* DijetISREvent::trigjet(uint idx) const
{ return &m_trigJets->at(idx);  }


void DijetISREvent::setTruths(std::vector<xAH::TruthPart> *truths)
{ m_truths=truths; }

std::vector<xAH::TruthPart>* DijetISREvent::getTruths() const
{ return m_truths; }

bool DijetISREvent::haveTruths() const
{ return m_truths!=nullptr; }

uint DijetISREvent::truths() const
{ return (m_truths)?m_truths->size():0; }

const xAH::TruthPart* DijetISREvent::truth(uint idx) const
{ return &m_truths->at(idx);  }
