#include <ZprimeDM/DijetISRHists.h>


DijetISRHists :: DijetISRHists (const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& photonDetailStr)
  : EventHists(name, detailStr),
    m_infoSwitch(detailStr),
    m_detailStr(detailStr), m_Zprime(0), m_ZprimeTruth(0), m_ZprimeISR(0), m_dbg(0),
    m_jetDetailStr(jetDetailStr), h_nJet(0), m_jet0(0), m_jet1(0), m_jet2(0), m_jet3(0),
    m_photonDetailStr(photonDetailStr), h_nPhoton(0), m_photon0(0)
{ }

DijetISRHists :: ~DijetISRHists () 
{ }

void DijetISRHists::record(EL::IWorker* wk)
{
  EventHists::record(wk);

  m_Zprime     ->record(wk);
  if(m_infoSwitch.m_truthz) m_ZprimeTruth->record(wk);
  m_ZprimeISR  ->record(wk);
  if(m_infoSwitch.m_debug) m_dbg         ->record(wk);

  if(!m_jetDetailStr.empty())
    {
      m_jet0    ->record(wk);
      m_jet1    ->record(wk);
      m_jet2    ->record(wk);
      m_jet3    ->record(wk);
    }

  if(!m_photonDetailStr.empty())
    {
      m_photon0 ->record(wk);
    }
}

StatusCode DijetISRHists::initialize()
{
  if(m_debug) std::cout << "DijetISRHists::initialize()" << std::endl;

  ANA_CHECK(EventHists::initialize());

  m_Zprime        =new ZprimeResonanceHists(m_name+"Zprime_", m_detailStr);
  ANA_CHECK(m_Zprime->initialize());

  if(m_infoSwitch.m_truthz)
    {
      m_ZprimeTruth=new ZprimeTruthResonanceHists(m_name+"ZprimeTruth_", m_detailStr);
      ANA_CHECK(m_ZprimeTruth->initialize());
    }

  m_ZprimeISR     =new ZprimeISRHists(m_name+"ZprimeISR_"   , m_detailStr);
  ANA_CHECK(m_ZprimeISR->initialize());

  if(m_infoSwitch.m_debug)
    {
      m_dbg           =new DebugHists(m_name+"Debug_"   , "");
      ANA_CHECK(m_dbg->initialize());
    }

  if(!m_jetDetailStr.empty())
    {
      h_nJet= book(m_name, "nJets",     "N_{jets}",      10,     -0.5,     9.5 );

      m_jet0=new ZprimeDM::JetHists(m_name+"jet0_" , m_jetDetailStr, "leading");
      ANA_CHECK(m_jet0->initialize());

      m_jet1=new ZprimeDM::JetHists(m_name+"jet1_" , m_jetDetailStr, "subleading");
      ANA_CHECK(m_jet1->initialize());

      m_jet2=new ZprimeDM::JetHists(m_name+"jet2_" , m_jetDetailStr, "third");
      ANA_CHECK(m_jet2->initialize());

      m_jet3=new ZprimeDM::JetHists(m_name+"jet3_" , m_jetDetailStr, "fourth");
      ANA_CHECK(m_jet3->initialize());
    }

  if(!m_photonDetailStr.empty())
    {
      h_nPhoton= book(m_name, "nPhotons",  "N_{#gamma}",    10,     -0.5,     9.5 );

      m_photon0=new ZprimeDM::PhotonHists(m_name+"photon0_", m_photonDetailStr, "leading");
      ANA_CHECK(m_photon0->initialize());
    }

  return StatusCode::SUCCESS;
}

StatusCode DijetISRHists::execute(const DijetISREvent& event, const xAH::Jet *reso0, const xAH::Jet *reso1, const xAH::Particle *isr, float eventWeight)
{
  if(m_debug) std::cout << "DijetISRHists::execute()" << std::endl;
  ANA_CHECK(EventHists::execute(event, eventWeight));

  ANA_CHECK(m_Zprime->execute(reso0, reso1     , eventWeight));
  if(m_infoSwitch.m_truthz)
    ANA_CHECK(m_ZprimeTruth->execute(event.m_Zprime   , eventWeight));
  if(isr!=0)
    ANA_CHECK(m_ZprimeISR  ->execute(reso0, reso1, isr, eventWeight));
  if(m_infoSwitch.m_debug)
    ANA_CHECK(m_dbg        ->execute(reso0, reso1, isr, eventWeight));

  if(!m_jetDetailStr.empty())
    {
      h_nJet   ->Fill(event.jets()   , eventWeight);

      if(event.jets()>0)    ANA_CHECK(m_jet0   ->execute(event.jet(0)   , eventWeight));
      if(event.jets()>1)    ANA_CHECK(m_jet1   ->execute(event.jet(1)   , eventWeight));
      if(event.jets()>2)    ANA_CHECK(m_jet2   ->execute(event.jet(2)   , eventWeight));
      if(event.jets()>3)    ANA_CHECK(m_jet3   ->execute(event.jet(3)   , eventWeight));
    }

  if(!m_photonDetailStr.empty())
    {
      h_nPhoton->Fill(event.photons(), eventWeight);

      if(event.photons()>0) ANA_CHECK(m_photon0->execute(event.photon(0), eventWeight));
    }

  return StatusCode::SUCCESS;
}

StatusCode DijetISRHists::finalize()
{
  if(m_debug) std::cout << "DijetISRHists::finalize()" << std::endl;
  
  ANA_CHECK(EventHists::finalize());

  ANA_CHECK(m_Zprime     ->finalize());
  delete m_Zprime;
  if(m_infoSwitch.m_truthz)
    {
      ANA_CHECK(m_ZprimeTruth->finalize());
      delete m_ZprimeTruth;
    }
  ANA_CHECK(m_ZprimeISR  ->finalize());
  delete m_ZprimeISR;
  if(m_infoSwitch.m_debug)
    {
      ANA_CHECK(m_dbg        ->finalize());
      delete m_dbg;
    }

  if(!m_jetDetailStr.empty())
    {
      ANA_CHECK(m_jet0   ->finalize());
      ANA_CHECK(m_jet1   ->finalize());
      ANA_CHECK(m_jet2   ->finalize());
      ANA_CHECK(m_jet3   ->finalize());

      delete m_jet0;
      delete m_jet1;
      delete m_jet2;
      delete m_jet3;
    }

  if(!m_photonDetailStr.empty())
    {
      ANA_CHECK(m_photon0->finalize());

      delete m_photon0;
    }

  return StatusCode::SUCCESS;
}
