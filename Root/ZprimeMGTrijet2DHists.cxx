#include <ZprimeDM/ZprimeMGTrijet2DHists.h>

ZprimeMGTrijet2DHists :: ZprimeMGTrijet2DHists (const std::string& name, const std::string& detailStr) 
  : HistogramManager(name, detailStr)
{ }

ZprimeMGTrijet2DHists :: ~ZprimeMGTrijet2DHists () 
{}

StatusCode ZprimeMGTrijet2DHists::initialize()
{
  std::cout << "ZprimeMGTrijet2DHists::initialize()" << std::endl;
  HistogramManager::initialize();

  h_m13vsm23 = book(m_name, "m13vsm23","m_{23} [GeV]", 100, 0, 1000,
		                       "m_{13} [GeV]", 100, 0, 1000);

  h_asymjj13vsasymjj23 = book(m_name, "asymjj13vsasymjj23","(|p_{T}^{2}|-|p_{T}^{3}|)/(|p_{T}^{2}|+|p_{T}^{3}|)", 100, 0, 1,
			                                   "(|p_{T}^{1}|-|p_{T}^{3}|)/(|p_{T}^{1}|+|p_{T}^{3}|)", 100, 0, 1);

  return StatusCode::SUCCESS;
}

StatusCode ZprimeMGTrijet2DHists::execute(const xAH::TruthPart* jets[3], float eventWeight)
{
  double m23=(jets[1]->p4+jets[2]->p4).M();
  double m13=(jets[0]->p4+jets[2]->p4).M();

  h_m13vsm23->Fill(m23, m13, eventWeight);

  // Asym
  double asymjj13 = (jets[0]->p4.Pt()-jets[2]->p4.Pt())/(jets[0]->p4.Pt()+jets[2]->p4.Pt());
  double asymjj23 = (jets[1]->p4.Pt()-jets[2]->p4.Pt())/(jets[1]->p4.Pt()+jets[2]->p4.Pt());

  h_asymjj13vsasymjj23->Fill(asymjj23, asymjj13, eventWeight);

  return StatusCode::SUCCESS;
}
