#include <ZprimeDM/ZprimeHistsBaseAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <xAODAnaHelpers/HelperFunctions.h>

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeHistsBaseAlgo)

ZprimeHistsBaseAlgo :: ZprimeHistsBaseAlgo () :
  m_mc(false),
  m_histDetailStr(""),
  m_jetDetailStr(""),
  m_photonDetailStr(""),
  m_doDetails(false),
  m_doBCIDCheck(false),
  m_doPUReweight(false),
  m_doCleaning(false),
  m_jetPtCleaningCut(25.),
  m_doSysts(0),
  m_doTrigger(false),
  m_dumpTrig(false),
  m_trigger(""),
  m_reso0PtCut(25.),
  m_reso0PtCutMax(-1.0),
  m_reso1PtCut(25.),
  m_nresoBtag(0),
  m_resoBtagAlgo(xAH::Jet::BTaggerOP::None),
  m_ptjjCut(0.),
  m_ystarCut(-1),
  m_YBoostCut(-1),
  m_dPhijjCut(0),
  m_asymjjCut(0),
  m_dRISRclosejCut(0),
  m_dPhiISRclosejCut(0),
  m_yStarISRjjCut(0),
  m_pt1ptisrCut(0),
  m_mjjCut(0),
  m_survivingEvents(0),
  m_BCIDchecker(0),
  hIncl(nullptr),
  hMjj100_200(nullptr),
  hMjj150_250(nullptr),
  hMjj200_300(nullptr),
  hMjj250_350(nullptr),
  hMjj300_400(nullptr),
  hMjj350_450(nullptr),
  hMjj400_500(nullptr),
  hMjj450_550(nullptr),
  hMjj500_600(nullptr),
  hMjj700_800(nullptr)
{
  ANA_MSG_INFO("ZprimeHistsBaseAlgo::ZprimeHistsBaseAlgo()");
}

EL::StatusCode ZprimeHistsBaseAlgo :: histInitialize ()
{
  ANA_CHECK(xAH::Algorithm::algInitialize());
  ANA_MSG_INFO("ZprimeHistsBaseAlgo::histInitialize()");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_trigger    =m_cutflow->addCut("trigger");
  m_cf_cleaning   =m_cutflow->addCut("cleaning");
  initISRCutflow();

  m_cf_reso0    =m_cutflow->addCut("reso0");
  m_cf_reso0_max=m_cutflow->addCut("reso0_max");
  m_cf_reso1    =m_cutflow->addCut("reso1");

  if(m_nresoBtag>0)        m_cf_nresoBtag    =m_cutflow->addCut("nresoBtag");

  if(m_ptjjCut>0)          m_cf_ptjj         =m_cutflow->addCut("ptjj");
  if(m_ystarCut>0)         m_cf_ystar        =m_cutflow->addCut("ystar");
  if(m_YBoostCut>0)        m_cf_yboost       =m_cutflow->addCut("yboost");
  if(m_dRISRclosejCut>0)   m_cf_drisrclosej  =m_cutflow->addCut("drisrclosej");
  if(m_dPhiISRclosejCut>0) m_cf_dphiisrclosej=m_cutflow->addCut("dphiisrclosej");
  if(m_yStarISRjjCut>0)    m_cf_ystarisrjj   =m_cutflow->addCut("ystarisrjj");
  if(m_dPhijjCut>0)        m_cf_dphijj       =m_cutflow->addCut("dphijj");
  if(m_asymjjCut>0)        m_cf_asymjj       =m_cutflow->addCut("asymjj");
  if(m_projasymjjCut>0)    m_cf_projasymjj   =m_cutflow->addCut("projasymjj");
  if(m_pt1ptisrCut>0)      m_cf_pt1ptisr     =m_cutflow->addCut("pt1ptisr");
  if(m_mjjCut>0)           m_cf_mjj          =m_cutflow->addCut("mjj");

  m_cutflow->record(wk());

  //
  // Histograms
  hIncl      =new DijetISRHists(m_name, m_histDetailStr, m_jetDetailStr, m_photonDetailStr);
  ANA_CHECK(hIncl->initialize());
  hIncl->record(wk());

  // Initialise histograms for bSFSystHists
  // Not doing this smartly -- sorry future reader
  // But I think there are never more than ~55 uncertainty terms
  // So I'm just making 65 of these guys and then there should be enough
  m_bSFSystHists.clear();
  if (m_doSysts && m_nresoBtag < 3) {
    for (int i=1; i<66; i++) {
      TString thisName = Form("/variedBTagSF_%i/",i);
      DijetISRHists * thisHist = new DijetISRHists(m_name+thisName.Data(), m_histDetailStr, m_jetDetailStr, m_photonDetailStr);
      m_bSFSystHists.push_back(thisHist);
      ANA_CHECK(m_bSFSystHists.back()->initialize());
      m_bSFSystHists.back()->record(wk());
    }
  }

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  hMjj100_150=new DijetISRHists(m_name+"/Mjj100to150/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj100_150->initialize());
  hMjj100_150->record(wk());

  hMjj100_200=new DijetISRHists(m_name+"/Mjj100to200/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj100_200->initialize());
  hMjj100_200->record(wk());

  hMjj150_250=new DijetISRHists(m_name+"/Mjj150to250/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj150_250->initialize());
  hMjj150_250->record(wk());

  hMjj200_300=new DijetISRHists(m_name+"/Mjj200to300/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj200_300->initialize());
  hMjj200_300->record(wk());

  hMjj250_350=new DijetISRHists(m_name+"/Mjj250to350/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj250_350->initialize());
  hMjj250_350->record(wk());

  hMjj300_400=new DijetISRHists(m_name+"/Mjj300to400/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj300_400->initialize());
  hMjj300_400->record(wk());

  hMjj350_450=new DijetISRHists(m_name+"/Mjj350to450/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj350_450->initialize());
  hMjj350_450->record(wk());

  hMjj400_500=new DijetISRHists(m_name+"/Mjj400to500/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj400_500->initialize());
  hMjj400_500->record(wk());

  hMjj450_550=new DijetISRHists(m_name+"/Mjj450to550/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj450_550->initialize());
  hMjj450_550->record(wk());

  hMjj500_600=new DijetISRHists(m_name+"/Mjj500to600/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj500_600->initialize());
  hMjj500_600->record(wk());

  hMjj700_800=new DijetISRHists(m_name+"/Mjj700to800/", "", "kinematic", m_photonDetailStr);
  ANA_CHECK(hMjj700_800->initialize());
  hMjj700_800->record(wk());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeHistsBaseAlgo :: initialize ()
{
  ANA_MSG_DEBUG("ZprimeHistsBaseAlgo::initialize()");

  // Trigger
  std::string token;
  std::istringstream ss(m_trigger);

  while(std::getline(ss, token, ','))
    m_triggers.push_back(token);

  // BCID bug BS
  if(m_doBCIDCheck)
  {
    m_BCIDchecker=new BCIDBugChecker();
    m_BCIDchecker->addList(PathResolverFindDataFile("ZprimeDM/MistimedEvents/EventList_EGamma.txt"));
    m_BCIDchecker->addList(PathResolverFindDataFile("ZprimeDM/MistimedEvents/EventList_Jets.txt"));
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeHistsBaseAlgo :: execute ()
{
  ANA_MSG_DEBUG("ZprimeHistsBaseAlgo::execute()");

  //
  // Cuts
  m_eventWeight    =m_event->m_weight;
  if(m_mc && m_doPUReweight)
    m_eventWeight *= m_event->m_weight_pileup;

  //
  // do trigger
  if(m_doTrigger)
    {
      ANA_MSG_DEBUG("Doing Trigger");

      if(m_dumpTrig)
	{
	  for(std::string& thisTrig:*m_event->m_passedTriggers)
	    ANA_MSG_DEBUG("\t" << thisTrig);
	}

      //
      // trigger
      bool passTrigger=true;
      for(const std::string& trigger : m_triggers)
	passTrigger &= (std::find(m_event->m_passedTriggers->begin(), m_event->m_passedTriggers->end(), trigger ) != m_event->m_passedTriggers->end());

      if(!passTrigger)
	{
	  ANA_MSG_DEBUG(" Fail Trigger");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_trigger, m_eventWeight);
    }

  //
  // doing cleaning
  //
  bool  passCleaning   = true;
  if(!m_event->m_doTruthOnly)
    {
      for(unsigned int i = 0;  i<m_event->jets(); ++i)
	{
	  const xAH::Jet* jet=m_event->jet(i);
	  if(jet->p4.Pt() > m_jetPtCleaningCut)
	    {
	      if(!m_doCleaning && !jet->clean_passLooseBad)
		{
		  ANA_MSG_DEBUG(" Skipping jet clean");
		  continue;
		}
	      if(!jet->clean_passLooseBad) passCleaning = false;
	    }
	  else
	    break;
	}
    }

  //
  //  Jet Cleaning 
  //
  if(m_doCleaning && !passCleaning)
    {
      ANA_MSG_DEBUG(" Fail cleaning");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_cleaning, m_eventWeight);

  //
  // ISR part of the selection
  //
  if(!doISRCutflow())
    return EL::StatusCode::SUCCESS;

  //
  // Resonance part of the selection
  //

  // pt cuts
  if(m_reso0->p4.Pt() < m_reso0PtCut)
    {
      ANA_MSG_DEBUG(" Fail ResoPt0 with " << m_reso0->p4.Pt());
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_reso0,m_eventWeight);

  if((m_reso0PtCutMax > 0) && (m_reso0->p4.Pt() > m_reso0PtCutMax))
    {
      ANA_MSG_DEBUG(" Fail ResoPt0 Max Cut with " << m_reso0->p4.Pt());
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_reso0_max,m_eventWeight);

  if(m_reso1->p4.Pt() < m_reso1PtCut)
    {
      ANA_MSG_DEBUG(" Fail JetPt1 with " << m_reso1->p4.Pt());
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_reso1,m_eventWeight);

  // The scale factor does not affect the cutflow, so we don't need to
  // change anything in the workflow.
  // FIXME: doesn't run on systematic trees atm
  std::vector<float> variedBTagSFs;
  float eventWeight_noBSF = m_eventWeight;
  if (m_nresoBtag<3)
    {
      bool isB0=0, isB1=0;
      isB0 = m_reso0->is_btag(m_resoBtagAlgo);
      isB1 = m_reso1->is_btag(m_resoBtagAlgo);
      float sfB     = m_reso0->SF_btag(m_resoBtagAlgo)[0] * m_reso1->SF_btag(m_resoBtagAlgo)[0];
      // Record systematic variations
      for (unsigned int i=1; i<m_reso0->SF_btag(m_resoBtagAlgo).size(); i++) {
        variedBTagSFs.push_back(m_reso0->SF_btag(m_resoBtagAlgo)[i] * m_reso1->SF_btag(m_resoBtagAlgo)[i]);
      }
      if (m_nresoBtag==0)
    	{
	  if (!(!isB0 && !isB1)) 
	    {
	      ANA_MSG_DEBUG("Fail m_nresoBtag");
	      return EL::StatusCode::SUCCESS;
	    }
	}
      if (m_nresoBtag==1)
	{
	  if (!((isB0 && !isB1)||( isB1 && !isB0)))
	    {
	      ANA_MSG_DEBUG("Fail m_nresoBtag");
	      return EL::StatusCode::SUCCESS;
	    }
	}
      if (m_nresoBtag==2)
	{
	  if (!(isB0 && isB1)) 
	    {
	      ANA_MSG_DEBUG("Fail m_nresoBtag");
	      return EL::StatusCode::SUCCESS;
	    }
	}
      
      m_eventWeight *= sfB;
      m_cutflow->execute(m_cf_nresoBtag, m_eventWeight);
    }

  // jj cuts
  TLorentzVector jj = ( m_reso0->p4 + m_reso1->p4 ); // Build jj object

  if(m_ptjjCut>0)
    {
      if(jj.Pt() < m_ptjjCut)
	{
	  ANA_MSG_DEBUG(" Fail Ptjj with " << jj.Pt());
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_ptjj,m_eventWeight);
    }

  if(m_ystarCut>0)
    {
      double yStar = ( m_reso0->p4.Rapidity() - m_reso1->p4.Rapidity() ) / 2.0;
      if(fabs(yStar) > m_ystarCut){
	ANA_MSG_DEBUG(" Fail Ystar with " << fabs(yStar));
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_ystar,m_eventWeight);
    }

  if(m_YBoostCut>0)
    {
      float yBoost = ( m_reso0->p4.Rapidity() + m_reso1->p4.Rapidity() ) / 2.0;
      if(fabs(yBoost) > m_YBoostCut){
	ANA_MSG_DEBUG(" Fail YBoost with " << fabs(yBoost));
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_yboost,m_eventWeight);
    }

  if(m_dPhijjCut>0)
    {
      double dPhijj = fabs(m_reso0->p4.DeltaPhi(m_reso1->p4));
      if(dPhijj > m_dPhijjCut){
	ANA_MSG_DEBUG(" Fail dPhijj with " << dPhijj);
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_dphijj, m_eventWeight);
    }

  if(m_asymjjCut>0)
    {
      double asymjj = (m_reso0->p4.Pt()-m_reso1->p4.Pt())/(m_reso0->p4.Pt()+m_reso1->p4.Pt());
      if(asymjj > m_asymjjCut){
	ANA_MSG_DEBUG(" Fail asymjj with " << asymjj);
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_asymjj, m_eventWeight);
    }

  if(m_projasymjjCut>0)
    {
      TVector3 p3_Zprime=jj.Vect();
      p3_Zprime.SetZ(0);
      p3_Zprime.SetMag(1.);
      double projasymjj = ((m_reso0->p4-m_reso1->p4).Vect().Dot(p3_Zprime))/jj.Pt();

      if(projasymjj > m_projasymjjCut){
	ANA_MSG_DEBUG(" Fail projasymjj with " << projasymjj);
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_projasymjj, m_eventWeight);
    }

  // close by jet cuts
  const xAH::Jet* closej=0;
  if(m_isr)
    closej=(m_isr->p4.DeltaR(m_reso0->p4)<m_isr->p4.DeltaR(m_reso1->p4))?m_reso0:m_reso1;

  if(m_dRISRclosejCut>0)
    {
      float dRISRclosej = m_isr->p4.DeltaR(closej->p4);
      if(fabs(dRISRclosej) < m_dRISRclosejCut){
	ANA_MSG_DEBUG(" Fail dRISRclosej with " << fabs(dRISRclosej));
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_drisrclosej,m_eventWeight);
    }

  if(m_dPhiISRclosejCut>0)
    {
      float dPhiISRclosej = m_isr->p4.DeltaPhi(closej->p4);
      if(fabs(dPhiISRclosej) < m_dPhiISRclosejCut){
	ANA_MSG_DEBUG(" Fail dPhiISRclosej with " << fabs(dPhiISRclosej));
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_dphiisrclosej,m_eventWeight);
    }

  // ISR jj cuts
  if(m_yStarISRjjCut>0)
    {
      float ystarISRjj = fabs(m_isr->p4.Rapidity()-jj.Rapidity())/2.;
      if(ystarISRjj > m_yStarISRjjCut ){
	ANA_MSG_DEBUG(" Fail YStarISRjjCut with " << ystarISRjj);
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_ystarisrjj,m_eventWeight);
    }

  // pT cuts
  if(m_pt1ptisrCut>0)
    {
      double pt1ptisr=m_reso1->p4.Pt()/m_isr->p4.Pt();
      if(pt1ptisr < m_pt1ptisrCut) {
	ANA_MSG_DEBUG(" Fail pt1ptisr with " << pt1ptisr);
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_pt1ptisr,m_eventWeight);
    }

  // mjj cut
  float mjj = jj.M();
  if(mjj < m_mjjCut){
    ANA_MSG_DEBUG(" Fail mjj with " << mjj);
    return EL::StatusCode::SUCCESS;
  }
  m_cutflow->execute(m_cf_mjj,m_eventWeight);

  ANA_MSG_DEBUG(" Pass All Cuts");

  if(m_doBCIDCheck)
    {
      if(m_BCIDchecker->check(m_event->m_runNumber,m_event->m_eventNumber))
	std::cout << "BCID BUG!!! runNumber = " << m_event->m_runNumber << ", eventNumber = " << m_event->m_eventNumber << std::endl;
    }

  ANA_CHECK(histFill(m_eventWeight));
  m_survivingEvents++;

  // Fill varied histograms with b-tag SFs
  if (m_doSysts && m_nresoBtag < 3) {
    // Doing it this way means it still works if I test with fewer histograms....
    for (unsigned int i=0; i<std::min(variedBTagSFs.size(),m_bSFSystHists.size()); i++) {
      float thisSF = variedBTagSFs.at(i);
      ANA_CHECK(histFill(eventWeight_noBSF*thisSF, m_bSFSystHists.at(i)));
    }
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeHistsBaseAlgo::histFill(float eventWeight, DijetISRHists* systHist)
{

  // If a particular set of histograms are given, only fill those.
  if (systHist) {
    systHist->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);
    return EL::StatusCode::SUCCESS;
  }
  
  // Else, continue to the normal set of hists.

  //
  //Filling nominal
  hIncl->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  TLorentzVector jj = m_reso0->p4 + m_reso1->p4;
  float mjj = jj.M();
  
  if(mjj > 100 && mjj < 150)
    hMjj100_150->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);

  if(mjj > 100 && mjj < 200)
    hMjj100_200->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);
  else if(mjj > 200 && mjj < 300)
    hMjj200_300->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);
  else if(mjj > 300 && mjj < 400)
    hMjj300_400->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);
  else if(mjj > 400 && mjj < 500)
    hMjj400_500->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);
  else if(mjj > 500 && mjj < 600)
    hMjj500_600->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);
  else if(mjj > 700 && mjj < 800)
    hMjj700_800->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);

  if(mjj > 150 && mjj < 250)
    hMjj150_250->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);
  else if(mjj > 250 && mjj < 350)
    hMjj250_350->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);
  else if(mjj > 350 && mjj < 450)
    hMjj350_450->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);
  else if(mjj > 450 && mjj < 550)
    hMjj450_550->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeHistsBaseAlgo::histFinalize()
{
  ANA_MSG_INFO("ZprimeHistsBaseAlgo::histFinalize()");
  if (m_doBCIDCheck)
    {
      delete m_BCIDchecker;
      m_BCIDchecker = 0;
    }

  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  ANA_CHECK(hIncl->finalize());
  delete hIncl;

  for (DijetISRHists* hists : m_bSFSystHists)
  {
    ANA_CHECK(hists->finalize());
    delete hists;
  }
  m_bSFSystHists.clear();

  if (!m_doDetails) return EL::StatusCode::SUCCESS;

  ANA_CHECK(hMjj100_150->finalize());
  delete hMjj100_150;

  ANA_CHECK(hMjj100_200->finalize());
  delete hMjj100_200;

  ANA_CHECK(hMjj150_250->finalize());
  delete hMjj150_250;

  ANA_CHECK(hMjj200_300->finalize());
  delete hMjj200_300;

  ANA_CHECK(hMjj250_350->finalize());
  delete hMjj250_350;

  ANA_CHECK(hMjj300_400->finalize());
  delete hMjj300_400;

  ANA_CHECK(hMjj350_450->finalize());
  delete hMjj350_450;

  ANA_CHECK(hMjj400_500->finalize());
  delete hMjj400_500;

  ANA_CHECK(hMjj450_550->finalize());
  delete hMjj450_550;

  ANA_CHECK(hMjj500_600->finalize());
  delete hMjj500_600;

  ANA_CHECK(hMjj700_800->finalize());
  delete hMjj700_800;

  ANA_MSG_INFO("End of ZprimeHistsBaseAlgo::histFinalize()");

  return EL::StatusCode::SUCCESS;
}

