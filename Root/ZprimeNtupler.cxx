#include <ZprimeDM/ZprimeNtupler.h>

#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include <AthContainers/ConstDataVector.h>

#include <SampleHandler/MetaFields.h>

#include <xAODTracking/VertexContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODTrigger/JetRoIContainer.h>
#include <xAODEventInfo/EventInfo.h>

#include <BoostedJetTaggers/JSSWTopTaggerANN.h>
#include <PMGTools/PMGCrossSectionTool.h>

#include <xAODAnaHelpers/HelperFunctions.h>

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeNtupler)

ZprimeNtupler :: ZprimeNtupler (const std::string& className) :
ZprimeAlgorithm(className),
  m_cleanPileup(false),m_treeStream("tree")
{
  ANA_MSG_INFO("ZprimeNtupler::ZprimeNtupler()");
}

EL::StatusCode ZprimeNtupler :: setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init( "ZprimeNtupler" ).ignore(); // call before opening first file

  EL::OutputStream outForTree( m_treeStream );
  job.outputAdd (outForTree);
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeNtupler :: initialize ()
{
  ANA_MSG_DEBUG("ZprimeNtupler::initialize()");
  ANA_CHECK(ZprimeAlgorithm::initialize());

  m_eventCounter = -1;

  //
  // event info
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK(HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));
  m_isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
  uint32_t dsid= m_isMC ? eventInfo->mcChannelNumber() : -1;

  //
  // x-sec tool
  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_PMGCrossSectionTool_handle, PMGTools::PMGCrossSectionTool) );
  ANA_CHECK( m_PMGCrossSectionTool_handle.retrieve() );
  m_PMGCrossSectionTool_handle->readInfosFromDir(PathResolverFindCalibDirectory("dev/PMGTools"));
  std::vector<int> loadedDSIDs=m_PMGCrossSectionTool_handle->getLoadedDSIDs();

  m_weight_xs=1.;
  if(m_isMC)
    {
      double xs;
      double eff;
      double kfactor;
      if( std::find(loadedDSIDs.begin(),loadedDSIDs.end(),dsid)!=loadedDSIDs.end() )
	{ // Have data, use it
	  xs     =m_PMGCrossSectionTool_handle->getAMIXsection(dsid);
	  eff    =m_PMGCrossSectionTool_handle->getFilterEff  (dsid);
	  kfactor=m_PMGCrossSectionTool_handle->getKfactor    (dsid);
	}
      else
	{ // Try sample information
	  xs     =wk()->metaData()->castDouble(SH::MetaFields::crossSection    ,1);
	  eff    =wk()->metaData()->castDouble(SH::MetaFields::filterEfficiency,1);
	  kfactor=wk()->metaData()->castDouble(SH::MetaFields::kfactor         ,1);
	}
      m_weight_xs = xs * eff * kfactor;
    }

  //
  // Pythia dijet JZ*W samples
  m_cleanPileup = m_isMC && ((361020<=dsid && dsid<=361032 ) ||
			     (364700<=dsid && dsid<=364712 ));
  if(m_cleanPileup)
    m_cf_cleanPileup=init_cutflow("CleanPileup");

  //Set up boosted jet taggers only when running over large-R jets
  if(m_calcExtraVariables)
    {
      ANA_CHECK( ASG_MAKE_ANA_TOOL( m_jetTagger_handle, JSSWTopTaggerANN) );
      m_jetTagger_handle.setProperty( "ConfigFile", "JSSANNTagger_AntiKt10LCTopoTrimmed_20190827.dat");
      m_jetTagger_handle.setProperty( "CalibArea", "JSSWTopTaggerANN/Rel21");
      m_jetTagger_handle.setProperty( "IsMC", m_isMC);
      ANA_CHECK( m_jetTagger_handle.retrieve() );
  }

  Info("initialize()", "Succesfully initialized! \n");
  return EL::StatusCode::SUCCESS;
}

void ZprimeNtupler::AddTree( const std::string& name ) {

  std::string treeName("outTree");
  // naming convention
  treeName += name; // add systematic
  TTree * outTree = new TTree(treeName.c_str(),treeName.c_str());
  if( !outTree ) {
    Error("AddTree()","Failed to get output tree!");
    // FIXME!! kill here
  }
  TFile* treeFile = wk()->getOutputFile( m_treeStream );
  outTree->SetDirectory( treeFile );

  // Ensure tree flushes memory after a reasonable amount of time
  // Otherwise jobs with a lot of systematics use too much memory.
  std::cout << "Doing memory flushing step" << std::endl;
  outTree->SetAutoFlush(-100*1024*1024); // 100 Mb flush

  //
  // Create the output tree
  //
  ZprimeMiniTree* miniTree = new ZprimeMiniTree(m_event, outTree, treeFile, m_isMC, m_saveZprimeDecay, m_calcExtraVariables);
  if( m_truthLevelOnly )
    {
      miniTree->AddEvent  ( m_eventDetailStr );
    }
  else
    {
      miniTree->AddEvent  ( m_eventDetailStr );
      miniTree->AddTrigger( m_trigDetailStr );
    }

  for(auto container : m_containers)
    {
      switch(container.m_type)
	{
	case ZprimeNtuplerContainer::JET:
	  ANA_MSG_DEBUG("Create ZprimeNtuplerContainer::JET");
	  miniTree->AddJets      ((name.empty())?container.m_detailStr:container.m_detailStrSyst, container.m_branchName);
	  break;
	case ZprimeNtuplerContainer::PHOTON:
	  ANA_MSG_DEBUG("Create ZprimeNtuplerContainer::PHOTON");
	  miniTree->AddPhotons   ((name.empty())?container.m_detailStr:container.m_detailStrSyst, container.m_branchName);
	  break;
	case ZprimeNtuplerContainer::MUON:
	  ANA_MSG_DEBUG("Create ZprimeNtuplerContainer::MUON");
	  miniTree->AddMuons     ((name.empty())?container.m_detailStr:container.m_detailStrSyst, container.m_branchName);
	  break;
	case ZprimeNtuplerContainer::ELECTRON:
	  ANA_MSG_DEBUG("Create ZprimeNtuplerContainer::ELECTRON");
	  miniTree->AddElectrons ((name.empty())?container.m_detailStr:container.m_detailStrSyst, container.m_branchName);
	  break;
	case ZprimeNtuplerContainer::TRUTH:
	  ANA_MSG_DEBUG("Create ZprimeNtuplerContainer::TRUTH");
	  miniTree->AddTruthParts((name.empty())?container.m_detailStr:container.m_detailStrSyst, container.m_branchName);
	  break;
	case ZprimeNtuplerContainer::FATJET:
	  ANA_MSG_DEBUG("Create ZprimeNtuplerContainer::FATJET");
	  miniTree->AddFatJets   ((name.empty())?container.m_detailStr:container.m_detailStrSyst, container.m_branchName, (name.empty())?container.m_subjetDetailStr:container.m_subjetDetailStrSyst);
	  break;
        case ZprimeNtuplerContainer::HLT_PHOTON:
          ANA_MSG_DEBUG("Create ZprimeNtuplerContainer::HLT_PHOTON");
          miniTree->AddHLTPhotons();
          break;
	}
    }
  m_myTrees[name] = miniTree;
  // see Worker.cxx line 134: the following function call takes ownership of the tree
  // from the treeFile; no output is written. so don't do that!
  // wk()->addOutput( outTree );
}


EL::StatusCode ZprimeNtupler :: execute ()
{
  // Here you do everything that needs to be done on every single
  // event, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ANA_MSG_DEBUG("ZprimeNtupler::execute()");
  ++m_eventCounter;

  //
  // Pythia dijet sample cleaning
  if(m_cleanPileup)
    {
      const xAOD::JetContainer* recoJets=0;
      ANA_CHECK(HelperFunctions::retrieve(recoJets,  "AntiKt4EMTopoJets", m_event, m_store));

      const xAOD::JetContainer* truthJets=0;
      ANA_CHECK(HelperFunctions::retrieve(truthJets, "AntiKt4TruthJets",  m_event, m_store));

      if(recoJets->size() > 1) {
	float pTAvg = ( recoJets->at(0)->pt() + recoJets->at(1)->pt() ) /2.0;
	if( truthJets->size() == 0 || (pTAvg / truthJets->at(0)->pt() > 1.4) ) {
	  wk()->skipEvent();
	  return EL::StatusCode::SUCCESS;
	}
      }

      cutflow(m_cf_cleanPileup);
    }


  //
  // Retrieve Input Containers
  //
  ANA_MSG_DEBUG("Get Containers");
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  SG::AuxElement::ConstAccessor<float> NPVAccessor("NPV");
  const xAOD::VertexContainer* vertices = nullptr;
  if(!m_truthLevelOnly) {
    ANA_CHECK(HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store));
    if(!NPVAccessor.isAvailable( *eventInfo )) { // NPV might already be available
      // number of PVs with 2 or more tracks
      //eventInfo->auxdecor< int >( "NPV" ) = HelperFunctions::countPrimaryVertices(vertices, 2);
      // TMP for JetUncertainties uses the same variable
      eventInfo->auxdecor< float >( "NPV" ) = HelperFunctions::countPrimaryVertices(vertices, 2);
    }
  }

  //
  // Event weights
  //
  if(m_isMC)
    m_mcEventWeight = eventInfo->mcEventWeight();
  else
    m_mcEventWeight = 1;

  eventInfo->auxdecor< float >("weight_xs") = m_weight_xs;
  eventInfo->auxdecor< float >("weight")    = m_mcEventWeight * m_weight_xs;

  //
  // Truth info
  //
  if(m_isMC)
    {
      ANA_MSG_DEBUG("Finding truth Zprime");

      float Zprime_pt =0;
      float Zprime_eta=0;
      float Zprime_phi=0;
      float Zprime_m  =0;
      int   Zprime_pdg=0;

      float reso0_pt =0;
      float reso0_eta=0;
      float reso0_phi=0;
      float reso0_E  =0;

      float reso1_pt =0;
      float reso1_eta=0;
      float reso1_phi=0;
      float reso1_E  =0;

      float ISR_pt =0;
      float ISR_eta=0;
      float ISR_phi=0;
      float ISR_E  =0;
      float ISR_pdgId  =0;

      const xAOD::TruthParticleContainer* truths;
      if(HelperFunctions::isAvailable<xAOD::TruthParticleContainer>("TruthBSM",m_event,m_store))
	{
	  ANA_CHECK(HelperFunctions::retrieve(truths, "TruthBSM", m_event, m_store));

	  bool found_ISR = false;
          bool found_reso = false;
          bool found_zprime = false;

	  for(auto truth : *truths)
	    {
	      if(truth->status()!=62) continue;
	      if(truth->pdgId()==101 || truth->pdgId()==25 || truth->pdgId()==23)
		{
		  Zprime_pt =truth->pt();
		  if(Zprime_pt>0)
		    {
		      Zprime_eta=truth->eta();
		      Zprime_phi=truth->phi();
		    }
		  Zprime_m  =truth->m();
		  Zprime_pdg=truth->pdgId();
		  
		  found_zprime = true;
		}

	      //Needed for the combinatorics in the resolved trijet analysis
	      if(!m_saveZprimeDecay && found_zprime)
                break;

              if(m_saveZprimeDecay && truth->pdgId()==101){

                // Find the ISR particle
		if(truth->barcode() == 5){
                  auto Zprime_vertex = truth->prodVtx();

                  for(auto truth2 : *truths){
                    auto truth_vertex = truth2->prodVtx();
                    if(truth2->pdgId()!=101 && truth_vertex == Zprime_vertex){

                      ISR_pt = truth2->pt();
                      ISR_eta = truth2->eta();
                      ISR_phi = truth2->phi();
                      ISR_E = truth2->e();
                      ISR_pdgId = truth2->pdgId();
		      
                      found_ISR = true;
		      
                      break;
                    }
                  }
                }
                if(truth->nChildren() == 2){
                  reso0_pt = truth->child(0)->pt();
                  reso0_eta = truth->child(0)->eta();
                  reso0_phi = truth->child(0)->phi();
                  reso0_E = truth->child(0)->e();
		  
                  reso1_pt = truth->child(1)->pt();
                  reso1_eta = truth->child(1)->eta();
                  reso1_phi = truth->child(1)->phi();
                  reso1_E = truth->child(1)->e();
		  
                  found_reso = true;
                }
		
                if(found_zprime && found_reso && found_ISR)
                  break;
              }
	      
	    }
	}
      
      eventInfo->auxdecor< float >("Zprime_pt")  = Zprime_pt/1000;
      eventInfo->auxdecor< float >("Zprime_eta") = Zprime_eta;
      eventInfo->auxdecor< float >("Zprime_phi") = Zprime_phi;
      eventInfo->auxdecor< float >("Zprime_m")   = Zprime_m/1000;
      eventInfo->auxdecor< int   >("Zprime_pdg") = Zprime_pdg;

      eventInfo->auxdecor< float >("reso0_pt")  = reso0_pt/1000;
      eventInfo->auxdecor< float >("reso0_eta") = reso0_eta;
      eventInfo->auxdecor< float >("reso0_phi") = reso0_phi;
      eventInfo->auxdecor< float >("reso0_E")   = reso0_E/1000;

      eventInfo->auxdecor< float >("reso1_pt")  = reso1_pt/1000;
      eventInfo->auxdecor< float >("reso1_eta") = reso1_eta;
      eventInfo->auxdecor< float >("reso1_phi") = reso1_phi;
      eventInfo->auxdecor< float >("reso1_E")   = reso1_E/1000;

      eventInfo->auxdecor< float >("ISR_pt")  = ISR_pt/1000;
      eventInfo->auxdecor< float >("ISR_eta") = ISR_eta;
      eventInfo->auxdecor< float >("ISR_phi") = ISR_phi;
      eventInfo->auxdecor< float >("ISR_E")   = ISR_E/1000;
      eventInfo->auxdecor< int >("ISR_pdgId")   = ISR_pdgId;

    }

  //
  // Loop over Systematics
  //

  //
  // did any collection pass the cuts?
  //
  bool pass(false);

  //
  // if input comes from xAOD, or just running one collection, 
  // then get the one collection and be done with it
  //
  if( m_inputAlgo.empty() || m_truthLevelOnly ) {

    if(m_truthLevelOnly)
      {
  	pass = this->executeTruthAnalysis( eventInfo );
      }
    else
      {
  	pass = this->executeAnalysis( eventInfo, vertices );
      }

  }

  //
  // get the list of systematics to run over
  //
  else 
  {

      // get vector of strings giving the names
      std::vector<std::string>* systNames = nullptr;
      if ( m_store->contains< std::vector<std::string> >( m_inputAlgo ) ) 
      	{
      	  if(!m_store->retrieve( systNames, m_inputAlgo ).isSuccess()) 
      	    {
      	      ANA_MSG_INFO("Cannot find vector from " << m_inputAlgo);
      	      return StatusCode::FAILURE;
      	    }
      	}
    
      // loop over systematics
      bool saveContainerNames(false);
      std::vector< std::string >* vecOutContainerNames = nullptr;
      if(saveContainerNames) { vecOutContainerNames = new std::vector< std::string >; }

      // should only count for the nominal
      bool passOne(false);
      for( auto systName : *systNames ) 
      	{
      	  // align with Dijet naming conventions
      	  passOne = this->executeAnalysis( eventInfo, vertices, systName );

      	  // save the string if passing the selection
      	  if( saveContainerNames && passOne ) { vecOutContainerNames->push_back( systName ); }

      	  // the final decision - if at least one passes keep going!
      	  pass = pass || passOne;
 
       	}

      // save list of systs that should be considered down stream
      if( vecOutContainerNames )
      	{
      	  ANA_CHECK( m_store->record( vecOutContainerNames, m_name));
      	}
 
  }

  //
  // kill events if nothing passes
  //
  if(!pass) 
    wk()->skipEvent();

  // Clear store in an attempt to clean up memory leak
  m_store->clear();

  return EL::StatusCode::SUCCESS;
  
}

bool ZprimeNtupler :: executeTruthAnalysis ( const xAOD::EventInfo* eventInfo,
					     const std::string& systName) {
  ANA_MSG_DEBUG("ZprimeNtupler::executeTruthAnalysis()");

  //
  //  fill the tree
  //
  ANA_MSG_DEBUG(" Filling Branches");
  if( m_myTrees.find( systName ) == m_myTrees.end() ) AddTree( systName );

  ANA_MSG_DEBUG(" Filling Event");
  if(eventInfo) m_myTrees[systName]->FillEvent( eventInfo, m_event );

  ANA_MSG_DEBUG(" Filling Particles");

  for(auto containerInfo : m_containers)
    {
      ANA_MSG_DEBUG(" Filling " << containerInfo.m_containerName);
      switch(containerInfo.m_type)
	{
	case ZprimeNtuplerContainer::JET:
	  {
	    const xAOD::JetContainer* jets = 0;
	    ANA_CHECK(HelperFunctions::retrieve(jets, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillJets( jets, -1, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::PHOTON:
	  {
	    const xAOD::PhotonContainer* photons = 0;
	    ANA_CHECK(HelperFunctions::retrieve(photons, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillPhotons( photons, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::MUON:
	  {
	    const xAOD::MuonContainer* muons = 0;
	    ANA_CHECK(HelperFunctions::retrieve(muons, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillMuons( muons, 0, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::ELECTRON:
	  {
	    const xAOD::ElectronContainer* electrons = 0;
	    ANA_CHECK(HelperFunctions::retrieve(electrons, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillElectrons( electrons, 0, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::TRUTH:
	  {
	    const xAOD::TruthParticleContainer* truths = 0;
	    ANA_CHECK(HelperFunctions::retrieve(truths, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillTruth( truths, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::FATJET:
	  {
	    const xAOD::JetContainer* fatjets = 0;
	    ANA_CHECK(HelperFunctions::retrieve(fatjets,    containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillFatJets( fatjets, 0, containerInfo.m_branchName );
	    break;
	  }

        case ZprimeNtuplerContainer::HLT_PHOTON:
          {
            const xAOD::PhotonContainer *HLT_photons = 0;
            ANA_CHECK(HelperFunctions::retrieve(HLT_photons, containerInfo.m_containerName+systName, m_event, m_store));
            m_myTrees[systName]->ClearHLTPhotons();
            m_myTrees[systName]->FillHLTPhotons( HLT_photons );
            break;
          }

	}
    }

  ANA_MSG_DEBUG(" Fill Tree");
  m_myTrees[systName]->Fill();

  return true;
}

bool ZprimeNtupler :: executeAnalysis ( const xAOD::EventInfo* eventInfo,
					const xAOD::VertexContainer* vertices,
					const std::string& systName)
{
  ANA_MSG_DEBUG("ZprimeNtupler::executeAnalysis()");

  //
  //  fill the tree
  //
  ANA_MSG_DEBUG(" Filling Branches");
  if( m_myTrees.find( systName ) == m_myTrees.end() ) AddTree( systName );

  ANA_MSG_DEBUG(" Filling Event");
  if(eventInfo) m_myTrees[systName]->FillEvent( eventInfo, m_event );

  ANA_MSG_DEBUG(" Filling Trigger");
  if(eventInfo) m_myTrees[systName]->FillTrigger(eventInfo);

  ANA_MSG_DEBUG(" Filling Particles");

  for(auto containerInfo : m_containers)
    {
      ANA_MSG_DEBUG(" Filling " << containerInfo.m_containerName);
      switch(containerInfo.m_type)
	{
	case ZprimeNtuplerContainer::JET:
	  {
	    const xAOD::JetContainer* jets = 0;
	    if(evtStore()->contains<xAOD::JetContainer>(containerInfo.m_containerName+systName))
	      { ANA_CHECK(evtStore()->retrieve(jets,    containerInfo.m_containerName+systName)); }
	    else
	      { ANA_CHECK(evtStore()->retrieve(jets,    containerInfo.m_containerName)); }
	    m_myTrees[systName]->FillJets( jets, HelperFunctions::getPrimaryVertexLocation( vertices ), containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::PHOTON:
	  {
	    const xAOD::PhotonContainer* photons = 0;
	    if(evtStore()->contains<xAOD::PhotonContainer>(containerInfo.m_containerName+systName))
	      { ANA_CHECK(evtStore()->retrieve(photons, containerInfo.m_containerName+systName)); }
	    else
	      { ANA_CHECK(evtStore()->retrieve(photons, containerInfo.m_containerName)); }
	    m_myTrees[systName]->FillPhotons( photons, containerInfo.m_branchName );
	    break;
	  }


	case ZprimeNtuplerContainer::MUON:
	  {
	    const xAOD::MuonContainer* muons = 0;
	    if(evtStore()->contains<xAOD::MuonContainer>(containerInfo.m_containerName+systName))
	      { ANA_CHECK(evtStore()->retrieve(muons, containerInfo.m_containerName+systName)); }
	    else
	      { ANA_CHECK(evtStore()->retrieve(muons, containerInfo.m_containerName)); }
	    m_myTrees[systName]->FillMuons( muons, 0, containerInfo.m_branchName );
	    break;
	  }


	case ZprimeNtuplerContainer::ELECTRON:
	  {
	    const xAOD::ElectronContainer* electrons = 0;
	    if(evtStore()->contains<xAOD::ElectronContainer>(containerInfo.m_containerName+systName))
	      { ANA_CHECK(evtStore()->retrieve(electrons, containerInfo.m_containerName+systName)); }
	    else
	      { ANA_CHECK(evtStore()->retrieve(electrons, containerInfo.m_containerName)); }
	    m_myTrees[systName]->FillElectrons( electrons, 0, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::TRUTH:
	  {
	    const xAOD::TruthParticleContainer* truths = 0;
	    if(evtStore()->contains<xAOD::TruthParticleContainer>(containerInfo.m_containerName+systName))
	      { ANA_CHECK(evtStore()->retrieve(truths, containerInfo.m_containerName+systName)); }
	    else
	      { ANA_CHECK(evtStore()->retrieve(truths, containerInfo.m_containerName)); }
	    m_myTrees[systName]->FillTruth( truths, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::FATJET:
	  {

	    const xAOD::JetContainer* fatjets = 0;
	    if(evtStore()->contains<xAOD::JetContainer>(containerInfo.m_containerName+systName))
	      { ANA_CHECK(evtStore()->retrieve(fatjets, containerInfo.m_containerName+systName)); }
	    else
	      { ANA_CHECK(evtStore()->retrieve(fatjets, containerInfo.m_containerName)); }

	    if(m_calcExtraVariables)
              calculateExtraVariables(fatjets);

	    m_myTrees[systName]->FillFatJets( fatjets, HelperFunctions::getPrimaryVertexLocation( vertices ), containerInfo.m_branchName );
	    break;
	  }

        case ZprimeNtuplerContainer::HLT_PHOTON:
          {
            const xAOD::PhotonContainer* HLT_photons = 0;
            if(evtStore()->contains<xAOD::PhotonContainer>(containerInfo.m_containerName+systName))
              { ANA_CHECK(evtStore()->retrieve(HLT_photons, containerInfo.m_containerName+systName)); }
            else
              { ANA_CHECK(evtStore()->retrieve(HLT_photons, containerInfo.m_containerName)); }

            m_myTrees[systName]->ClearHLTPhotons();
            m_myTrees[systName]->FillHLTPhotons( HLT_photons );
            break;
          }

	}
    }

  ANA_MSG_DEBUG(" Fill Tree");
  m_myTrees[systName]->Fill();

  return true;
}
EL::StatusCode ZprimeNtupler :: histFinalize ()
{ 
  ANA_CHECK(ZprimeAlgorithm::histFinalize());
  
  write_cutflow(m_treeStream, "Test");
      
  // Write out the Metadata
  //
  TFile *fileMD = wk()->getOutputFile("metadata");
  TH1D* histEventCount = (TH1D*)fileMD->Get("MetaData_EventCount");

  TFile *treeFile = wk()->getOutputFile( m_treeStream );
  TH1F* thisHistEventCount = (TH1F*) histEventCount->Clone();
  std::string thisName = thisHistEventCount->GetName();
  thisHistEventCount->SetName( (thisName+"_Test").c_str() );
  thisHistEventCount->SetDirectory( treeFile );

  return EL::StatusCode::SUCCESS;
}

void ZprimeNtupler::calculateExtraVariables(const xAOD::JetContainer* fatjets)
{
  static SG::AuxElement::ConstAccessor<float> Tau1_wta ("Tau1_wta");
  static SG::AuxElement::ConstAccessor<float> Tau2_wta ("Tau2_wta");

  for(const xAOD::Jet *fatjet_itr : *fatjets)
    {
      bool m_undefInput = false;

      if(Tau1_wta.isAvailable(*fatjet_itr) && Tau2_wta.isAvailable(*fatjet_itr))
	{
	  float tau21wta = Tau2_wta(*fatjet_itr)/Tau1_wta(*fatjet_itr);
	  if(std::isnan(tau21wta))
	    m_undefInput = true;
	}

      if(!m_undefInput)
	m_jetTagger_handle->tag(*fatjet_itr);
      else
	fatjet_itr->auxdecor<float>("ANNTagger_Score") = -666;
    }
}
