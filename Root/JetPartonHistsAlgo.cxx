#include <ZprimeDM/JetPartonHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <SampleHandler/MetaFields.h>

#include <xAODJet/JetContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODTruth/TruthParticle.h>
#include <AthContainers/ConstDataVector.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

// this is needed to distribute the algorithm to the workers
ClassImp(JetPartonHistsAlgo)

JetPartonHistsAlgo :: JetPartonHistsAlgo (const std::string& className)
: ZprimeAlgorithm(className)
{
  m_jetContainerName     = "";
  m_truthContainerName   = "TruthParticles";
  for(uint i=0; i<10; i++) m_jetshists[i]=0;
}

EL::StatusCode JetPartonHistsAlgo :: setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init("JetPartonHistsAlgo").ignore();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetPartonHistsAlgo :: histInitialize ()
{
  ANA_CHECK(ZprimeAlgorithm::histInitialize());

  // declare class and add histograms to output
  m_jethists = new JetHists( "jets", "kinematic 1LeadingJets" );
  ANA_CHECK(m_jethists->initialize());
  m_jethists -> record( wk() );

  for(uint i=0;i<10;i++)
    {
      std::stringstream jethistname;
      jethistname << "jets" <<i*100;
      m_jetshists[i] = new JetHists( jethistname.str(), "kinematic 1LeadingJets" );
      ANA_CHECK(m_jetshists[i]->initialize());
      m_jetshists[i] -> record( wk() );
    }

  m_partonjethists = new PartonJetHists("partonjet");
  ANA_CHECK(m_partonjethists->initialize());
  m_partonjethists -> record( wk() );

  m_partonjethists_0 = new PartonJetHists("partonjet_0");
  ANA_CHECK(m_partonjethists_0->initialize());
  m_partonjethists_0 -> record( wk() );

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetPartonHistsAlgo :: initialize ()
{
  Info("initialize()", "JetPartonHistsAlgo");

  ANA_CHECK(ZprimeAlgorithm::initialize());

  m_cf_n23s=init_cutflow("n23s");

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetPartonHistsAlgo :: execute ()
{
  //
  // Update xs weight
  //
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK(HelperFunctions::retrieve(eventInfo, m_eventInfoContainerName, m_event, m_store));

  // load all of the necessary containers
  const xAOD::JetContainer* inJets = 0;
  ANA_CHECK(HelperFunctions::retrieve(inJets   , m_jetContainerName, m_event, m_store));

  const xAOD::TruthParticleContainer* inTruth = 0;
  ANA_CHECK(HelperFunctions::retrieve(inTruth  , m_truthContainerName, m_event, m_store));

  m_jethists->execute(inJets   , event_weight() );

  const xAOD::Jet *jet0=inJets->at(0);

  // Find leading parton pt
  //std::cout << "=== NEW EVENT ===" << std::endl;

  const xAOD::TruthParticle *parton0=0;
  float pjdR0=-1;

  uint n23s=0;
  float highpt=0;
  const xAOD::TruthParticle *p23=0;
  for(auto particle : *inTruth)
    {
      //std::cout << "Particle " << particle->status() << ":\t" << particle->pt() << "\t" << particle->phi() << "\t" << particle->eta() << "\t" << particle->pdgId() << std::endl;
      if(particle->status()==23)
	{
	  n23s++;
	  if(particle->pt()>highpt)
	    {
	      highpt=particle->pt();
	      p23=particle;
	    }

	  if(pjdR0<0 || particle->p4().DeltaR(jet0->p4())<pjdR0)
	    {
	      parton0=particle;
	      pjdR0=particle->p4().DeltaR(jet0->p4());
	    }
	}
    }
  highpt/=1e3;

  //std::cout << "GOT STATUS 23 " << n23s << std::endl;
  uint wantN23s;
  switch(eventInfo->runNumber())
    {
    case 999911:
    case 999921:
    case 999941:
    case 999951:
    case 999998:
      wantN23s=3;
      break;
    default:
      wantN23s=2;
      break;
    }

  if(n23s!=wantN23s)
    {
      // std::cout << "CUTTING N23" << std::endl;
      // for(auto particle : *inTruth)
      // 	{
      // 	  std::cout << particle->barcode() << "\tParticle " << particle->status() << ":\t" 
      // 		    << particle->pt() << "\t" 
      // 		    << particle->phi() << "\t" 
      // 		    << particle->eta() << "\t"
      // 		    << particle->pdgId() << "\t" 
      // 		    << ((particle->parent()!=0)?particle->parent()->barcode():0) << std::endl;
      // 	  std::cout << "\t";
      // 	  for(uint i=0; i<particle->nChildren(); i++)
      // 	    {
      // 	      std::cout << ((particle->child(i))?particle->child(i)->barcode():0) << "\t";
      // 	    }
      // 	  std::cout << std::endl;
      // 	}
      return EL::StatusCode::SUCCESS;
    }
  cutflow(m_cf_n23s);

  // if(highpt<200 && jet0->pt()>400e3)
  //   {
  //     std::cout << "ONE GOT THROUGH w/ PT " << highpt << "!" << std::endl;
  //     for(auto particle : *inTruth)
  // 	{
  // 	  std::cout << particle->barcode() << "\tParticle " << particle->status() << ":\t" 
  // 		    << particle->pt() << "\t" 
  // 		    << particle->phi() << "\t" 
  // 		    << particle->eta() << "\t"
  // 		    << particle->pdgId() << "\t" 
  // 		    << ((particle->parent()!=0)?particle->parent()->barcode():0) << std::endl;
  // 	  std::cout << "\t";
  // 	  for(uint i=0; i<particle->nChildren(); i++)
  // 	    {
  // 	      std::cout << ((particle->child(i))?particle->child(i)->barcode():0) << "\t";
  // 	    }
  // 	  std::cout << std::endl;
  // 	}
  //   }

  for(uint i=0; i<10; i++)
    {
      if(highpt>100*i) m_jetshists[i]->execute(inJets   , event_weight() );
      //if(highpt>100*i) m_jetshists[i]->execute(inJets   , 1 );
    }

  m_partonjethists  ->execute(jet0, p23, event_weight());
  m_partonjethists_0->execute(jet0, parton0, event_weight());

  // Leading jet
  

  // Debugging output
  // if(jet0->pt()>350e3)
  //   {
  //     std::cout << "HIGH PT JET from " << highpt << std::endl;
  //     std::cout << jet0->pt() << "\t" << jet0->phi() << "\t" << jet0->eta() << std::endl;
  //     if(highpt<300) std::cout << "PROBLEM" << std::endl;
  //   }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetPartonHistsAlgo :: histFinalize ()
{
  ANA_CHECK(ZprimeAlgorithm::histFinalize());

  // clean up memory
  delete m_jethists;
  m_jethists=0;

  for(uint i=0; i<10; i++){
    
    if(m_jetshists[i])
      {
	delete m_jetshists[i];
	m_jetshists[i] = 0;
      }
  }

  delete m_partonjethists;
  m_partonjethists=0;

  delete m_partonjethists_0;
  m_partonjethists_0=0;

  return EL::StatusCode::SUCCESS;
}
