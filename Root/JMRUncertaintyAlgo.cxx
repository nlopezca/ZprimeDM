#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <PathResolver/PathResolver.h>

#include <AsgTools/MessageCheck.h>

#include <ZprimeDM/JMRUncertaintyAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

// this is needed to distribute the algorithm to the workers
ClassImp(JMRUncertaintyAlgo)

DijetISREvent* JMRUncertaintyAlgo::m_eventData1 =nullptr;
DijetISREvent* JMRUncertaintyAlgo::m_eventData2 =nullptr;
DijetISREvent* JMRUncertaintyAlgo::m_eventData3 =nullptr;

DijetISREvent* JMRUncertaintyAlgo::global(JMRUncertaintyAlgo::VARIATIONS var)
{
  switch(var)
    {
    case JMRUncertaintyAlgo::NOMINAL:
      return DijetISREvent::global();
    case JMRUncertaintyAlgo::SIGMA1:
      return m_eventData1;
    case JMRUncertaintyAlgo::SIGMA2:
      return m_eventData2;
    case JMRUncertaintyAlgo::SIGMA3:
      return m_eventData3;
    default:
      return nullptr;
    }
}

JMRUncertaintyAlgo :: JMRUncertaintyAlgo (const std::string& className)
: Algorithm(className)
{ Info("JMRUncertaintyAlgo()", "Calling constructor"); }

JMRUncertaintyAlgo :: ~JMRUncertaintyAlgo()
{ }

void JMRUncertaintyAlgo::copyNominal(DijetISREvent* eventDataV, std::vector<xAH::FatJet> *fatjetV)
{
  eventDataV->setJets     (m_eventData->getJets     ());
  eventDataV->setPhotons  (m_eventData->getPhotons  ());
  eventDataV->setMuons    (m_eventData->getMuons    ());
  eventDataV->setElectrons(m_eventData->getElectrons());
  eventDataV->setFatJets  (fatjetV                    );
  eventDataV->setTrigJets (m_eventData->getTrigJets ());
  eventDataV->setTruths   (m_eventData->getTruths   ());
    
  eventDataV->m_passedTriggers  =m_eventData->m_passedTriggers  ;
  eventDataV->m_disabledTriggers=m_eventData->m_disabledTriggers;
  eventDataV->m_triggerPrescales=m_eventData->m_triggerPrescales;
  eventDataV->m_isPassBitsNames =m_eventData->m_isPassBitsNames ;
  eventDataV->m_isPassBits      =m_eventData->m_isPassBits      ;
}

void JMRUncertaintyAlgo::varyNominal(DijetISREvent* eventDataV, JMRUncertaintyAlgo::VARIATIONS var)
{
  eventDataV->getFatJets()->clear();

  double smear=0.;
  switch(var)
    {
    case JMRUncertaintyAlgo::NOMINAL:
      smear=0.;
      break;
    case JMRUncertaintyAlgo::SIGMA1:
      smear=0.66*0.15;
      break;
    case JMRUncertaintyAlgo::SIGMA2:
      smear=0.98*0.15;
      break;
    case JMRUncertaintyAlgo::SIGMA3:
      smear=1.25*0.15;
      break;
    default:
      smear=1.00*0.15;
      break;
    }

  for(auto particle : *(m_eventData->getFatJets()))
    {
      // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JetUncertaintiesRel21Moriond2018LargeR#Resolution_uncertainties
      // 15% is the current resolution
      // 0.66 is the "20%" smearing of the resolution
      particle.p4.SetPtEtaPhiM(particle.p4.Pt(),
			       particle.p4.Eta(),
			       particle.p4.Phi(),
			       m_random.Gaus(particle.p4.M(), smear*particle.p4.M()));

      eventDataV->getFatJets()->push_back(particle);
    }

  //
  // scalar
  eventDataV->m_doTruthOnly                   =m_eventData->m_doTruthOnly                   ;
  eventDataV->m_mc                            =m_eventData->m_mc                            ;

  eventDataV->m_NPV                           =m_eventData->m_NPV                           ;
  eventDataV->m_actualInteractionsPerCrossing =m_eventData->m_actualInteractionsPerCrossing ;
  eventDataV->m_averageInteractionsPerCrossing=m_eventData->m_averageInteractionsPerCrossing;
  eventDataV->m_runNumber                     =m_eventData->m_runNumber                     ;
  eventDataV->m_eventNumber                   =m_eventData->m_eventNumber                   ;
  eventDataV->m_lumiBlock                     =m_eventData->m_lumiBlock                     ;

  eventDataV->m_mcEventNumber                 =m_eventData->m_mcEventNumber                 ;
  eventDataV->m_mcChannelNumber               =m_eventData->m_mcChannelNumber               ;
  eventDataV->m_mcEventWeight                 =m_eventData->m_mcEventWeight                 ;

  eventDataV->m_weight                        =m_eventData->m_weight                        ;
  eventDataV->m_weight_xs                     =m_eventData->m_weight_xs                     ;
  eventDataV->m_weight_pileup                 =m_eventData->m_weight_pileup                 ;

  eventDataV->m_Zprime_pt                     =m_eventData->m_Zprime_pt                     ;
  eventDataV->m_Zprime_eta                    =m_eventData->m_Zprime_eta                    ;
  eventDataV->m_Zprime_phi                    =m_eventData->m_Zprime_phi                    ;
  eventDataV->m_Zprime_m                      =m_eventData->m_Zprime_m                      ;

  eventDataV->m_Zprime                        =m_eventData->m_Zprime                        ;
}

EL::StatusCode JMRUncertaintyAlgo :: initialize ()
{
  ANA_MSG_INFO("JMRUncertaintyAlgo::initialize()");

  m_eventData=DijetISREvent::global();
  if(!m_eventData->m_mc) return EL::StatusCode::SUCCESS; // Do not run JMR on data

  if(m_do1Sigma)
    {
      if(!m_eventData1)
	m_eventData1=new DijetISREvent();
      copyNominal(m_eventData1, &m_jmrFatJets1);
    }

  if(m_do2Sigma)
    {
      if(!m_eventData2)
	m_eventData2=new DijetISREvent();
      copyNominal(m_eventData2, &m_jmrFatJets2);
    }

  if(m_do3Sigma)
    {
      if(!m_eventData3)
	m_eventData3=new DijetISREvent();
      copyNominal(m_eventData3, &m_jmrFatJets3);
    }

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JMRUncertaintyAlgo :: execute ()
{
  ANA_MSG_DEBUG("JMRUncertaintyAlgo::execute()");

  if(m_eventData1)
    varyNominal(m_eventData1, SIGMA1);

  if(m_eventData2)
    varyNominal(m_eventData2, SIGMA2);

  if(m_eventData3)
    varyNominal(m_eventData3, SIGMA3);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JMRUncertaintyAlgo :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}
