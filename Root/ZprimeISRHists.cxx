#include <ZprimeDM/ZprimeISRHists.h>

ZprimeISRHists :: ZprimeISRHists (const std::string& name, const std::string& detailStr)
  : HistogramManager(name, detailStr),
    m_infoSwitch(detailStr)
{}

ZprimeISRHists :: ~ZprimeISRHists () 
{}

StatusCode ZprimeISRHists::initialize()
{

  // Basic mass
  if(m_infoSwitch.m_mass)
  {
    h_mISRjj      = book(m_name, "mISRjj",       "m_{ISRjj} [GeV]",  100,            0,    5000   );
  }
  
  // Kinematics
  if(m_infoSwitch.m_kinematic)
  {
      h_ptISRjj     = book(m_name, "ptISRjj",      "p_{T,ISRjj} [GeV]",100,            0,     500   );
      h_etaISRjj    = book(m_name, "etaISRjj",     "#eta_{ISRjj}",     100,           -5,       5   );
      h_phiISRjj    = book(m_name, "phiISRjj",     "#phi_{ISRjj}",     100, -TMath::Pi(),TMath::Pi());

      h_ht   = book(m_name, "ht",   "h_{T} [GeV]",      100, 0, 2000);
  
      h_pt0isr=book(m_name, "pt0isr", "p_{T,r1}/p_{T,ISR}", 50, 0, 2);
      h_pt1isr=book(m_name, "pt1isr", "p_{T,r2}/p_{T,ISR}", 50, 0, 2);
      h_pt1pt0=book(m_name, "pt1pt0", "p_{T,r2}/p_{T,r1}" , 50, 0, 1);
  }

  // Y's
  if(m_infoSwitch.m_rapidities)
  {
      h_yStarISRjj     = book(m_name, "yStarISRjj",     "y^{*}_{ISR,jj}",        90,      0,       3   );
      h_yBoostISRjj    = book(m_name, "yBoostISRjj",    "y^{boost}_{ISR,jj}",    90,      0,       3   );
      h_chiISRjj       = book(m_name, "chiISRjj",       "#chi_{ISR,jj}",        100,      0,      60   );
  }

  // Asym
  if(m_infoSwitch.m_asymmetries) {
      h_asymISRjj     = book(m_name, "asymISRjj",     "(|p_{T}^{ISR}|-|p_{T}^{jj}|)/(|p_{T}^{ISR}|+|p_{T}^{jj}|)",       120,  -1, 1);
      h_vecasymISRjj  = book(m_name, "vecasymISRjj",  "|#vec{p}_{T}^{ISR}-#vec{p}_{T}^{jj}|/|#vec{p}_{T}^{ISR}+#vec{P}_{T}^{jj}|",   120,  0, 7);
      h_projasymISRjj = book(m_name, "projasymISRjj", "(#vec{p}_{T}^{ISR}-#vec{p}_{T}^{jj})#upoint#hat{p}_{T}^{ISRjj}/p_{T}^{ISRjj}",120,  -20, 20);
      h_simpasymISRjj = book(m_name, "simpasymISRjj", "(|p_{T}^{ISR}|-|p_{T}^{jj}|)/|p_{T}^{ISR}|",                                  120,  -1, 1);
  }

  // Deltas
  if(m_infoSwitch.m_deltas)
  {

      h_dEtaISRjj   = book(m_name, "dEtaISRjj",    "|#Delta#eta_{ISR,jj}|",   100, 0,           4);
      h_dPhiISRjj   = book(m_name, "dPhiISRjj",    "|#Delta#phi_{ISR,jj}|",   100, 0, TMath::Pi());
      h_dRISRjj     = book(m_name, "dRISRjj",      "#DeltaR_{ISR,jj}",        100, 0,           5);
  
      h_dEtaISRclosej = book(m_name, "dEtaISRclosej",    "|#Delta#eta_{ISR,close-j}|",   100, 0,           4);
      h_dPhiISRclosej = book(m_name, "dPhiISRclosej",    "|#Delta#phi_{ISR,close-j}|",   100, 0, TMath::Pi());
      h_dRISRclosej   = book(m_name, "dRISRclosej",      "#DeltaR_{ISR,close-j}",        100, 0,           5);
      h_dEtaISRfarj   = book(m_name, "dEtaISRfarj",      "|#Delta#eta_{ISR,far-j}|",     100, 0,           4);
      h_dPhiISRfarj   = book(m_name, "dPhiISRfarj",      "|#Delta#phi_{ISR,far-j}|",     100, 0, TMath::Pi());
      h_dRISRfarj     = book(m_name, "dRISRfarj",        "#DeltaR_{ISR,far-j}",          100, 0,           5);
      h_asymISRclosej = book(m_name, "asymISRclosej",    "(|p_{T}^{ISR-close}|-|p_{T}^{far}|)/(|p_{T}^{ISR-close}|+|p_{T}^{far}|)",       120,  -1, 1);

      h_dEtaISRj0     = book(m_name, "dEtaISRj0",    "|#Delta#eta_{ISR,lead-j}|",   100, 0,           4);
      h_dPhiISRj0     = book(m_name, "dPhiISRj0",    "|#Delta#phi_{ISR,lead-j}|",   100, 0, TMath::Pi());
      h_dRISRj0       = book(m_name, "dRISRj0",      "#DeltaR_{ISR,lead-j}",        100, 0,           5);
      h_asymISRj0     = book(m_name, "asymISRj0",    "(|p_{T}^{ISR}|-|p_{T}^{lead-j}|)/(|p_{T}^{ISR}|+|p_{T}^{lead-j}|)", 120,  -1, 1);
      h_simpasymISRj0 = book(m_name, "simpasymISRj0","(|p_{T}^{ISR}|-|p_{T}^{lead-j}|)/|p_{T}^{ISR}|",                    120,  -1, 1);

      h_dEtaISRj1     = book(m_name, "dEtaISRj1",    "|#Delta#eta_{ISR,subl-j}|",   100, 0,           4);
      h_dPhiISRj1     = book(m_name, "dPhiISRj1",    "|#Delta#phi_{ISR,subl-j}|",   100, 0, TMath::Pi());
      h_dRISRj1       = book(m_name, "dRISRj1",      "#DeltaR_{ISR,subl-j}",        100, 0,           5);
      h_asymISRj1     = book(m_name, "asymISRj1",    "(|p_{T}^{ISR}|-|p_{T}^{subl-j}|)/(|p_{T}^{ISR}|+|p_{T}^{subl-j}|)", 120,  -1, 1);
     h_simpasymISRj1 = book(m_name, "simpasymISRj1","(|p_{T}^{ISR}|-|p_{T}^{subl-j}|)/|p_{T}^{ISR}|",                    120,  -1, 1);
  }

  //
  //  2D plots
  //
  if(m_infoSwitch.m_2d)
    {
      h_mjj_vs_isr        = book(m_name, "mjj_vs_isr", "ISR p_{T} [GeV]"   , 200, 0, 1000,
				                       "m_{jj} [GeV]"      , 200, 0, 2000 );

      h_mjj_vs_pt0isr     = book(m_name, "mjj_vs_pt0isr", "p_{T,r1}/p_{T,ISR}",  50, 0, 2,
				                          "m_{jj} [GeV]"      , 200, 0, 2000 );

      h_mjj_vs_pt1isr     = book(m_name, "mjj_vs_pt1isr", "p_{T,r2}/p_{T,ISR}",  50, 0, 2,
				                          "m_{jj} [GeV]"      , 200, 0, 2000 );

      h_mjj_vs_yStarISRjj = book(m_name, "mjj_vs_yStarISRjj", "y^{*}_{ISR,jj}",  90, 0,    3,
				                              "m_{jj} [GeV]",   200, 0, 2000 );

      h_dRISRclose_vs_dRjj= book(m_name, "dRISRclose_vs_dRjj",    "dRISRclose",   50, 0, 4,  "dRjj",  50, 0, 4);
      h_asymISRclosefar_vs_asymISRjj = book(m_name, "asymISRclosefar_vs_asymISRjj",    "asymISRclosefar", 50, -1, 1,  "asymISRjj",   50, 0, 1);

      h_asymISRj0_vs_dPhiISRj0= book(m_name, "asymISRj0_vs_dPhiISRj0", "|#Delta#phi_{ISR,lead-j}|"                                        , 100,  0, TMath::Pi(),
				                                       "(|p_{T}^{ISR}|-|p_{T}^{lead-j}|)/(|p_{T}^{ISR}|+|p_{T}^{lead-j}|)", 120, -1, 1);
      h_asymISRj1_vs_dPhiISRj1= book(m_name, "asymISRj1_vs_dPhiISRj1", "|#Delta#phi_{ISR,subl-j}|"                                        , 100,  0, TMath::Pi(),
				                                       "(|p_{T}^{ISR}|-|p_{T}^{subl-j}|)/(|p_{T}^{ISR}|+|p_{T}^{subl-j}|)", 120, -1, 1);
    }

  return StatusCode::SUCCESS;
}

StatusCode ZprimeISRHists::execute(const xAH::Particle* reso0, const xAH::Particle* reso1, const xAH::Particle* ISR, float eventWeight)
{
  TLorentzVector p4_Zprime=reso0->p4+reso1->p4;
  TLorentzVector p4_ISRjj=p4_Zprime+ISR->p4;
  TLorentzVector p4_diffISRjj=ISR->p4-p4_Zprime;

  //
  //  close/far jet
  //
  TLorentzVector closej = reso0->p4;
  TLorentzVector farj   = reso1->p4;
  if(ISR->p4.DeltaR(reso1->p4) < ISR->p4.DeltaR(reso0->p4)){
    closej = reso1->p4;
    farj   = reso0->p4;
  }
  TLorentzVector p4_ISRclose=ISR->p4+closej;
  
  ////// Begin fills
  // kinematics
  if(m_infoSwitch.m_mass)
  {
      h_mISRjj  ->Fill(p4_ISRjj.M()   , eventWeight);
  }

  double pt0isr=reso0->p4.Pt()/ISR  ->p4.Pt();
  double pt1isr=reso1->p4.Pt()/ISR  ->p4.Pt();
  double pt1pt0=reso1->p4.Pt()/reso0->p4.Pt();
  if(m_infoSwitch.m_kinematic)
  {
      h_ptISRjj ->Fill(p4_ISRjj.Pt()  , eventWeight);
      h_etaISRjj->Fill(p4_ISRjj.Eta() , eventWeight);
      h_phiISRjj->Fill(p4_ISRjj.Phi() , eventWeight);

      h_ht->Fill(reso0->p4.Pt()+reso1->p4.Pt()+ISR->p4.Pt(), eventWeight);

      h_pt0isr->Fill(pt0isr, eventWeight);
      h_pt1isr->Fill(pt1isr, eventWeight);
      h_pt1pt0->Fill(pt1pt0, eventWeight);
  }

  // Y's
  float yStarISRjj=fabs(ISR->p4.Rapidity()-p4_Zprime.Rapidity())/2.;
  float yBoostISRjj=fabs(ISR->p4.Rapidity()+p4_Zprime.Rapidity())/2.;
  if(m_infoSwitch.m_rapidities)
  {
      h_yStarISRjj ->Fill(yStarISRjj, eventWeight);

      h_yBoostISRjj->Fill(yBoostISRjj, eventWeight);

      h_chiISRjj   ->Fill(exp(2*fabs(yStarISRjj))        , eventWeight);
  }
  
  // Asym
  TVector3 p3_ISRjj=p4_ISRjj.Vect();
  p3_ISRjj.SetZ(0);
  p3_ISRjj.SetMag(1.);
  double asymISRjj = (ISR->p4.Pt()-p4_Zprime.Pt())/(ISR->p4.Pt()+p4_Zprime.Pt());
  double vecasymISRjj = p4_diffISRjj.Pt()/(ISR->p4+p4_Zprime).Pt();
  double projasymISRjj = (p4_diffISRjj.Vect().Dot(p3_ISRjj))/p4_ISRjj.Pt();
  double simpasymISRjj = (ISR->p4.Pt()-p4_Zprime.Pt())/ISR->p4.Pt();
  if(m_infoSwitch.m_asymmetries) {

      h_asymISRjj->Fill( asymISRjj, eventWeight);

      h_vecasymISRjj->Fill( vecasymISRjj, eventWeight);

      h_projasymISRjj->Fill( projasymISRjj, eventWeight);

      h_simpasymISRjj->Fill( simpasymISRjj, eventWeight);
  }
 
  // Deltas
  float dRISRclosej = ISR->p4.DeltaR(closej);
  float dRISRfarj = ISR->p4.DeltaR(farj);
  float asymISRclosej = (p4_ISRclose.Pt()-farj.Pt())/(p4_ISRclose.Pt()+farj.Pt());
  if(m_infoSwitch.m_deltas)
  {
      h_dEtaISRjj->Fill(fabs(ISR->p4.Eta()-p4_Zprime.Eta()), eventWeight);
      h_dPhiISRjj->Fill(fabs(ISR->p4.DeltaPhi(p4_Zprime))  , eventWeight);
      h_dRISRjj  ->Fill(ISR->p4.DeltaR(p4_Zprime)    , eventWeight);
  
      h_dEtaISRclosej->Fill(fabs(ISR->p4.Eta()-closej.Eta()), eventWeight);
      h_dPhiISRclosej->Fill(fabs(ISR->p4.DeltaPhi(closej))  , eventWeight);
      h_dRISRclosej  ->Fill(dRISRclosej    , eventWeight);

      h_dEtaISRfarj->Fill(fabs(ISR->p4.Eta()-farj.Eta()), eventWeight);
      h_dPhiISRfarj->Fill(fabs(ISR->p4.DeltaPhi(farj))  , eventWeight);
      h_dRISRfarj  ->Fill(dRISRfarj    , eventWeight);

      h_asymISRclosej  ->Fill(asymISRclosej    , eventWeight);
  }

  float dPhiISRj0     = fabs(ISR->p4.DeltaPhi(reso0->p4));
  float asymISRj0     = (ISR->p4.Pt()-reso0->p4.Pt())/(ISR->p4.Pt()+reso0->p4.Pt());
  float simpasymISRj0 = (ISR->p4.Pt()-reso0->p4.Pt())/ISR->p4.Pt();

  float dPhiISRj1     = fabs(ISR->p4.DeltaPhi(reso1->p4));
  float asymISRj1     = (ISR->p4.Pt()-reso1->p4.Pt())/(ISR->p4.Pt()+reso1->p4.Pt());
  float simpasymISRj1 = (ISR->p4.Pt()-reso1->p4.Pt())/ISR->p4.Pt();
  
  if(m_infoSwitch.m_softerjets) {

      h_dEtaISRj0    ->Fill(fabs(ISR->p4.Eta()-reso0->p4.Eta()), eventWeight);
      h_dPhiISRj0    ->Fill(dPhiISRj0        , eventWeight);
      h_dRISRj0      ->Fill(ISR->p4.DeltaR(reso0->p4)          , eventWeight);
      h_asymISRj0    ->Fill(asymISRj0        , eventWeight);
      h_simpasymISRj0->Fill(simpasymISRj0    , eventWeight);

      h_dEtaISRj1      ->Fill(fabs(ISR->p4.Eta()-reso1->p4.Eta()), eventWeight);
      h_dPhiISRj1      ->Fill(dPhiISRj1      , eventWeight);
      h_dRISRj1        ->Fill(ISR->p4.DeltaR(reso1->p4)          , eventWeight);
      h_asymISRj1      ->Fill(asymISRj1      , eventWeight);
      h_simpasymISRj1  ->Fill(simpasymISRj1  , eventWeight);
  }


  // 2D
  if(m_infoSwitch.m_2d)
    {
      h_mjj_vs_isr                  ->Fill(ISR->p4.Pt(),p4_Zprime.M(), eventWeight);

      h_mjj_vs_pt0isr               ->Fill(pt0isr    , p4_Zprime.M(), eventWeight);
      h_mjj_vs_pt1isr               ->Fill(pt1isr    , p4_Zprime.M(), eventWeight);
      h_mjj_vs_yStarISRjj           ->Fill(yStarISRjj, p4_Zprime.M(), eventWeight);

      h_dRISRclose_vs_dRjj          ->Fill(dRISRclosej,   closej.DeltaR(farj) , eventWeight);
      h_asymISRclosefar_vs_asymISRjj->Fill(asymISRclosej, asymISRjj           , eventWeight);

      h_asymISRj0_vs_dPhiISRj0->Fill(dPhiISRj0, asymISRj0, eventWeight);
      h_asymISRj1_vs_dPhiISRj1->Fill(dPhiISRj1, asymISRj1, eventWeight);
    }
  return StatusCode::SUCCESS;
}
