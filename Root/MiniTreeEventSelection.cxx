#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <PathResolver/PathResolver.h>

#include <AsgTools/MessageCheck.h>

#include <ZprimeDM/MiniTreeEventSelection.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

// this is needed to distribute the algorithm to the workers
ClassImp(MiniTreeEventSelection)

MiniTreeEventSelection :: MiniTreeEventSelection (const std::string& className) :
  Algorithm(className),
  m_mc(false),
  m_grl(nullptr),
  m_pileup_tool_handle("CP::PileupReweightingTool/PileupToolName", nullptr)
{
  Info("MiniTreeEventSelection()", "Calling constructor");

  // GRL
  m_applyGRL = false;
  m_GRLxml = ""; // Removing default to ensure this always gets set correctly

  // Pileup Reweighting
  m_doPUreweighting    = false;
  m_lumiCalcFileNames  = "";
  m_PRWFileNames       = "";
  m_PU_default_channel = 0;
}

MiniTreeEventSelection :: ~MiniTreeEventSelection()
{
  if(m_triggerInfoSwitch) { delete m_triggerInfoSwitch; m_triggerInfoSwitch=nullptr; }
  if(m_eventData && m_eventData->m_passedTriggers  ) { delete m_eventData->m_passedTriggers;   m_eventData->m_passedTriggers  =nullptr; }
  if(m_eventData && m_eventData->m_disabledTriggers) { delete m_eventData->m_disabledTriggers; m_eventData->m_disabledTriggers=nullptr; }
  if(m_eventData && m_eventData->m_triggerPrescales) { delete m_eventData->m_triggerPrescales; m_eventData->m_triggerPrescales=nullptr; }
  if(m_eventData && m_eventData->m_isPassBitsNames ) { delete m_eventData->m_isPassBitsNames;  m_eventData->m_isPassBitsNames =nullptr; }
  if(m_eventData && m_eventData->m_isPassBits      ) { delete m_eventData->m_isPassBits;       m_eventData->m_isPassBits      =nullptr; }

  if(m_jets             ) { delete m_jets             ; m_jets             =nullptr; }
  if(m_photons          ) { delete m_photons          ; m_photons          =nullptr; }
  if(m_muons            ) { delete m_muons            ; m_muons            =nullptr; }
  if(m_electrons        ) { delete m_electrons        ; m_electrons        =nullptr; }
  if(m_fatjets          ) { delete m_fatjets          ; m_fatjets          =nullptr; }
  if(m_trigJets         ) { delete m_trigJets         ; m_trigJets         =nullptr; }
  if(m_truths           ) { delete m_truths           ; m_truths           =nullptr; }
}

EL::StatusCode MiniTreeEventSelection :: setupJob (EL::Job& /*job*/)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: histInitialize ()
{
  ANA_MSG_INFO("MiniTreeEventSelection::histInitialize()");

  //
  // data model
  m_eventData=DijetISREvent::global();
  m_eventData->m_doTruthOnly=m_doTruthOnly;
  m_eventData->m_mc=m_mc;

  // Delete old containers
  if(m_triggerInfoSwitch) { delete m_triggerInfoSwitch; m_triggerInfoSwitch=nullptr; }
  if(m_info             ) { delete m_info             ; m_info             =nullptr; }
  if(m_jets             ) { delete m_jets             ; m_jets             =nullptr; }
  if(m_photons          ) { delete m_photons          ; m_photons          =nullptr; }
  if(m_muons            ) { delete m_muons            ; m_muons            =nullptr; }
  if(m_electrons        ) { delete m_electrons        ; m_electrons        =nullptr; }
  if(m_fatjets          ) { delete m_fatjets          ; m_fatjets          =nullptr; }
  if(m_trigJets         ) { delete m_trigJets         ; m_trigJets         =nullptr; }
  if(m_truths           ) { delete m_truths           ; m_truths           =nullptr; }

  // Create new containers
  m_triggerInfoSwitch=new HelperClasses::TriggerInfoSwitch(m_triggerDetailStr);

  if(!m_eventDetailStr   .empty())
    {
      m_info     =new xAH::EventInfo        (                 m_eventDetailStr   , 1e3, m_mc);
    }

  if(!m_jetDetailStr     .empty())
    {
      m_jets     =new xAH::JetContainer     (m_jetsName     , m_jetDetailStr     , 1e3, m_mc);
      m_eventData->setJets     (&m_jets     ->particles());
    }

  if(!m_photonDetailStr  .empty())
    {
      m_photons  =new xAH::PhotonContainer  (m_photonsName  , m_photonDetailStr  , 1e3, m_mc);
      m_eventData->setPhotons  (&m_photons  ->particles());
    }

  if(!m_muonDetailStr    .empty())
    {
      m_muons    =new xAH::MuonContainer    (m_muonsName    , m_muonDetailStr    , 1e3, m_mc);
      m_eventData->setMuons    (&m_muons    ->particles());
    }

  if(!m_electronDetailStr.empty())
    {
      m_electrons=new xAH::ElectronContainer(m_electronsName, m_electronDetailStr, 1e3, m_mc);
      m_eventData->setElectrons(&m_electrons->particles());
    }

  if(!m_fatjetDetailStr  .empty())
    {
      m_fatjets  =new xAH::FatJetContainer  (m_fatjetsName , m_fatjetDetailStr  , m_subjetDetailStr, "", 1e3, m_mc);
      m_eventData->setFatJets  (&m_fatjets  ->particles());
    }

  if(!m_trigJetDetailStr .empty())
    {
      m_trigJets =new xAH::JetContainer     (m_trigJetsName, m_trigJetDetailStr , 1e3, m_mc);
      m_eventData->setTrigJets (&m_trigJets ->particles());
    }

  if(!m_truthDetailStr   .empty())
    {
      m_truths   =new xAH::TruthContainer   (m_truthsName , m_truthDetailStr);
      m_eventData->setTruths   (&m_truths   ->particles());
    }

  //
  // Cutflow
  m_cutflow=new CutflowHists("", "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_init    =m_cutflow->addCut("init");
  m_cf_grl     =m_cutflow->addCut("grl");
  m_cutflow->record(wk());

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: changeInput (bool firstFile)
{
  ANA_MSG_INFO("MiniTreeEventSelection::changeInput(" << firstFile << ")");

  //
  // Update cutflow hists
  TFile* inputFile = wk()->inputFile();

  TH1D* MetaData_EventCount=dynamic_cast<TH1D*>(inputFile->Get("MetaData_EventCount_Test"));
  if(!MetaData_EventCount)
    {
      Error("MiniTreeEventSelection::changeInput()","Missing input event count histogram!");
      return EL::StatusCode::FAILURE;
    }

  float totalEvents= MetaData_EventCount->GetBinContent(1);
  std::cout << "\ttotalEvents = " << totalEvents << std::endl;

  float totalWeight= MetaData_EventCount->GetBinContent(3);
  std::cout << "\ttotalWeight = " << totalWeight << std::endl;

  m_cutflow->executeInitial(totalEvents, totalWeight);

  //
  // Prepare the branches
  TTree *tree = wk()->tree();

  tree->SetBranchStatus ("*", 0);

  // trigger
  if(m_triggerInfoSwitch->m_passTriggers)
    {
      if(!m_eventData->m_passedTriggers)   m_eventData->m_passedTriggers  =new std::vector<std::string>();
      tree->SetBranchStatus  ("passedTriggers",   1);
      tree->SetBranchAddress ("passedTriggers",   &m_eventData->m_passedTriggers);

      if(!m_eventData->m_disabledTriggers) m_eventData->m_disabledTriggers=new std::vector<std::string>();
      tree->SetBranchStatus  ("disabledTriggers", 1);
      tree->SetBranchAddress ("disabledTriggers", &m_eventData->m_disabledTriggers);
    }

  if(!m_mc && m_triggerInfoSwitch->m_prescales)
    {
      if(!m_eventData->m_triggerPrescales) m_eventData->m_triggerPrescales=new std::vector<float>();
      tree->SetBranchStatus  ("triggerPrescales", 1);
      tree->SetBranchAddress ("triggerPrescales", &m_eventData->m_triggerPrescales);
    }

  if(m_triggerInfoSwitch->m_passTrigBits)
    {
      if(!m_eventData->m_isPassBitsNames)  m_eventData->m_isPassBitsNames =new std::vector<std::string>();
      tree->SetBranchStatus  ("isPassBitsNames", 1);
      tree->SetBranchAddress ("isPassBitsNames", &m_eventData->m_isPassBitsNames);

      if(!m_eventData->m_isPassBits     )  m_eventData->m_isPassBits      =new std::vector<unsigned int>();
      tree->SetBranchStatus  ("isPassBits"     , 1);
      tree->SetBranchAddress ("isPassBits"     , &m_eventData->m_isPassBits);
    }

  // weights
  tree->SetBranchStatus  ("weight"   , 1);
  tree->SetBranchAddress ("weight"   , &m_eventData->m_weight);

  tree->SetBranchStatus  ("weight_xs", 1);
  tree->SetBranchAddress ("weight_xs", &m_eventData->m_weight_xs);

  // containers
  if(m_info)      m_info     ->setTree(tree);
  if(m_jets)      m_jets     ->setTree(tree);
  if(m_photons)   m_photons  ->setTree(tree);
  if(m_muons)     m_muons    ->setTree(tree);
  if(m_electrons) m_electrons->setTree(tree);
  if(m_fatjets)   m_fatjets  ->setTree(tree);
  if(m_truths)    m_truths   ->setTree(tree);
  if(m_trigJets)  m_trigJets ->setTree(tree);

  // custom
  if(tree->GetBranch("Zprime_pt"))
    {
      tree->SetBranchStatus ("Zprime_pt",  1);
      tree->SetBranchAddress("Zprime_pt",  &m_eventData->m_Zprime_pt);
    }

  if(tree->GetBranch("Zprime_eta")) 
    {
      tree->SetBranchStatus ("Zprime_eta", 1);
      tree->SetBranchAddress("Zprime_eta", &m_eventData->m_Zprime_eta);
    }

  if(tree->GetBranch("Zprime_phi")) 
    {
      tree->SetBranchStatus ("Zprime_phi", 1);
      tree->SetBranchAddress("Zprime_phi", &m_eventData->m_Zprime_phi);
    }

  if(tree->GetBranch("Zprime_m")) 
    {
      tree->SetBranchStatus ("Zprime_m",   1);
      tree->SetBranchAddress("Zprime_m",   &m_eventData->m_Zprime_m);
    }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: initialize ()
{
  ANA_MSG_DEBUG("MiniTreeEventSelection::initialize()");

  // 1.
  // initialize the GoodRunsListSelectionTool
  //

  if(m_applyGRL)
    {
      m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
      std::vector<std::string> vecStringGRL;
      std::istringstream f(m_GRLxml);
      std::string GRLxml;
      while(std::getline(f, GRLxml, ','))
	{
	  ANA_MSG_INFO("Adding GRL: " << PathResolverFindCalibFile( GRLxml.c_str() ));
	  vecStringGRL.push_back(PathResolverFindCalibFile( GRLxml.c_str() ));
	}
      ANA_CHECK(m_grl->setProperty("GoodRunsListVec", vecStringGRL));
      ANA_CHECK(m_grl->setProperty("PassThrough"    , false));
      ANA_CHECK(m_grl->initialize());
    }

  // 2.
  // initialize the CP::PileupReweightingTool
  //

  if ( m_doPUreweighting ) {

    std::vector<std::string> PRWFiles;
    std::vector<std::string> lumiCalcFiles;

    // Parse all comma seperated files
    //
    std::string token;
    std::istringstream ss("");

    Info("initialize()", "Adding Pileup files for CP::PileupReweightingTool:");
    ss.clear(); ss.str(m_PRWFileNames);
    while(std::getline(ss, token, ','))
      {
	PRWFiles.push_back(token);
	printf( "\t %s \n", token.c_str() );
      }

    Info("initialize()", "Adding LumiCalc files for CP::PileupReweightingTool:");
    ss.clear(); ss.str(m_lumiCalcFileNames);
    while(std::getline(ss, token, ','))
      {
	lumiCalcFiles.push_back(token);
	printf( "\t %s \n", token.c_str() );
    }

    ANA_CHECK(checkToolStore<CP::PileupReweightingTool>("Pileup"));
    // Based on twiki https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialxAODAnalysisInROOT
    // this command should live outside an ANA_CHECK:
    m_pileup_tool_handle.setTypeAndName("CP::PileupReweightingTool/Pileup");
    ANA_CHECK(m_pileup_tool_handle.setProperty("ConfigFiles",         PRWFiles));
    ANA_CHECK(m_pileup_tool_handle.setProperty("LumiCalcFiles",       lumiCalcFiles));
    if ( m_PU_default_channel ) {
      ANA_CHECK(m_pileup_tool_handle.setProperty("DefaultChannel",    m_PU_default_channel));
    }
    ANA_CHECK(m_pileup_tool_handle.setProperty("DataScaleFactor",     1.0/1.16));
    ANA_CHECK(m_pileup_tool_handle.setProperty("DataScaleFactorUP",   1.0));
    ANA_CHECK(m_pileup_tool_handle.setProperty("DataScaleFactorDOWN", 1.0/1.23));
    ANA_CHECK(m_pileup_tool_handle.initialize());
    //m_pileup_tool_handle->EnableDebugging(true);

  }

  ANA_MSG_INFO("Succesfully initialized!");
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode MiniTreeEventSelection :: execute ()
{
  ANA_MSG_DEBUG("Processing Event");

  // EDM
  wk()->tree()->GetEntry (wk()->treeEntry());

  if(m_info)
    {
      m_eventData->m_runNumber                     = m_info->m_runNumber      ;
      m_eventData->m_eventNumber                   = m_info->m_eventNumber    ;

      m_eventData->m_lumiBlock                     = m_info->m_lumiBlock      ;
      m_eventData->m_NPV                           = m_info->m_npv            ;
      m_eventData->m_actualInteractionsPerCrossing = m_info->m_actualMu       ;
      m_eventData->m_averageInteractionsPerCrossing= m_info->m_averageMu      ;
      m_eventData->m_weight_pileup                 = m_info->m_weight_pileup  ;

      m_eventData->m_mcEventNumber                 = m_info->m_mcEventNumber  ;
      m_eventData->m_mcChannelNumber               = m_info->m_mcChannelNumber;
      m_eventData->m_mcEventWeight                 = m_info->m_mcEventWeight  ;
    }

  if(m_jets)      m_jets     ->updateEntry();
  if(m_photons)   m_photons  ->updateEntry();
  if(m_muons)     m_muons    ->updateEntry();
  if(m_electrons) m_electrons->updateEntry();
  if(m_fatjets)   m_fatjets  ->updateEntry();
  if(m_truths)    m_truths   ->updateEntry();
  if(m_trigJets)  m_trigJets ->updateEntry();

  m_eventData->m_Zprime.SetPtEtaPhiM(m_eventData->m_Zprime_pt,
				     m_eventData->m_Zprime_eta,
				     m_eventData->m_Zprime_phi,
				     m_eventData->m_Zprime_m);

  //------------------------------------------------------------------------------------------
  // Update Pile-Up Reweighting
  //------------------------------------------------------------------------------------------
  if ( m_mc && m_doPUreweighting ) {
    m_eventData->m_weight_pileup=m_pileup_tool_handle->expert()->GetCombinedWeight( m_eventData->m_runNumber,
										    m_eventData->m_mcChannelNumber,
										    m_eventData->m_averageInteractionsPerCrossing );
  }

  //
  // Cuts
  float eventWeight    =m_eventData->m_weight * m_eventData->m_weight_pileup;

  m_cutflow->execute(m_cf_init, eventWeight);

  //------------------------------------------------------
  // If data, check if event passes GRL
  //------------------------------------------------------
  if(!m_mc && m_applyGRL)
    {
      if ( !m_grl->passRunLB( m_eventData->m_runNumber, m_eventData->m_lumiBlock ) ) 
	{
	  ANA_MSG_DEBUG("Fail GRL");

	  wk()->skipEvent();
	  return EL::StatusCode::SUCCESS; // go to next event
	}
    }
  m_cutflow->execute(m_cf_grl, eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MiniTreeEventSelection :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  return EL::StatusCode::SUCCESS;
}

