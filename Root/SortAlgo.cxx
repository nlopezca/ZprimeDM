#include <ZprimeDM/SortAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <xAODJet/JetContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODTruth/TruthParticleContainer.h>

// this is needed to distribute the algorithm to the workers
ClassImp(SortAlgo)

SortAlgo :: SortAlgo (const std::string& className) 
: ZprimeAlgorithm(className)
{
  m_inContainerName     = "";
  m_outContainerName    = "";
}

EL::StatusCode SortAlgo :: setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init("SortAlgo").ignore();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode SortAlgo :: execute ()
{
  // load all of the necessary containers
  if(sort<xAOD::JetContainer          >()==EL::StatusCode::SUCCESS) return EL::StatusCode::SUCCESS;
  if(sort<xAOD::PhotonContainer       >()==EL::StatusCode::SUCCESS) return EL::StatusCode::SUCCESS;
  if(sort<xAOD::TruthParticleContainer>()==EL::StatusCode::SUCCESS) return EL::StatusCode::SUCCESS;

  ::Error("SortAlgo::execute()", "SortAlgo called with unsupported container.");
  return EL::StatusCode::FAILURE;
}
