import ROOT
from xAODAnaHelpers import Config

import itertools

GRL = ','.join(["GoodRunsLists/data15_13TeV/20160720/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml",
                "GoodRunsLists/data16_13TeV/20170720/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"])

btagmodes=['FixedCutBEff']
btagWPs=[60,70,77,85]
btaggers=['MV2c10']

def generate_btag_detailstr(btaggers=btaggers,btagmodes=btagmodes,btagWPs=btagWPs):
    detailStr=''
    for btagger,btagmode,btagWP in itertools.product(btaggers,btagmodes,btagWPs):
        detailStr+=' jetBTag_{btagger}_{btagmode}_{btagWP}'.format(btagger=btagger,btagWP=btagWP, btagmode=btagmode)
    return detailStr

def apply_common_config(c,isMC=False,isAFII=False,triggerSelection='',
                        doSyst=False,
                        doJets=True, doFatJets=False, doTrackJets=False, doPhotons=True, doMuons=False, doElectrons=False,
                        btaggers=[],btagmodes=[],btagWPs=[],
                        msgLevel=ROOT.MSG.INFO):
    if not isMC: # Data Config
        jet_calibSeq = 'JetArea_Residual_Origin_EtaJES_GSC_Insitu'
        systName     = ''
        systVal      = 0
        jetSystVal   = ''
    else: # MC Config
        jet_calibSeq = 'JetArea_Residual_Origin_EtaJES_GSC'
        systName     = "All" if doSyst else ''
        systVal      = 1 if doSyst else 0
        jetSystVal   = '' if doSyst else '' #"0.5,1,1.5,2,2.5,3" # dijet limit setting requirement

    # AFII
    #
    if isAFII:
            JESUncertMCType = "AFII"
    else:
            JESUncertMCType = "MC15"

    # GoodRunsLists/data16_13TeV/20170215/
    BasicEventSelection = { "m_name"                  : "BasicEventSelection",
                            "m_msgLevel"              : msgLevel,
                            "m_applyGRLCut"           : True,
                            "m_GRLxml"                : GRL,
                            "m_useMetaData"           : True,
                            "m_storeTrigDecisions"    : True,
                            "m_triggerSelection"      : triggerSelection,
                            "m_applyTriggerCut"       : not isMC,
                            "m_PVNTrack"              : 2,
                            "m_applyPrimaryVertexCut" : True,
                            "m_applyEventCleaningCut" : True,
                            "m_applyCoreFlagsCut"     : True,
                            "m_doPUreweighting"       : isMC,
                            "m_PRWFileNames"          : "ZprimeDM/prwconfig.root",
                            "m_lumiCalcFileNames"     : "GoodRunsLists/data15_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root,GoodRunsLists/data16_13TeV/20170720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-009.root  ",
                            "m_duplicatesStreamName"  : 'dup_tree'
                            }

    c.algorithm("BasicEventSelection", BasicEventSelection )



    # Setup intermediate names, if needed
    doOR=doJets and doPhotons
    if doOR:
        nameJetContainer     ='Jets_Select'
        namePhotonContainer  ='Photons_Select'
        nameMuonContainer    ='Muons_Select'
        nameElectronContainer='Electrons_Select'
    else:
        nameJetContainer     ='SignalJets'
        namePhotonContainer  ='SignalPhotons'
        nameMuonContainer    ='SignalMuons'
        nameElectronContainer='SignalElectrons'

    if doJets:
        c.algorithm("JetCalibrator",      { "m_name"                    : "CalibrateJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "AntiKt4EMTopoJets",
                                            "m_outContainerName"        : "Jets_Calib",
                                            "m_outputAlgo"              : "Jets_Calib_Algo",
                                            "m_jetAlgo"                 : "AntiKt4EMTopo",
                                            "m_sort"                    : True,
                                            "m_saveAllCleanDecisions"   : True,
                                            "m_calibConfigAFII"         : "JES_MC15Prerecommendation_AFII_June2015.config",
                                            "m_calibConfigFullSim"      : "JES_data2016_data2015_Recommendation_Dec2016.config",
                                            "m_calibConfigData"         : "JES_data2016_data2015_Recommendation_Dec2016.config",
                                            "m_calibSequence"           : jet_calibSeq,
                                            "m_setAFII"                 : isAFII,
                                            "m_JESUncertConfig"         : "JES_2016/Moriond2017/JES2016_SR_Scenario1.config",
                                            "m_JESUncertMCType"         : JESUncertMCType,
                                            "m_JERUncertConfig"         : "JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root",
                                            "m_JERFullSys"              : False,
                                            "m_JERApplyNominal"         : False,
                                            "m_redoJVT"                 : False,
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_systValVectorString"     : jetSystVal
                                            } )

        c.algorithm("JetSelector",        { "m_name"                    : "SelectJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Jets_Calib",
                                            "m_inputAlgo"               : "Jets_Calib_Algo",
                                            "m_outContainerName"        : nameJetContainer,
                                            "m_outputAlgo"              : nameJetContainer+"_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 25e3,
                                            "m_eta_max"                 : 2.8,
                                            "m_useCutFlow"              : True,
                                            "m_doBTagCut"               : False,
                                            "m_doJVF"                   : False,
                                            "m_doJVT"                   : True,
                                            "m_WorkingPointJVT"         : "Medium",
                                            "m_SFFileJVT"               : "JetJvtEfficiency/Moriond2017/JvtSFFile_EM.root"
                                            } )

        for btagger,btagmode,btagWP in itertools.product(btaggers,btagmodes,btagWPs):
            btagWPstr="%s_%d"%(btagmode,btagWP)
            c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_"+nameJetContainer+"_"+btagger+"_"+btagWPstr,
                                                     "m_msgLevel"                : msgLevel,
                                                     "m_inContainerName"         : nameJetContainer,
                                                     "m_inputAlgo"               : nameJetContainer+"_Algo",
                                                     "m_systName"                : systName,
                                                     "m_systVal"                 : systVal,
                                                     "m_operatingPt"             : btagWPstr,
                                                     "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root",
                                                     "m_jetAuthor"               : "AntiKt4EMTopoJets",
                                                     "m_taggerName"              : btagger,
                                                     "m_decor"                   : "BTag"
                                                     } )


    #
    # Fat jets
    if doFatJets:
        c.algorithm("JetCalibrator",      { "m_name"                    : "CalibrateFatJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                                            "m_outContainerName"        : "FatJets_Calib",
                                            "m_outputAlgo"              : "FatJets_Calib_Algo",
                                            "m_jetAlgo"                 : "AntiKt10LCTopoTrimmedPtFrac5SmallR20",
                                            "m_sort"                    : True,
                                            "m_saveAllCleanDecisions"   : True,
                                            "m_calibConfigFullSim"      : "JES_MC15recommendation_FatJet_Nov2016_QCDCombinationUncorrelatedWeights.config",
                                            "m_calibConfigData"         : "JES_MC15recommendation_FatJet_Nov2016_QCDCombinationUncorrelatedWeights.config",
                                            "m_calibSequence"           : 'EtaJES_JMS',
                                            "m_setAFII"                 : isAFII,
                                            "m_JESUncertConfig"         : "$ROOTCOREBIN/data/JetUncertainties/UJ_2016/Moriond2017/UJ2016_CombinedMass_strong.config",
                                            "m_JESUncertMCType"         : JESUncertMCType,
                                            "m_jetCleanCutLevel"        : "LooseBad",
                                            "m_jetCleanUgly"            : True,
                                            "m_cleanParent"             : True,
                                            "m_applyFatJetPreSel"       : True,    # make sure fat-jet uncertainty is applied only in valid region
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_systValVectorString"     : jetSystVal
                                            } )

        c.algorithm("JetSelector",        { "m_name"                    : "SelectFatJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "FatJets_Calib",
                                            "m_inputAlgo"               : "FatJets_Calib_Algo",
                                            "m_outContainerName"        : "SignalFatJets",
                                            "m_outputAlgo"              : "SignalFatJets_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_cleanJets"               : True,
                                            "m_pT_min"                  : 250e3,
                                            "m_eta_max"                 : 2.0,
                                            "m_useCutFlow"              : True
                                            } )
    #
    # Track Jets
    if doTrackJets:
        c.algorithm("JetSelector", { "m_name"                    : "SelectFixedRTrackJets",
                                     "m_inContainerName"         : "AntiKt2PV0TrackJets",
                                     "m_outContainerName"        : "SignalFixedRTrackJets",
                                     "m_decorateSelectedObjects" : False,
                                     "m_createSelectedContainer" : True,
                                     "m_cleanJets"               : True,
                                     "m_pT_min"                  : 10e3,
                                     "m_eta_max"                 : 2.5,
                                     "m_useCutFlow"              : True,
                                     "m_doJVF"                   : False
                                     } )

        c.algorithm("JetSelector", { "m_name"                    : "SelectVRTrackJets",
                                     "m_inContainerName"         : "AntiKtVR30Rmax4Rmin02TrackJets",
                                     "m_outContainerName"        : "SignalVRTrackJets",
                                     "m_decorateSelectedObjects" : False,
                                     "m_createSelectedContainer" : True,
                                     "m_cleanJets"               : True,
                                     "m_pT_min"                  : 10e3,
                                     "m_eta_max"                 : 2.5,
                                     "m_useCutFlow"              : True,
                                     "m_doJVF"                   : False
                                     } )


        for btagger,btagmode,btagWP in itertools.product(btaggers,btagmodes,btagWPs):
            btagWPstr="%s_%d"%(btagmode,btagWP)
            c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_SignalFixedRTrackJets_"+btagger+"_"+btagWPstr,
                                                     "m_msgLevel"                : msgLevel,
                                                     "m_inContainerName"         : "SignalFixedRTrackJets",
                                                     "m_systName"                : systName,
                                                     "m_systVal"                 : systVal,
                                                     "m_operatingPt"             : btagWPstr,
                                                     "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root",
                                                     "m_jetAuthor"               : "AntiKt2PV0TrackJets",
                                                     "m_taggerName"              : btagger,
                                                     "m_decor"                   : "BTag",
                                                     "m_outputSystName"          : "BTag_FixedRJets"
                                                     } )

            c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_SignalVRTrackJets_"+btagger+"_"+btagWPstr,
                                                     "m_msgLevel"                : msgLevel,
                                                     "m_inContainerName"         : "SignalVRTrackJets",
                                                     "m_systName"                : systName,
                                                     "m_systVal"                 : systVal,
                                                     "m_operatingPt"             : btagWPstr,
                                                     "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root",
                                                     "m_jetAuthor"               : "AntiKt2PV0TrackJets",
                                                     "m_taggerName"              : btagger,
                                                     "m_decor"                   : "BTag",
                                                     "m_outputSystName"          : "BTag_VRJets"
                                                     } )

    #
    # Photons
    if doPhotons:
        c.algorithm("PhotonCalibrator",   { "m_name"                    : "CalibratePhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons",
                                            "m_outContainerName"        : "Photons_Calib",
                                            "m_outputAlgoSystNames"     : "Photons_Calib_Algo",
                                            "m_photonCalibMap"          : "PhotonEfficiencyCorrection/2015_2016/rel20.7/Moriond2017_v1/map0.txt",
                                            "m_tightIDConfigPath"       : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf",
                                            "m_mediumIDConfigPath"      : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMMediumSelectorCutDefs.conf",
                                            "m_looseIDConfigPath"       : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf",
                                            "m_esModel"                 : "es2016data_mc15c",
                                            "m_decorrelationModel"      : "1NP_v1",
                                            "m_useAFII"                 : isAFII,
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_sort"                    : True
                                            } )

        c.algorithm("PhotonSelector",     { "m_name"                    : "SelectPhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons_Calib",
                                            "m_inputAlgoSystNames"      : "Photons_Calib_Algo",
                                            "m_outContainerName"        : namePhotonContainer,
                                            "m_outputAlgoSystNames"     : namePhotonContainer+"_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_pT_min"                  : 10e3,
                                            "m_eta_max"                 : 2.37,
                                            "m_vetoCrack"               : True,
                                            "m_doAuthorCut"             : True,
                                            "m_doOQCut"                 : True,
                                            "m_photonIdCut"             : "Tight",
                                            "m_MinIsoWPCut"             : "FixedCutTightCaloOnly"
                                            } )

    #
    # Muons
    if doMuons:
        c.algorithm("MuonCalibrator", { "m_name"                   : "CalibrateMuons",
                                        "m_msgLevel"               : msgLevel,
                                        "m_inContainerName"        : "Muons",
                                        "m_outContainerName"       : "Muons_Calib",
                                        "m_outputAlgoSystNames"    : "Muons_Calib_Algo",
                                        "m_Years"                  : "Data15,Data16",
                                        "m_sagittaRelease"         : "sagittaBiasDataAll_25_07_17",
                                        "m_do_sagittaCorr"         : True,
                                        "m_do_sagittaMCDistortion" : False,
                                        "m_release"                : "Recs2016_15_07",
                                        "m_forceDataCalib"         : True
                                        } )

        c.algorithm("MuonSelector", { "m_name"                    : "SelectMuons", 
                                      "m_msgLevel"                : msgLevel,
                                      "m_inContainerName"         : "Muons_Calib", 
                                      "m_outContainerName"        : nameMuonContainer, 
                                      "m_createSelectedContainer" : True,
                                      "m_pT_min"                  : 10e3,
                                      "m_eta_max"                 : 2.5,
                                      "m_muonQualityStr"          : "Medium",
                                      "m_d0sig_max"               : 3,
                                      "m_z0sintheta_max"          : 1.0,
                                      "m_MinIsoWPCut"             : "Loose"
                                      } )

    #
    # Electrons
    if doElectrons:
        c.algorithm("ElectronCalibrator", { "m_name"                : "CalibrateElectrons",
                                            "m_msgLevel"            : msgLevel,
                                            "m_inContainerName"     : "Electrons",
                                            "m_outContainerName"    : "Electrons_Calib",
                                            "m_outputAlgoSystNames" : "Electrons_Calib_Algo",
                                            "m_esModel"             : "es2016PRE",
                                            "m_decorrelationModel"  : "FULL_v1"
                                            })

        c.algorithm("ElectronSelector", { "m_name"                    : "SelectElectrons",
                                          "m_inContainerName"         : "Electrons_Calib",
                                          "m_inputAlgoSystNames"      : "Electrons_Calib_Algo",
                                          "m_outContainerName"        : nameElectronContainer,
                                          "m_outputAlgoSystNames"     : nameElectronContainer+"_Algo",
                                          "m_createSelectedContainer" : True,
                                          "m_doLHPID"                 : True,
                                          "m_doLHPIDcut"              : True,
                                          "m_LHOperatingPoint"        : "Loose",
                                          "m_MinIsoWPCut"             : "LooseTrackOnly",
                                          "m_d0sig_max"               : 5.,
                                          "m_z0sintheta_max"          : 0.5,
                                          "m_pT_min"                  : 7*1000.,
                                          "m_eta_max"                 : 2.47
                                          })

    if doOR:
        OverlapRemover={ "m_name"                    : "RemoveOverlaps",
                         "m_outputAlgoSystNames"     : "ORAlgo_Syst",
                         "m_createSelectedContainers": True}
        if doJets:
            OverlapRemover["m_inContainerName_Jets"]      =nameJetContainer
            OverlapRemover["m_inputAlgoJets"]             =nameJetContainer+"_Algo"
            OverlapRemover["m_outContainerName_Jets"]     ="SignalJets"
        if doPhotons:
            OverlapRemover["m_inContainerName_Photons"]   =namePhotonContainer
            OverlapRemover["m_inputAlgoPhotons"]          =namePhotonContainer+"_Algo"
            OverlapRemover["m_outContainerName_Photons"]  ="SignalPhotons"
        if doMuons:
            OverlapRemover["m_inContainerName_Muons"]     =nameMuonContainer
            OverlapRemover["m_inputAlgoMuons"]            =nameMuonContainer+"_Algo"
            OverlapRemover["m_outContainerName_Muons"]    ="SignalMuons"
        if doElectrons:
            OverlapRemover["m_inContainerName_Electrons"] =nameElectronContainer
            OverlapRemover["m_inputAlgoElectrons"]        =nameElectronContainer+"_Algo"
            OverlapRemover["m_outContainerName_Electrons"]="SignalElectrons"

        c.algorithm("OverlapRemover", OverlapRemover)


def findAlgo(c,name):
    for algo in c._algorithms:
        if algo.m_name==name: return algo
    return None
