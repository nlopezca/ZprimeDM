import ROOT
from xAODAnaHelpers import Config, utils
if utils.is_release21():
    from ZprimeDM import commonconfigPFlow as commonconfig
else:
    from ZprimeDM import commonconfigR20p7 as commonconfig
import datetime

date=datetime.datetime.now().strftime("%Y%m%d")

#trigger stuff
#singlejettrig = 'HLT_j[0-9]*.*'
singlejettrig = 'HLT_j100* | HLT_j100*_jes | HLT_j110* | HLT_j110*_jes | HLT_j380 | HLT_j400 | HLT_j420 | HLT_j225_gsc400_boffperf_split | HLT_j225_gsc420_boffperf_split '
#singlejettrig = ''
multijettrig = 'HLT_3j200 '
httrig = 'HLT_ht1000_L1J100 '
twobplusonejtrig = 'HLT_j100_2j55_bmv2c2060_split '
twobplushttrig = 'HLT_2j55_bmv2c2060_split_ht300_L14J15 '

c = Config()

#training = "_BTagging201810"
training = "_BTagging201903"

usebtags = []
if training == "_BTagging201810":
    usebtags = ["DL1","MV2c10"]
elif training == "_BTagging201903":
    usebtags = ["DL1","DL1r"]

btagmodes = ["FixedCutBEff"] #latest CDI only supports fixed cut

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 #triggerSelection='L1_J[0-9]*|L1_[0-9]J[0-9]*|HLT_j[0-9]*.*|HLT_noalg_J[0-9]*|HLT_[0-9]j*.*|HLT_ht[0-9]*.*', #old! too broad
                                 triggerSelection=singlejettrig+' | '+multijettrig+' | '+httrig+' | '+twobplusonejtrig+' | '+twobplushttrig,
                                 GRLset='ALLGOOD',
                                 doJets=True,doPhotons=False,doHLTObjects=True,
                                 doSyst=False,
                                 jetBtaggers=usebtags,jetBtagmodes=btagmodes,jetBtagWPs=commonconfig.btagWPs,jetBtagTraining=training,
                                 args=args.extra_options)

jetDetailStr="kinematic trackPV flavorTag %s"%(commonconfig.generate_btag_detailstr(btaggers=usebtags,btagmodes=btagmodes))
if args.is_MC: jetDetailStr+=" truth"
else : jetDetailStr+="energy layer "
# For jets in systematically varied trees: need enough to run full analysis
jetDetailStrSyst = "kinematic trackPV flavorTag %s"%(commonconfig.generate_btag_detailstr(btaggers=usebtags,btagmodes=btagmodes))
#jetDetailStrSyst = jetDetailStr

containers=ROOT.vector('ZprimeNtuplerContainer')()
# Second string is for systematics! If you want them to be the same as for nominal, need to say so.
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET, 'SignalJets'   ,'jet',jetDetailStr                          ,jetDetailStrSyst))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET, 'SelectedHLTJets', 'HLT_jet', "kinematic", "kinematic"))

c.algorithm("ZprimeNtupler",          { "m_name"                : "AnalysisAlgo",
                                        "m_inputAlgo"           : "SignalJets_Algo",
                                        "m_containers"          : containers,
                                        "m_eventDetailStr"      : "pileup",
                                        "m_trigDetailStr"       : "passTriggers passTrigBits",
#                                        "m_msgLevel"            : 1,
                                        } )
