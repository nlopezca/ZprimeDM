import ROOT
from xAODAnaHelpers import Config

commonsel={"m_mc"                     : args.is_MC,
           "m_debug"                  : False,
           "m_jetDetailStr"           : "kinematic energy clean trackPV",
           "m_doPUReweight"           : True,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True,
           "m_trigger"                : "HLT_g35_loose_g25_loose",
           }

c = Config()

GRL = "GoodRunsLists/data15_13TeV/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml,GoodRunsLists/data16_13TeV/data16_13TeV.periodAllYear_DetStatus-v80-pro20-08_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml"

#
# Process Ntuple
#

c.algorithm("MiniTreeEventSelection", { "m_name"                   : "",
                                        "m_debug"                  : False,
                                        "m_mc"                     : args.is_MC,
                                        "m_GRLxml"                 : GRL,
                                        "m_applyGRL"               : not args.is_MC,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"           : "kinematic energy clean trackPV",
                                        "m_photonDetailStr"        : "kinematic PID isolation"
                                        } )

diphoton=commonsel.copy()
diphoton.update({"m_name"            : "diphoton",
                 "m_photon0PtCut"    : 25,
                 "m_photon1PtCut"    : 25})
c.algorithm("DiphotonHistsAlgo", diphoton )

diphoton_scalar=commonsel.copy()
diphoton_scalar.update({"m_name"            : "diphoton_scalar",
                        "m_photon0PtCut"    : 25,
                        "m_photon1PtCut"    : 25,
                        "m_scalarCuts"      : True})
c.algorithm("DiphotonHistsAlgo", diphoton_scalar )

diphoton_ystar=commonsel.copy()
diphoton_ystar.update({"m_name"          : "diphoton_ystar",
                       "m_photon0PtCut"  : 25,
                       "m_photon1PtCut"  : 25,
                       "m_YStarCut"      : 0.6})
c.algorithm("DiphotonHistsAlgo", diphoton_ystar )

