import ROOT
from xAODAnaHelpers import Config

c = Config()

c.algorithm("BasicEventSelection",          { "m_truthLevelOnly"      : True,
                                              "m_derivationName"      : "TRUTH1",
                                              "m_useMetaData"         : False
                                              } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "AntiKt4TruthJets",
                                       "m_outContainerName"        :  "AntiKt4TruthJetsSort"
                                       } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "TruthPhotons",
                                       "m_outContainerName"        :  "TruthPhotonsSort"
                                       } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "TruthMuons",
                                       "m_outContainerName"        :  "TruthMuonsSort"
                                       } )

c.algorithm("JetSelector",                  { "m_inContainerName"         :  "AntiKt4TruthJetsSort",
                                              "m_outContainerName"        :  "SignalJets",
                                              "m_decorateSelectedObjects" :  False, 
                                              "m_createSelectedContainer" :  True
                                              } )

c.algorithm("TruthSelector",                { "m_inContainerName"         :  "TruthPhotonsSort",
                                              "m_outContainerName"        :  "SignalPhotons",
                                              "m_decorateSelectedObjects" :  False,
                                              "m_createSelectedContainer" :  True
                                              } )

c.algorithm("TruthSelector",                { "m_inContainerName"         :  "TruthMuonsSort",
                                              "m_outContainerName"        :  "SignalMuons",
                                              "m_decorateSelectedObjects" :  False,
                                              "m_createSelectedContainer" :  True
                                              } )

c.algorithm("TruthPhotonJetOR",             { "m_jetContainerName"    : "SignalJets",
                                              "m_photonContainerName" : "SignalPhotons",
                                              "m_outJetContainerName" : "SignalJetswoPhoton",
                                              "m_minDR"               : 0.4
                                              } )

c.algorithm("ZprimeGammaJetJetHistsAlgo",   { "m_name"                : "gammajetjet_all",
                                              "m_jetContainerName"    : "SignalJetswoPhoton",
                                              "m_photonContainerName" : "SignalPhotons",
                                              "m_muonContainerName"   : "SignalMuons",
                                              "m_minJetPt"            : 0,
                                              "m_minPhotonPt"         : 0
                                              } )

c.algorithm("ZprimeGammaJetJetHistsAlgo",   { "m_name"                : "gammajetjet_HLT_g140_ystar",
                                              "m_jetContainerName"    : "SignalJetswoPhoton",
                                              "m_photonContainerName" : "SignalPhotons",
                                              "m_muonContainerName"   : "SignalMuons",
                                              "m_minJetPt"            : 25,
                                              "m_minPhotonPt"         : 140,
                                              "m_jjYStarCut"          : 0.8
                                              } )

c.algorithm("ZprimeGammaJetJetHistsAlgo",   { "m_name"                : "gammajetjet_HLT_g120_ystar",
                                              "m_jetContainerName"    : "SignalJetswoPhoton",
                                              "m_photonContainerName" : "SignalPhotons",
                                              "m_muonContainerName"   : "SignalMuons",
                                              "m_minJetPt"            : 25,
                                              "m_minPhotonPt"         : 120,
                                              "m_jjYStarCut"          : 0.8
                                              } )

for photonPt in [35,45,55,65,75]:
    for jetPt in [25, 50]:
        c.algorithm("ZprimeGammaJetJetHistsAlgo",   { "m_name"                : "gammajetjet_HLT_g%d_2j%d_ystar"%(photonPt,jetPt),
                                                      "m_jetContainerName"    : "SignalJetswoPhoton",
                                                      "m_photonContainerName" : "SignalPhotons",
                                                      "m_muonContainerName"   : "SignalMuons",
                                                      "m_minJetPt"            : jetPt,
                                                      "m_minPhotonPt"         : photonPt,
                                                      "m_jjYStarCut"          : 0.8
                                                      } )
