import ROOT
from xAODAnaHelpers import Config, utils
if utils.is_release21():
    from ZprimeDM import commonconfig
else:
    from ZprimeDM import commonconfigR20p7 as commonconfig

c = Config()

jetDetailStr="kinematic clean"
if args.is_MC: jetDetailStr+=" truth"

fatjetDetailStr="kinematic substructure constituent trackJetName_GhostVR30Rmax4Rmin02TrackJet"
if args.is_MC: fatjetDetailStr+=" bosonCount"
fatjetDetailStrSyst="kinematic substructure constituent trackJetName_GhostVR30Rmax4Rmin02TrackJet"

subjetDetailStr="kinematic flavorTag constituent %s"%(commonconfig.generate_btag_detailstr())
muonDetailStr="kinematic isolationKinematics"
elDetailStr="kinematic"

btaggers =commonconfig.btaggers  if args.is_MC else ['DL1r']
btagmodes=commonconfig.btagmodes if args.is_MC else ['FixedCutBEff']
btagWPs  =commonconfig.btagWPs   if args.is_MC else [85,77]

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 triggerSelection='HLT_g140+_loose',
                                 GRLset='ALLGOOD',
                                 doJets=False,doFatJets=True,doTrackJets=commonconfig.DoTrackJets.FALSE,
                                 doPhotons=True,doMuons=False,doElectrons=False,
                                 fatJetBtaggers =commonconfig.btaggers  if args.is_MC else ['DL1r'],
                                 fatJetBtagmodes=commonconfig.btagmodes if args.is_MC else ['FixedCutBEff'],
                                 fatJetBtagWPs  =commonconfig.btagWPs   if args.is_MC else [85,77],
                                 msgLevel = ROOT.MSG.INFO,
                                 args=args.extra_options)

SelectFatJets=commonconfig.findAlgo(c,"SelectFatJets")
SelectFatJets.m_pass_min=1

containers=ROOT.vector('ZprimeNtuplerContainer')()

containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.PHOTON,'SignalPhotons','ph' ,'kinematic PID effSF isolation','kinematic PID effSF isolation'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.FATJET  ,'SignalFatJets'                              ,'fatjet'  ,fatjetDetailStr  ,fatjetDetailStrSyst))
containers.back().m_subjetDetailStr=subjetDetailStr
containers.back().m_subjetDetailStrSyst="kinematic {}".format(commonconfig.generate_btag_detailstr(btaggers=['MV2c10'], btagmodes=['FixedCutBEff'], btagWPs=[85,77]))

c.algorithm("ZprimeNtupler",       { "m_inputAlgo"           : "SignalFatJets_Algo",
                                     "m_containers"          : containers,
                                     "m_eventDetailStr"      : "pileup weightsSys",
                                     "m_trigDetailStr"       : "passTriggers passTrigBits",
                                     "m_calcExtraVariables"  : True
                                     } )
