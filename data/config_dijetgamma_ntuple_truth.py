import ROOT
from xAODAnaHelpers import Config

c = Config()

c.algorithm("BasicEventSelection",          { "m_name"                : "TruthBasicEvSelAlgo",
                                              "m_truthLevelOnly"      : True,
#                                              "m_msgLevel"            : 1,
                                              "m_useMetaData"         : False
                                              } )

c.algorithm("SortAlgo",              { "m_name"                    :  "TruthJetSortAlgo",
                                       "m_inContainerName"         :  "AntiKt4TruthDressedWZJets",
#                                       "m_msgLevel"                :  1,
                                       "m_outContainerName"        :  "AntiKt4TruthJetsSort"
                                       } )

c.algorithm("SortAlgo",              { "m_name"                    :  "TruthPhotonSortAlgo",
                                       "m_inContainerName"         :  "TruthPhotons",
#                                       "m_msgLevel"                :  1,
                                       "m_outContainerName"        :  "TruthPhotonsSort"
                                       } )

c.algorithm("JetSelector",                  { "m_name"                    :  "TruthJetSelectorAlgo",
                                              "m_inContainerName"         :  "AntiKt4TruthJetsSort",
                                              "m_outContainerName"        :  "SignalJets",
                                              "m_decorateSelectedObjects" :  False, 
                                              "m_createSelectedContainer" :  True, 
                                              "m_pT_min"                  :  25e3,
                                              "m_eta_max"                 :  2.8,
                                              "m_pass_min"                :  2
                                              } )

c.algorithm("TruthSelector",                { "m_name"                    :  "TruthSelectorAlgo",
                                              "m_inContainerName"         :  "TruthPhotonsSort",
                                              "m_outContainerName"        :  "SignalPhotons",
                                              "m_decorateSelectedObjects" :  False,
                                              "m_createSelectedContainer" :  True,
                                              "m_pT_min"                  :  25e3,
                                              "m_eta_max"                 :  2.5,
                                              "m_pass_min"                :  1
                                              } )

c.algorithm("TruthPhotonJetOR",             { "m_name"                :  "TruthORAlgo",
                                              "m_jetContainerName"    : "SignalJets",
                                              "m_photonContainerName" : "SignalPhotons",
                                              "m_outJetContainerName" : "SignalJetswoPhoton",
                                              "m_minDR"               : 0.2
                                              } )

containers=ROOT.vector('ZprimeNtuplerContainer')()
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'SignalJetswoPhoton','jet','kinematic truth truthDetails'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.TRUTH ,'SignalPhotons'     ,'ph','kinematic'))

c.algorithm("ZprimeNtupler",                { "m_name"           : "TruthZPrimeAlgo",
                                              "m_truthLevelOnly" : True,
                                              "m_containers"     : containers
                                              } )
