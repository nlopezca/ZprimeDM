import ROOT
from xAODAnaHelpers import Config

c = Config()

c.algorithm("BasicEventSelection",          { "m_name"                : "TruthBasicEvSelAlgo",
                                              "m_truthLevelOnly"      : True,
#                                              "m_msgLevel"            : 1,
                                              "m_useMetaData"         : False
                                              } )

c.algorithm("SortAlgo",              { "m_name"                    :  "TruthJetSortAlgo",
#                                       "m_msgLevel"                : 1,
                                       "m_inContainerName"         :  "AntiKt4TruthDressedWZJets",
                                       "m_outContainerName"        :  "AntiKt4TruthJetsSort"
                                       } )

c.algorithm("JetSelector",                  { "m_name"                    :  "TruthJetSelectorAlgo",
                                              "m_inContainerName"         :  "AntiKt4TruthJetsSort",
                                              "m_outContainerName"        :  "SignalJets",
#                                              "m_msgLevel"            : 1,
                                              "m_decorateSelectedObjects" :  False, 
                                              "m_createSelectedContainer" :  True, 
                                              "m_pT_min"                  :  25e3,
                                              "m_eta_max"                 :  2.8,
                                              "m_pass_min"                :  2
                                              } )

containers=ROOT.vector('ZprimeNtuplerContainer')()
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'SignalJets'   ,'jet','kinematic'))

c.algorithm("ZprimeNtupler",                { "m_name"           : "TruthZPrimeAlgo",
                                              "m_truthLevelOnly" : True,
                                              "m_containers"     : containers
                                              } )
