import ROOT
from xAODAnaHelpers import Config, utils
if utils.is_release21():
    from ZprimeDM import commonconfigPFlow as commonconfig
else:
    from ZprimeDM import commonconfigR20p7 as commonconfig
import datetime

date=datetime.datetime.now().strftime("%Y%m%d")

c = Config()

#training = "_BTagging201810"
training = "_BTagging201903"

usebtags = []

if training == "_BTagging201810":
    usebtags = ["DL1","MV2c10"]
elif training == "_BTagging201903":
    usebtags = ["DL1","DL1r"]

btagmodes = ["FixedCutBEff"]

# For jets in nominal tree
jetDetailStr="kinematic trackPV flavorTag %s"%(commonconfig.generate_btag_detailstr(btaggers=usebtags,btagmodes=btagmodes))
if args.is_MC: jetDetailStr+=" truth"
else : jetDetailStr+="energy layer "
# For jets in systematically varied trees: need enough to run full analysis. Don't need truth.
jetDetailStrSyst = "kinematic trackPV flavorTag %s"%(commonconfig.generate_btag_detailstr(btaggers=usebtags,btagmodes=btagmodes))

# Lowest unprescaled single photon triggers:
# - HLT_g120_loose (2015)
# - HLT_g140_loose (2016)
# - HLT_g140_loose (2017)
# Lowest unprescaled photon + 3j (no overlap removal) triggers:
# - HLT_g20_loose_L1EM18VH_2j40_0eta490_3j25_0eta490_invm700 (2015) (yuck)
# - HLT_g75_tight_3j50noL1_L1EM22VHI (2016)
# - HLT_g85_tight_L1EM22VHI_3j50noL1 (2017)
# Prescaled single photon triggers also included for trigger turn-on studies.

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 triggerSelection='HLT_g[0-9]+_loose|HLT_g[0-9]+_tight|HLT_g75_tight_3j50noL1_L1EM22VHI|HLT_g85_tight_L1EM22VHI_3j50noL1|HLT_g75_tight_3j[0-9]+noL1_L1EM22VHI|HLT_g20_loose_L1EM18VH_2j40_0eta490_3j25_0eta490_invm700|HLT_3j200|HLT_3j50|',
                                 GRLset='ALLGOOD',
                                 doJets=True,doPhotons=True,doHLTObjects=True,
                                 doSyst=False,
                                 jetBtaggers=usebtags,jetBtagmodes=btagmodes,jetBtagWPs=commonconfig.btagWPs,jetBtagTraining=training,#msgLevel=ROOT.MSG.DEBUG,
                                 args=args.extra_options)

SelectPhotons=commonconfig.findAlgo(c,"SelectPhotons")
SelectPhotons.m_pT_min=30

containers=ROOT.vector('ZprimeNtuplerContainer')()
# Second string is for systematics! If you want them to be the same as for nominal, need to say so.
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.PHOTON    ,'SignalPhotons','ph' ,'kinematic PID effSF','kinematic PID effSF'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET       ,'SignalJets'   ,'jet',jetDetailStr                          ,jetDetailStrSyst))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET       ,'SelectedHLTJets', 'HLT_jet', "kinematic", "kinematic"))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.HLT_PHOTON,'HLT_xAOD__PhotonContainer_egamma_Photons', 'HLT_photon', "kinematic", "kinematic"))

c.algorithm("ZprimeNtupler",          { "m_name"                : "AnalysisAlgo",
                                        "m_inputAlgo"           : "ORAlgo_Syst",
                                        "m_containers"          : containers,
                                        "m_eventDetailStr"      : "pileup",
                                        "m_trigDetailStr"       : "passTriggers passTrigBits"
                                        } )
