#!/usr/bin/env python

import ROOT

import argparse

parser = argparse.ArgumentParser(description="Remove entry in a ROOT file.")
parser.add_argument('path',nargs='+',help="Path to remove in format file:/path.")
args = parser.parse_args()


for path in args.path:
    parts=path.split(':/')
    if len(parts)!=2:
        print('Wrong format for ',parts)
        continue
    if parts[1]=='':
        print('Empty path')
        continue

    fh=ROOT.TFile.Open(parts[0],'update')
    fh.Delete(parts[1]+';*')
    fh.Close()
