#!/usr/bin/env python

import sys
import glob
import os, os.path
import shutil
import multiprocessing

import ROOT
import cmdLineUtils

if len(sys.argv)!=3:
    print('usage: {0} oldhistsdir newhistsdir'.format(sys.argv[0]))
    sys.exit(-1)

oldhistdir=sys.argv[1]
newhistdir=sys.argv[2]

def recurse_purge(d):
    for key in d.GetListOfKeys():
        obj=key.ReadObj()
        if obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            recurse_purge(obj)
    d.Purge()

def recurse_ls_tfile(d,prep=''):
    result=[]
    for key in d.GetListOfKeys():
        name=key.GetName()
        obj=key.ReadObj()
        if obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            result+=recurse_ls_tfile(obj,'{0}{1}/'.format(prep,name))
        else:
            result.append(('{0}{1}'.format(prep,name),obj))
    return result
        
def update_hist(newhistfile):
    histfile=os.path.basename(newhistfile)

    oldhistfile='{0}/{1}'.format(oldhistdir,histfile)

    if os.path.exists(oldhistfile): # Update dictionary
        shutil.copyfile(oldhistfile,'{0}/bkp/{1}'.format(oldhistdir,histfile))

        sourceList=cmdLineUtils.patternToFileNameAndPathSplitList(newhistfile+':*',wildcards=True)
        cmdLineUtils.rootCp(sourceList, oldhistfile, [],
                            recreate=False, recursive=True, replace=True)

        # Cleanup old cycles
        fh_old=ROOT.TFile.Open(oldhistfile,'UPDATE')
        ROOT.gROOT.GetListOfFiles().Remove(fh_old); # ROOT sucks
        recurse_purge(fh_old)
        fh_old.Close()
    else: # just copy the new one
        shutil.copyfile(newhistfile,oldhistfile)

    return True

# Prepare a backup, just in case
if not os.path.exists('{0}/bkp'.format(oldhistdir)):
    os.mkdir('{0}/bkp'.format(oldhistdir))

newhistfiles=glob.glob('{0}/hist*.root'.format(newhistdir))
workers = multiprocessing.Pool(10)
for result in workers.imap_unordered(update_hist,newhistfiles):
    pass
