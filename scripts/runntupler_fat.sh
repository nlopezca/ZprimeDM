#!/bin/bash

function abortError {
    echo "FAILED ${1}"
    exit -1
}

if [ ${#} != 1 ]; then
    echo "usage: ${0} direct/condor"
    exit -1
fi
MODE=${1}

#
# Declare options for running with different drivers
#  RUNCODE = For running on offical samples
#  RUNCODECONT = For running on private rucio containers (needed for grid to get rid of user.${USER} prefix in outname
#
if [ "x${MODE}x" == "xcondorx" ]; then
    RUNCODE="condor --optFilesPerWorker 5 --optBatchWait"
    RUNCODECONT=${RUNCODE}
elif [ "x${MODE}x" == "xgridx" ]; then
    TAG=20170702-01
    RUNCODE="prun --optGridDestSE=NERSC_LOCALGROUPDISK  --optGridOutputSampleName=user.%nickname%.%in:name[1]%.%in:name[2]%.%in:name[3]%.SELECTION.${TAG}/" # --optSubmitFlags=\"--useNewCode\""
    RUNCODECONT="prun --optGridDestSE=NERSC_LOCALGROUPDISK  --optGridOutputSampleName=user.%nickname%.%in:name[3]%.%in:name[4]%.SELECTION.${TAG}/" # --optSubmitFlags=\"--allowTaskDuplication\""
else
    #RUNCODE="--nevents 1000 direct"
    RUNCODE="direct"
    RUNCODECONT=${RUNCODE}
fi

#
# List of input dataset lists
# String TYPE will be replaced by the necessary DxAOD name
FILES_MC="ZprimeDM/filelists/Signal_trijet.TYPE.list ZprimeDM/filelists/Pythia8_jetjet.TYPE.list ZprimeDM/filelists/Pythia8_jetjet_JZ.TYPE.list ZprimeDM/filelists/Sherpa_jetjet.TYPE.list"
FILES_MC="ZprimeDM/filelists/Pythia8_jetjet.TYPE.list"
FILES_DATA="ZprimeDM/filelists/data15.TYPE.list ZprimeDM/filelists/data16.TYPE.list"

#
# Prepare output directory
TAG=$(date +%Y%m%d)-${MODE}
DATADIR=$(pwd) #$(mktemp -d --suffix ${USER}_ZPrimeDM_${TAG})

#
# Run!
#

#
# Full sim
./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_fatjet_ntuple.py         --files ZprimeDM/filelists/Higgs_ggFHbb.EXOT8.txt --inputList --isMC          --submitDir ${DATADIR}/OUT_fatjet_ntuple              --force ${RUNCODE/SELECTION/fatjet}             || abortError OUT_fatjet_ntuple
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_fatjet_ntuple.py         --files ${FILES_MC//TYPE/EXOT8}               --inputList --inputRucio --isMC          --submitDir ${DATADIR}/OUT_fatjet_ntuple              --force ${RUNCODE/SELECTION/fatjet}             || abortError OUT_fatjet_ntuple
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_fatjet_ntuple.py         --files ${FILES_DATA//TYPE/EXOT2.cont}        --inputList --inputRucio                 --submitDir ${DATADIR}/OUT_fatjet_ntuple_data         --force ${RUNCODECONT/SELECTION/fatjet}         || abortError OUT_fatjet_ntuple_data

#
# Clean-up
#rm -rf ${DATADIR}