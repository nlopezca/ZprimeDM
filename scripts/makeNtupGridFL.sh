#!/bin/bash

if [ ${#} != 3 ]; then
    echo "usage: ${0} dslist selection date"
    exit -1
fi

dslist=${1}
selection=${2}
date=${3}

dirpath=$(dirname ${dslist})

# Prepare output
outlist=${dirpath}/$(basename ${dslist} | cut -f 1 -d '.').${selection}.NTUP.list
echo ${outlist}
if [ -e ${outlist} ]; then
    rm ${outlist}
fi

# Determine campaign
campaign=$(basename ${dirpath})
if [ "${campaign}" == "filelists" ]; then
    campaign=''
fi

# loop over filelists
for dsname in $(cat ${dslist})
do
    echo ${dsname}

    if [[ "${dslist}" == *".cont."* ]]; then
	dsprefix=$(echo ${dsname} | cut -f 3-4 -d '.')
    elif [[ "x${campaign}x" == "xx" ]]; then
	dsprefix=$(echo ${dsname} | cut -f -3 -d '.')
    else
	dsprefix=${campaign}.$(echo ${dsname} | cut -f 2-3 -d '.')
    fi

    rucio list-dids user.kkrizka.${dsprefix}.${selection}.${date}_tree.root/ | grep CONTAINER | awk '{print $2}' | cut -f 2 -d ':' >> ${outlist}
done
