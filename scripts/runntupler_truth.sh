#!/bin/bash

function abortError {
    echo "FAILED ${1}"
    exit -1
}

if [ ${#} != 1 ]; then
    echo "usage: ${0} direct/condor"
    exit -1
fi
MODE=${1}

#
# Declare options for running with different drivers
#  RUNCODE = For running on offical samples
#
if [ "x${MODE}x" == "xcondorx" ]; then
    RUNCODE=(condor --optFilesPerWorker 5 --optBatchWait)
elif [ "x${MODE}x" == "xgenginex" ]; then
    RUNCODE=(gridengine --optFilesPerWorker 1 --optBatchWait --optBatchSharedFileSystem 1 --optSubmitFlags="-S /bin/bash -l cvmfs=1,h_vmem=3G" --shellInit "shopt -s expand_aliases")
else
    RUNCODE=(--nevents 1000 direct)
fi

#
# List of input dataset lists
# String TYPE will be replaced by the necessary DxAOD name
FILES_GAMMAJETJET_TRUTH="ZprimeFilelists/filelists/MC15.*.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph*_*.TRUTH1.txt"
FILES_JETJET_TRUTH="ZprimeFilelists/filelists/MC15.*.MGPy8EG_N30LO_A14N23LO_dmA_jjj_J*_*.TRUTH1.txt ZprimeFilelists/filelists/MC15.*.MGPy8EG_N30LO_A14N23LO_dmA_jj_*.TRUTH1.txt"

#
# Prepare output directory
TAG=$(date +%Y%m%d)-${MODE}
DATADIR=/global/projecta/projectdirs/atlas/${USER}/data/${TAG}
if [ ! -e ${DATADIR} ]; then
    mkdir ${DATADIR}
fi

#
# Run!
#

#
# Truths
./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_dijet_ntuple_truth.py       --files ${FILES_JETJET_TRUTH}                             --inputList --submitDir ${DATADIR}/OUT_dijet_ntuple_truth       --force "${RUNCODE[@]}" || abortError OUT_dijet_ntuple_truth

./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_gammajetjet_ntuple_truth.py --files ${FILES_GAMMAJETJET_TRUTH}                        --inputList --submitDir ${DATADIR}/OUT_gammajetjet_ntuple_truth --force "${RUNCODE[@]}" || abortError OUT_gammajetjet_ntuple_truth
