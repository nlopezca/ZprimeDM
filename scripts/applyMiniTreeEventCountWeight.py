#!/usr/bin/env python

import ROOT

import sys
import glob
import os, os.path
import shutil
import argparse
import multiprocessing
import re

EXTRA_WEIGHT={}

# Sherpa 2.1 W+jets
EXTRA_WEIGHT[304307]=1.28
EXTRA_WEIGHT[304308]=1.28
EXTRA_WEIGHT[304309]=1.28

# Sherpa 2.1 Z+jets
EXTRA_WEIGHT[304707]=1.37
EXTRA_WEIGHT[304708]=1.37
EXTRA_WEIGHT[304709]=1.37

re_run=re.compile('.*mc[0-9]+[0-9a-zA-Z_]+\.([0-9]+)\..*')

#
# Run numbers for which to fix the denominator to be
# the sum of weights (True) or event counts (False)
# Key is run number
OVERRIDE_UNWEIGHTED_NORM={}

def rescale_file(rootfile):
    # Get extra information
    rootfilebase=os.path.basename(rootfile)
    match=re_run.match(rootfilebase)
    run=int(match.group(1)) if match!=None else 0
    extra_weight=EXTRA_WEIGHT.get(run,1.)*args.extra
    unweighted=OVERRIDE_UNWEIGHTED_NORM.get(run,args.sumofevents)
    a="%s  %d  %g"%(rootfile,run,extra_weight)

    # Check if backup file exists
    origrootfile='%s/orig/%s'%(args.sampledir,os.path.basename(rootfile))
    if not os.path.exists(origrootfile): # Create backup
        shutil.copyfile(rootfile,origrootfile)

    fh_out =ROOT.TFile.Open(rootfile,'recreate')
    fh_hist=ROOT.TFile.Open(origrootfile)

    # Get event counts
    EventCount=None
    
    MetaData_EventCount=fh_hist.Get('MetaData_EventCount')
    if MetaData_EventCount!=None:
        EventCount=MetaData_EventCount.GetBinContent(3 if not unweighted else 1)
    elif fh_hist.Get('cutflow' if unweighted else 'cutflow_weighted')!=None:
        cutflow=fh_hist.Get('cutflow' if unweighted else 'cutflow_weighted')
        EventCount=cutflow.GetBinContent(1)
    else:
        path_md='%s/data-metadata/%s'%(args.sampledir,os.path.basename(rootfile)[5:])
        if os.path.exists(path_md):
            fh_md=ROOT.TFile.Open(path_md)
            MetaData_EventCount=fh_md.Get('MetaData_EventCount')
            EventCount=MetaData_EventCount.GetBinContent(3 if not unweighted else 1)
            fh_md.Close()

    recursive_rescale(fh_hist,fh_out,EventCount,extra_weight)

    fh_out .Close()
    fh_hist.Close()

    return (rootfile,EventCount,extra_weight)

def recursive_rescale(indir,outdir,EventCount,extra_weight=1.):
    keys=indir.GetListOfKeys()

    for key in keys:
        name=key.GetName()
        obj=key.ReadObj()
        if obj.InheritsFrom(ROOT.TH1.Class()):
            outdir.cd()
            if(EventCount!=0): obj.Scale(extra_weight/EventCount)
            obj.Write()
        elif obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            newoutdir=outdir.mkdir(obj.GetName())
            recursive_rescale(obj,newoutdir,EventCount,extra_weight)
        obj.Delete()

parser = argparse.ArgumentParser(description="Normalize histograms by number of events")
parser.add_argument('sampledir',help="Path of sampledir containing histograms.")
parser.add_argument('-n','--sumofevents',action='store_true',help="Normalize using sum of events insteada of sum of weights.")
parser.add_argument('-e','--extra',type=float,default=1.,help="Apply an extra weight to all histograms.")
parser.add_argument('-f','--filter',type=str,default='*',help="Run only over files matching pattern hist-FILTER.root")
args = parser.parse_args()

# Check if backup directory exists
if not os.path.isdir('%s/orig'%args.sampledir):
    os.makedirs('%s/orig'%args.sampledir)

rootfiles=glob.glob('%s/hist-%s.root'%(args.sampledir,args.filter))

workers = multiprocessing.Pool(6)
for result in workers.imap_unordered(rescale_file,rootfiles):
    print(result)
# for rootfile in rootfiles:
#     print(rescale_file(rootfile))
