trap 'echo "KILL $$"; kill -- -$$' EXIT

if [ ! -d logs ]; then
    mkdir logs
fi

function runOne {
    CONFIG=${1}    
    OUTNAME=${2}
    EXTRA=${3}
    FILELIST=${@:4}

    # only allow 4 processes at a time
    while [ $(ps --no-headers --ppid=$$ | grep $(basename ${0}) | wc -l) -gt 4 ]; do
	sleep 60
    done

    runone.sh -d ${DATADIR} ${EXTRA} ${MODE} ${CONFIG} ${OUTNAME} ${FILELIST} > logs/${OUTNAME}.out 2> logs/${OUTNAME}.err || { echo "Error in ${OUTNAME}" && kill $$; } &

    echo "${OUTNAME}"
}

function mergemc {
    OUTNAME=${1}
    OUTROOT=${2}
    IFS=' ' read -a FILELIST <<< "${@:3}"

    pushd ${DATADIR}/OUT_${OUTNAME}
    for start in $(ls ${FILELIST} | sed -r 's/^hist-([a-z]+\.[a-z]+\.[a-zA-Z1-9\_]+).*/\1/g' | uniq)
    do
	thetype=${start##*.}

	hadd -j 8 -n 0 -f hist-${thetype}-${OUTROOT} ${FILELIST[@]/#/hist-${start}} || exit -1
	for i in data-*
	do
	    hadd -j 8 -n 0 -f ${i}/${thetype}-${OUTROOT} ${FILELIST[@]/#/${i}/${start}} || exit -1
	done
    done
    popd
}

function merge {
    OUTNAME=${1}
    OUTROOT=${2}
    FILELIST=${@:3}

    pushd ${DATADIR}/OUT_${OUTNAME}
    hadd -j 8 -n 0 -f hist-${OUTROOT} hist-${FILELIST} || exit -1
    for i in data-*
    do
	hadd -j 8 -n 0 -f ${i}/${OUTROOT} $(find ${i} -type f -name "${FILELIST}") # || exit -1b
    done
    popd
}

function mergeSys {
    OUTNAME=${1}
    SYSLIST=${@:2}

    if [ -e ${DATADIR}/OUT_${OUTNAME} ]; then
	rm -rf ${DATADIR}/OUT_${OUTNAME}
    fi
    mkdir ${DATADIR}/OUT_${OUTNAME}

    # Merge histograms
    for path in ${DATADIR}/OUT_${OUTNAME}_nominal/hist-*
    do
	name=$(basename ${path})

	SYSFILES=""
	ROOTRM=""
	for sys in ${SYSLIST}
	do
	    THEFILE=${DATADIR}/OUT_${OUTNAME}_sys${sys}/${name}
	    if [ -f ${THEFILE} ]; then
		SYSFILES="${SYSFILES} ${THEFILE}"
		ROOTRM="${ROOTRM} ${THEFILE}:/cutflow"
		ROOTRM="${ROOTRM} ${THEFILE}:/cutflow_weighted"
	    fi
	done
	if [ "x${ROOTRM}x" != "xx" ]; then rootrm ${ROOTRM}; fi

	hadd -j 8 -n 0 ${DATADIR}/OUT_${OUTNAME}/${name} ${path} ${SYSFILES}
    done

    # Merge data
    for path in $(find ${DATADIR}/OUT_${OUTNAME}_nominal/data-* -type f)
    do
	name=$(basename ${path})
	data=$(basename $(dirname ${path}))

	SYSFILES=""
	for sys in ${SYSLIST}
	do
	    THEFILE=${DATADIR}/OUT_${OUTNAME}_sys${sys}/${data}/${name}
	    if [ -f ${THEFILE} ]; then
		SYSFILES="${SYSFILES} ${THEFILE}"
	    fi
	done

	outdir=${DATADIR}/OUT_${OUTNAME}/${data}
	if [ ! -e ${outdir} ]; then
	    mkdir ${outdir}
	fi

	hadd -j 8 -n 0 ${outdir}/${name} ${path} ${SYSFILES}
    done
}

if [ ${#} != 1 ]; then
    echo "usage: ${0} direct/condor/gengine"
    exit -1
fi
# configuration
MODE=${1}
if [ -z ${DATADIR} ]; then
    DATADIR=$(pwd)
fi
