#!/usr/bin/env python

import ROOT

import sys
import glob
import os, os.path
import shutil

def recursive_rescale(indir,EventCount,outdir):
    keys=indir.GetListOfKeys()
    for key in keys:
        name=key.GetName()
        obj=key.ReadObj()
        if obj.InheritsFrom(ROOT.TH1.Class()):
            obj.Scale(1./EventCount)
            outdir.cd()
            obj.Write()
        elif obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            newoutdir=outdir.mkdir(obj.GetName())
            recursive_rescale(obj,EventCount,newoutdir)

if len(sys.argv)==1:
    print('usage: %s sampledir'%sys.argv[0])
    sys.exit(-1)

sampledir=sys.argv[1]

# Check if backup directory exists
if not os.path.isdir('%s/orig'%sampledir):
    os.makedirs('%s/orig'%sampledir)

rootfiles=glob.glob('%s/hist-*.root'%sampledir)

for rootfile in rootfiles:
    isTruth='TRUTH' in rootfile

    # Check if backup file exists
    origrootfile='%s/orig/%s'%(sampledir,os.path.basename(rootfile))
    if not os.path.exists(origrootfile): # Create backup
        shutil.copyfile(rootfile,origrootfile)

    if isTruth:
        metafile='%s/data-cutflow/%s'%(sampledir,os.path.basename(rootfile)[5:])
    else:
        metafile='%s/data-metadata/%s'%(sampledir,os.path.basename(rootfile)[5:])

    fh_out =ROOT.TFile.Open(rootfile,'recreate')
    fh_hist=ROOT.TFile.Open(origrootfile)
    fh_meta=ROOT.TFile.Open(metafile)

    if isTruth:
        EventCount=fh_meta.Get('cutflow_weighted')
        EventCount=EventCount.GetBinContent(1)
    else:
        EventCount=fh_meta.Get('MetaData_EventCount')
        EventCount=EventCount.GetBinContent(3)

    recursive_rescale(fh_hist,EventCount,fh_out)
