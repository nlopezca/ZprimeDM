#!/bin/bash

export PACKAGE=ZprimeDM
source ${AnalysisBase_PLATFORM}/bin/librun.sh

# sample list
TRUTH_DIJETGAMMA="../ZprimeFilelists/filelists/MC15.*.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph*.NTUP.txt"
TLA_DIJETGAMMA="../ZprimeDM/filelists/R20p7_lists/Sherpa_gammajet.gammajet_truth.NTUP.list"
MC_DIJETGAMMA="../ZprimeDM/filelists/Signal_dijetgamma.dijetgamma.NTUP.list ../ZprimeDM/filelists/Sherpa_gammajet.dijetgamma.NTUP.list"
DATA_DIJETGAMMA="../ZprimeDM/filelists/R20p7_lists/data15.gammajet.NTUP.list ../ZprimeDM/filelists/R20p7_lists/data16.gammajet.NTUP.list"

TRUTH_TRIJET="../ZprimeFilelists/filelists/MC15.*.MGPy8EG_N30LO_A14N23LO_dmA_jjj_J*.NTUP.txt ../ZprimeFilelists/filelists/MC15.*.MGPy8EG_N30LO_A14N23LO_dmA_jj_*.NTUP.txt"
TLA_TRIJET="../ZprimeDM/filelists/R20p7_lists/Pythia8_jetjet_tla.dijet.NTUP.list"
MC_TRIJET="../ZprimeDM/filelists/Pythia8_dijet.trijet.NTUP.list ../ZprimeDM/filelists/Sherpa_dijet.trijet.NTUP.list ../ZprimeDM/filelists/Signal_trijet.trijet.NTUP.list"
DATA_TRIJET="../ZprimeDM/filelists/R20p7_lists/data15.dijet.NTUP.list ../ZprimeDM/filelists/R20p7_lists/data16.dijet.NTUP.list"

syslist_jja=()
syslist_jjj=()

# Process
RUNLIST=""

# dijetjet
# runOne dijet_truth dijetjet_truth "-m -l" ${TRUTH_TRIJET}
# runOne dijet_truth dijetjet_tla "-m -w" ${TLA_TRIJET}
runOne trijet dijetjet_mc_nominal "-m" ${MC_TRIJET}
for sys in ${syslist_jjj[@]}
do
     runOne trijet dijetjet_mc_sys${sys} "-m -s ${sys}" ${MC_TRIJET}
done
# runOne trijet dijetjet_data "" ${DATA_TRIJET}

#
# dijetgamma
#runOne dijetgamma_truth dijetgamma_truth    "-m -l" "${TRUTH_DIJETGAMMA}"
#runOne dijetgamma_truth dijetgamma_tla "-m"    "${TLA_DIJETGAMMA}"
runOne dijetgamma dijetgamma_mc_nominal "-m" ${MC_DIJETGAMMA}
for sys in ${syslist_jja[@]}
do
    runOne dijetgamma dijetgamma_mc_sys${sys} "-m -s ${sys}" ${MC_DIJETGAMMA}
done
#runOne dijetgamma dijetgamma_data "" ${DATA_DIJETGAMMA}

for RUN in ${RUNLIST}
do
    echo "Waiting for PID ${RUN}"
    wait ${RUN} || exit 1
    echo "COMPLETED WITH ${?}"
done

#
# Merge
#

# dijetjet
mergeSys dijetjet_mc ${syslist_jjj[@]}
merge dijetjet_mc hist.root "hist-*Pythia*jetjet*JZ*[0-9]W.*.root"
merge dijetjet_mc hist_Sherpa.root "hist-*Sherpa_CT10_jets*.root"

# merge dijetjet_data hist15.root "hist-*data15*root"
# merge dijetjet_data hist16.root "hist-*data16*root"
# merge dijetjet_data hist.root "hist1[0-9].root"

# merge dijetjet_data hist15_debug.root "hist-*data15*debugrec_hlt*root"
# merge dijetjet_data hist16_debug.root "hist-*data16*debugrec_hlt*root"
# merge dijetjet_data hist_debug.root "hist1[0-9]_debug.root"

# dijetgamma
mergeSys dijetgamma_mc ${syslist_jja[@]}
merge dijetgamma_mc  hist.root "hist-*Sherpa*SinglePhoton*root"
#merge dijetgamma_tla hist.root "hist-*Sherpa*SinglePhoton*root"

# merge dijetgamma_data hist15.root "hist-*data15*root"
# merge dijetgamma_data hist16.root "hist-*data16*root"
# merge dijetgamma_data hist.root "hist1[0-9].root"

# merge dijetgamma_data hist15_debug.root "hist-*data15*debugrec_hlt*root"
# merge dijetgamma_data hist16_debug.root "hist-*data16*debugrec_hlt*root"
# merge dijetgamma_data hist_debug.root "hist1[0-9]_debug.root"
