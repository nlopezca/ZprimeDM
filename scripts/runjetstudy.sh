#!/bin/bash

trap 'echo "KILL" ${RUNLIST}; kill ${RUNLIST}' EXIT

function runOne {
    CONFIG=${1}    
    OUTNAME=${2}
    EXTRA=${3}
    FILELIST=${@:4}

    runone.sh -d ${DATADIR} ${EXTRA} ${MODE} ${CONFIG} ${OUTNAME} ${FILELIST} > logs/${OUTNAME}.out 2> logs/${OUTNAME}.err &
    NEWPID=${!}
    RUNLIST="${RUNLIST} ${NEWPID}"
    echo "${OUTNAME} PID = ${NEWPID}"
}

function merge {
    OUTNAME=${1}
    OUTROOT=${2}
    FILELIST=${@:3}

    hadd -n 0 -f ${DATADIR}/OUT_${OUTNAME}/${OUTROOT} ${DATADIR}/OUT_${OUTNAME}/${FILELIST} || exit -1
}

function mergeSys {
    OUTNAME=${1}
    SYSLIST=${@:2}

    if [ -e ${DATADIR}/OUT_${OUTNAME} ]; then
	rm -rf ${DATADIR}/OUT_${OUTNAME}
    fi
    mkdir ${DATADIR}/OUT_${OUTNAME}

    for path in ${DATADIR}/OUT_${OUTNAME}_nominal/hist-*
    do
	name=$(basename ${path})

	SYSFILES=""
	ROOTRM=""
	for sys in ${SYSLIST}
	do
	    SYSFILES="${SYSFILES} ${DATADIR}/OUT_${OUTNAME}_sys${sys}/${name}"
	    ROOTRM="${ROOTRM} ${DATADIR}/OUT_${OUTNAME}_sys${sys}/${name}:/cutflow"
	    ROOTRM="${ROOTRM} ${DATADIR}/OUT_${OUTNAME}_sys${sys}/${name}:/cutflow_weighted"
	done
	rootrm.py ${ROOTRM}

	hadd ${DATADIR}/OUT_${OUTNAME}/${name} ${path} ${SYSFILES} > logs/haddsys_${OUTNAME}.out 2> logs/haddsys_${OUTNAME}.err &
    done
    wait
}

if [ ${#} != 1 ]; then
    echo "usage: ${0} direct/condor"
    exit -1
fi
# configuration
MODE=${1}
DATADIR=/share/t3data2/kkrizka/ZprimeDM/condor

MC_DIJET="ZprimeDM/filelists/Pythia8_jetjet_JZ.simpledijet.NTUP.list ZprimeDM/filelists/Pythia8_jetjet.simpledijet.NTUP.list ZprimeDM/filelists/Sherpa_jetjet.simpledijet.NTUP.list"
DATA_DIJET="ZprimeDM/filelists/data15.simpledijet.NTUP.list ZprimeDM/filelists/data16.simpledijet.NTUP.list"



# Process
RUNLIST=""



# run
runOne jetstudy jetstudy_mc   "-m" ${MC_DIJET}
runOne jetstudy jetstudy_data ""   ${DATA_DIJET}

for RUN in ${RUNLIST}
do
    echo "Waiting for PID ${RUN}"
    wait ${RUN} || exit 1
    echo "COMPLETED WITH ${?}"
done

# Merge

merge jetstudy_mc hist.root "hist-*Pythia*jetjet*JZ*[0-9]W.*.root"
merge jetstudy_mc hist_JZ.root "hist-*Pythia*jetjet*JZ*[0-9].*.root"

merge jetstudy_data hist15.root "hist-*data15*root"
merge jetstudy_data hist16.root "hist-*data16*root"
