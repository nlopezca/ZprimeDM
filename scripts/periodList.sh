#!/bin/bash

if [ ${#} != 2 ]; then
    echo "usage: ${0} input.list tag"
    exit -1
fi

TAG=${2}
NAME=$(basename ${1} .list)

YEAR=$(cut -d . -f 1 <<< ${NAME})
DAOD=$(cut -d . -f 2 <<< ${NAME})
echo "Processing data year ${YEAR} in DAOD ${DAOD}"

function join_by { local IFS="$1"; shift; echo "$*"; }

declare -A periods

if [ "${YEAR}" == "data15" ]; then
    periods["periodA"]="266904 267639"
    periods["periodB"]="267358 267599"
    periods["periodC"]="270441 272531"
    periods["periodD"]="276073 276954"
    periods["periodE"]="278727 279928"
    periods["periodF"]="279932 280422"
    periods["periodG"]="280423 281075"
    periods["periodH"]="281130 281411"
    periods["periodI"]="281662 282482"
    periods["periodJ"]="282625 284484"
elif [ "${YEAR}" == "data16" ]; then 
    periods["periodA"]="296939 300287"
    periods["periodB"]="300345 300908"
    periods["periodC"]="301912 302393"
    periods["periodD"]="302737 303560"
    periods["periodE"]="303638 303892"
    periods["periodF"]="303943 304494"
    periods["periodG"]="305291 306714"
    #periods["periodH"]="305359,309314,309346,310216" #include in G
    periods["periodI"]="307124 308084"
    periods["periodJ"]="308979 309166"
    periods["periodK"]="309311 309759"
    periods["periodL"]="310015 311481"
else
    echo "Unsupported data year ${YEAR}"
    exit -1
fi

if [ -e ${NAME}.cont.list ]; then
    rm ${NAME}.cont.list
fi

for key in ${!periods[@]}
do
    echo ${key}
    range=(${periods[${key}]})
    runs=$(join_by "|" $(seq ${periods[${key}]}))
    datasets=$(grep -E ${runs} ${1})
    if [ "x${datasets}x" == "xx" ]; then
	continue
    fi
    
    container=user.${USER}.${YEAR}.${key}.${DAOD}.${TAG}
    if [ "x$(rucio list-dids ${container} --short)x" == "xx" ]; then
	rucio add-container ${container}
	rucio attach ${container} ${datasets}
    fi
    echo ${container} >> ${NAME}.cont.list
done

