#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} datadir"
    exit -1
fi

cd ${1}

for outFile in $(ls hist-user.*.mc16?.*_tree.root.root | sed -e 's/mc16[a-d]/mc16_13TeV/' | sort | uniq)
do
    hadd -j 8 $(basename ${outFile}) ${outFile/mc16_13TeV/*}
done

for outFile in $(find data-* -type f -name 'user.*.mc16?.*_tree.root.root' | sed -e 's/mc16[a-d]/mc16_13TeV/' | sort | uniq)
do
    hadd -j 8 ${outFile} ${outFile/mc16_13TeV/*}
done