#!/bin/bash

if [ ${#} != 2 ]; then
    echo "usage: ${0} datasets.list /path/to/download"
    exit -1
fi

DSLIST=${1}
MAINDST=${2}

if [ ! -e ${MAINDST} ]; then
    mkdir -p ${MAINDST}
fi

for ds in $(cat ${DSLIST})
do
    DST=${MAINDST}/${ds}
    # if [ -e ${DST} ]; then
    # 	rm -rf ${DST}
    # fi
    # mkdir ${DST}

    FILELIST=${ds}.list
    if [ -e ${FILELIST} ]; then
	rm ${FILELIST}
    fi

    rucio download --dir ${MAINDST} ${ds}
    find  ${DST} -type f > ${FILELIST}
done
