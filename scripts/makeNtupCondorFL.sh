#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} outpath"
    exit -1
fi

outpath=${1}

for infile in ${outpath}/*
do
    xrdcp -f ${infile} root://faxbox.usatlas.org://faxbox2/user/kkrizka/ntup/$(basename ${infile}) &

    dsname=$(basename ${infile} .TRUTH1.root)
    echo root://faxbox.usatlas.org://faxbox2/user/kkrizka/ntup/$(basename ${infile}) > ZprimeFilelists/filelists/${dsname}.NTUP.txt
done
wait
