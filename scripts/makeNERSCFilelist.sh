#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} dslist"
    exit -1
fi

dslist=${1}

for dsname in $(cat ${dslist})
do
    dsname=$(basename ${dsname})
    rucio list-file-replicas ${dsname}/ --rse NERSC_LOCALGROUPDISK --protocol gsiftp | awk '{print $12}' | grep gsiftp | sed -e 's/gsiftp:\/\/[^\/]*//g' > filelists/${dsname}.txt
done
