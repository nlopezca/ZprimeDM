#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} dslist"
    exit -1
fi

dslist=${1}
workdir=$(pwd)

if [ -z ${RUCIODIR+x} ]; then
    RUCIODIR=$(pwd)/rucio
fi
   
cd ${RUCIODIR}

for dsname in $(cat ${workdir}/${dslist})
do
    rucio download ${dsname}
    find $(pwd)/${dsname} -type f | sort > ${workdir}/filelists/${dsname}.txt
done
