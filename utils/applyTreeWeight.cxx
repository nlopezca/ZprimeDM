#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>

#include <getopt.h>

#include <iostream>
#include <unordered_set>
#include <unordered_map>

//------ SETTINGS
bool debug=false;
float lumi=1.;
//---------------                                                                                                                                                                                                                                                                         

void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] treeFile.root" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -l, --lumi      Scale by luminosity (default: " << lumi << ")" << std::endl;
  std::cerr << " -d, --debug     Enable more verbose printout"  << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "" << std::endl;
}


int main(int argc, char* argv[])
{
  std::cout << "Hello World!" << std::endl;

  //
  // Argument parsing
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
        {
          {"lumi" ,    required_argument, 0,  'l' },
          {"debug",    no_argument      , 0,  'd' },
          {0,          0                , 0,  0 }
        };

      c = getopt_long(argc, argv, "dl:", long_options, &option_index);
      if (c == -1)
        break;

      switch (c)
        {
        case 'l':
          lumi = std::stof(optarg);
          break;
        case 'd':
          debug = true;
          break;
        default:
	  std::cerr << "Invalid option supplied. Aborting." << std::endl;
	  std::cerr << std::endl;
          usage(argv);
        }
    }

  if (argc-optind < 1)
    {
      std::cerr << "Required paths missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  std::string infilePath = argv[optind++];

  std::cout << infilePath << std::endl;

  //
  // List of datasets (DSIDs) to normalize using sum of events instead of weights
  std::unordered_set<uint32_t> UNWEIGHTED_NORM;

  // // Pythia 8 jetjet
  // for(uint32_t i=0;i<13;i++)
  //   UNWEIGHTED_NORM.insert(364700+i);

  // // Sherpa ttbar
  // UNWEIGHTED_NORM.insert(410249);
  // UNWEIGHTED_NORM.insert(410250);
  // UNWEIGHTED_NORM.insert(410251);
  // UNWEIGHTED_NORM.insert(410252);

  // // Powheg ttbar
  // UNWEIGHTED_NORM.insert(410470);
  // UNWEIGHTED_NORM.insert(410471);
  // UNWEIGHTED_NORM.insert(410472);

  // // Sherpa 2.1 V+jets
  // UNWEIGHTED_NORM.insert(304307);
  // UNWEIGHTED_NORM.insert(304308);
  // UNWEIGHTED_NORM.insert(304309);
  // UNWEIGHTED_NORM.insert(304707);
  // UNWEIGHTED_NORM.insert(304708);
  // UNWEIGHTED_NORM.insert(304709);

  // // Sherpa 2.2.5 V+jets
  // UNWEIGHTED_NORM.insert(364378);
  // UNWEIGHTED_NORM.insert(364379);
  // UNWEIGHTED_NORM.insert(364380);
  // UNWEIGHTED_NORM.insert(364375);
  // UNWEIGHTED_NORM.insert(364376);
  // UNWEIGHTED_NORM.insert(364377);

  // old samples
  UNWEIGHTED_NORM.insert(309450);
  UNWEIGHTED_NORM.insert(345931);

  //
  // List of datasets (DSIDs) to with extra weightsnormalize using sum of weights
  std::unordered_map<uint32_t, float> EXTRA_WEIGHT;

  // Sherpa 2.1 W+jets NLO-kfactor
  EXTRA_WEIGHT[304307]=1.28;
  EXTRA_WEIGHT[304308]=1.28;
  EXTRA_WEIGHT[304309]=1.28;

  // Sherpa 2.1 Z+jets NLO-kfactor
  EXTRA_WEIGHT[304707]=1.37;
  EXTRA_WEIGHT[304708]=1.37;
  EXTRA_WEIGHT[304709]=1.37;

  // VH k-factors
  EXTRA_WEIGHT[309451]=1.25;
  EXTRA_WEIGHT[309452]=1.47;

  // t-channel singletop Powheg Pythia x-section fix
  //EXTRA_WEIGHT[410646]=1./3.790700e+01;
  //EXTRA_WEIGHT[410647]=1./3.793700e+01;

  //
  // Updating
  TFile* fh=TFile::Open(infilePath.c_str(), "UPDATE");
  TH1F *cutflow =static_cast<TH1F*>(fh->Get("cutflow" ));
  TH1F *cutflowW=static_cast<TH1F*>(fh->Get("cutflow_weighted"));
  float norm =lumi/cutflow ->GetBinContent(1);
  float normW=lumi/cutflowW->GetBinContent(1);

  TIter next(static_cast<TList*>(fh->GetListOfKeys()->Clone()));
  TObject *keyobj = nullptr;
  while((keyobj = next()))
    {
      TKey *key=static_cast<TKey*>(keyobj);
      std::cout << "Processing " << key->GetName() << std::endl;
      TTree *t=dynamic_cast<TTree*>(fh->Get(key->GetName()));
      if(t==nullptr) continue;

      bool isMC=(t->GetBranch("mcChannelNumber")!=nullptr);

      // New branches
      Float_t v_w;
      TBranch *br_w = t->Branch("w", &v_w, "w/F");

      // Existing branches
      Float_t v_weight;
      t->SetBranchAddress("weight", &v_weight);

      Int_t v_mcChannelNumber;
      if(isMC) t->SetBranchAddress("mcChannelNumber", &v_mcChannelNumber);

      // Process
      Long64_t nentries = t->GetEntries();
      for (Long64_t i = 0; i < nentries; i++)
	{
	  if((i%10000)==0) std::cout << "Event " << i << std::endl;
	  t->GetEntry(i);
	  if(isMC)
	    v_w = v_weight * ((UNWEIGHTED_NORM.count(v_mcChannelNumber)==0)?normW:norm) * ((EXTRA_WEIGHT.count(v_mcChannelNumber)==0)?1.:EXTRA_WEIGHT[v_mcChannelNumber]);
	  else
	    v_w = 1.;
	  br_w->Fill();
	}
      t->Write("", TObject::kOverwrite);
    }

  fh->Close();

  return 0;
}
